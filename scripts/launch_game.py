import argparse
import os
import shutil
from argparse import ArgumentParser, Namespace
from os import PathLike
from typing import Dict


def copy_files(source_code_dir: PathLike, agent_code_dir: PathLike, game_dir: PathLike) -> None:
    target_dir = f"{game_dir}/agent_code"

    # Copy agents to game directory.
    shutil.copytree(agent_code_dir, target_dir, dirs_exist_ok=True)

    for agent_name in os.listdir(agent_code_dir):

        # Copy src/ directory to each agent folder.
        agent_dir = f"{agent_code_dir}/{agent_name}"
        if not os.path.isdir(agent_dir):
            continue
        game_agent_dir = f"{target_dir}/{agent_name}"
        game_agent_src_dir = f"{game_agent_dir}/src"
        shutil.copytree(source_code_dir, game_agent_src_dir, dirs_exist_ok=True)
        game_agent_callback_file = f"{game_agent_dir}/callbacks.py"

        # Add agent folders to PATH for imports to work correctly.
        with open(game_agent_callback_file, mode="r+") as f:
            contents = f.read()
            f.seek(0)
            f.write(
                "import pathlib\n"
                "import sys\n\n"
                "agent_file_path = pathlib.Path(__file__)  # noqa: RUF100\n"
                'sys.path.append(f"{agent_file_path.parent}")  # noqa: RUF100\n\n',
            )
            f.write(contents)


def get_system_variables(args: Namespace) -> Dict[str, str]:
    sys_vars = {}
    for key, value in vars(args).items():
        if not key.startswith("var_") or value is None:
            continue
        sys_vars[key[4:].upper()] = str(value)
    return sys_vars


def export_system_variables(variables: Dict[str, str]) -> None:
    for key, value in variables.items():
        os.environ[key] = value


def parse_args() -> Namespace:

    current_directory = os.path.abspath(os.curdir)

    # Add default arguments.
    parser = ArgumentParser()
    parser.add_argument(
        "--prefix",
        type=str,
        dest="var_prefix",
        help="A file prefix to use for storing and loading data.",
    )
    parser.add_argument(
        "--agent_code_directory",
        type=str,
        default=f"{current_directory}/agents",
        dest="agent_code_dir",
        help=f"Set the location of the agent code. Default: {current_directory}/agents",
    )
    parser.add_argument(
        "--source_code_directory",
        type=str,
        default=f"{current_directory}/src",
        dest="source_code_dir",
        help=f"Set the location of the additional source code. Default: {current_directory}/src",
    )
    parser.add_argument(
        "--game_directory",
        type=str,
        default=f"{current_directory}/game",
        dest="game_dir",
        help=f"Set the location of the game directory. Default: {current_directory}/game",
    )
    parser.add_argument(
        "--model_path",
        type=str,
        dest="var_model_path",
        default=f"{current_directory}/models",
        help=f"Set the model path to use. Default: {current_directory}/models",
    )
    parser.add_argument(
        "--log_path",
        type=str,
        dest="logging_path",
        default=f"{current_directory}/logs",
        help=f"Set the logging path to use. Default: {current_directory}/logs",
    )
    parser.add_argument(
        "--log_interval",
        type=int,
        dest="var_log_interval",
        default=1,
        help="Set the interval to use for saving logged data to disk.",
    )
    parser.add_argument(
        "--log_capacity",
        type=int,
        dest="var_log_capacity",
        default=100000,
        help="Set for how many rounds logging data is stored in memory.",
    )
    parser.add_argument(
        "--use_gpu",
        action="store_true",
        dest="var_use_gpu",
        help="Should the agent try to use GPU support?",
    )
    parser.add_argument(
        "--data_path",
        type=str,
        dest="var_data_path",
        default=f"{current_directory}/data",
        help=f"Set the data path to use. Default: {current_directory}/data",
    )
    parser.add_argument(
        "--game_args",
        required=True,
        dest="game_args",
        type=str,
        nargs=argparse.REMAINDER,
        help="Set the arguments to pass to the game. Remaining arguments will be appended here.",
    )
    return parser.parse_args()


if __name__ == "__main__":

    # Parse arguments.
    args = parse_args()

    # Convert paths to absolute ones.
    args.source_code_dir = os.path.abspath(args.source_code_dir)
    args.agent_code_dir = os.path.abspath(args.agent_code_dir)
    args.game_dir = os.path.abspath(args.game_dir)
    args.var_model_path = os.path.abspath(args.var_model_path)
    args.var_data_path = os.path.abspath(args.var_data_path)

    # Add variable for number of rounds to play.
    try:
        index = args.game_args.index("--n-rounds")
    except ValueError:
        args.var_last_round = 10
    else:
        args.var_last_round = args.game_args[index + 1]

    # Add variable for used seed value.
    try:
        index = args.game_args.index("--seed")
    except ValueError:
        pass
    else:
        args.var_seed = args.game_args[index + 1]

    # Copy code to game directory.
    copy_files(args.source_code_dir, args.agent_code_dir, args.game_dir)

    # Set environment variables.
    sys_vars = get_system_variables(args)
    export_system_variables(sys_vars)

    # Start game.
    python_name = "python3.11" if os.name == "nt" else "python3"
    os.system(
        f"{python_name} \"{args.game_dir}/main.py\" {' '.join(args.game_args)} "
        f'--log-dir \"{args.logging_path}\"',
    )
