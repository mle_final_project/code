import os
from argparse import ArgumentParser, Namespace
from typing import List, Optional

SETTINGS_FILE_NAME = "settings.py"

# Action names.
UPDATE = "update"
RESTORE = "restore"

# Mode names
COIN_HEAVEN = "coin-heaven"
LOOT_CRATE = "loot-crate"
CLASSIC = "classic"

# Setting names.
WIDTH = "COLS"
HEIGHT = "ROWS"
STEPS = "MAX_STEPS"
CRATE_DENSITY = "CRATE_DENSITY"
COIN_COUNT = "COIN_COUNT"

# Default settings.
DEFAULTS = {
    COIN_HEAVEN: {
        WIDTH: 17,
        HEIGHT: 17,
        STEPS: 400,
        CRATE_DENSITY: 0,
        COIN_COUNT: 50,
    },
    LOOT_CRATE: {
        WIDTH: 17,
        HEIGHT: 17,
        STEPS: 400,
        CRATE_DENSITY: 0.75,
        COIN_COUNT: 50,
    },
    CLASSIC: {
        WIDTH: 17,
        HEIGHT: 17,
        STEPS: 400,
        CRATE_DENSITY: 0.75,
        COIN_COUNT: 9,
    },
}

# Line indices in settings file.
LINE_INDICES = {
    COIN_HEAVEN: {
        WIDTH: 7,
        HEIGHT: 8,
        STEPS: 34,
        CRATE_DENSITY: 16,
        COIN_COUNT: 17,
    },
    LOOT_CRATE: {
        WIDTH: 7,
        HEIGHT: 8,
        STEPS: 34,
        CRATE_DENSITY: 20,
        COIN_COUNT: 21,
    },
    CLASSIC: {
        WIDTH: 7,
        HEIGHT: 8,
        STEPS: 34,
        CRATE_DENSITY: 25,
        COIN_COUNT: 26,
    },
}


def set_lines(
    lines: List[str],
    mode: str,
    width: Optional[int],
    height: Optional[int],
    crate_density: Optional[float],
    coin_count: Optional[int],
    steps: Optional[int],
) -> None:
    line_indices = LINE_INDICES[mode]
    if mode is not None:
        lines[line_indices[WIDTH]] = f"{WIDTH} = {width}\n"
    if height is not None:
        lines[line_indices[HEIGHT]] = f"{HEIGHT} = {height}\n"
    if crate_density is not None:
        lines[line_indices[CRATE_DENSITY]] = f'        "{CRATE_DENSITY}": {crate_density},\n'
    if coin_count is not None:
        lines[line_indices[COIN_COUNT]] = f'        "{COIN_COUNT}": {coin_count},\n'
    if steps is not None:
        lines[line_indices[STEPS]] = f"{STEPS} = {steps}\n"


def update(args: Namespace) -> None:
    file_name = f"{args.game_dir}/{SETTINGS_FILE_NAME}"

    # Read settings file.
    lines = []
    with open(file_name) as f:
        lines = f.readlines()

    # Get settings from args.
    settings = {
        WIDTH: args.width,
        HEIGHT: args.height,
        STEPS: args.steps,
        COIN_COUNT: args.coins,
        CRATE_DENSITY: args.crate_density,
    }

    # Reset settings to default values.
    mode = args.mode
    modes = [mode] if mode not in ("all", ["all"]) else [COIN_HEAVEN, LOOT_CRATE, CLASSIC]
    for mode in modes:
        default_settings = DEFAULTS[mode]

        # Get settings to use.
        width = settings[WIDTH]
        height = settings[HEIGHT]
        crate_density = settings[CRATE_DENSITY]
        coin_count = settings[COIN_COUNT]
        steps = settings[STEPS]

        # Check for None values.
        if width is None:
            width = default_settings[WIDTH]
        if height is None:
            height = default_settings[HEIGHT]
        if crate_density is None:
            crate_density = default_settings[CRATE_DENSITY]
        if coin_count is None:
            coin_count = default_settings[COIN_COUNT]
        if steps is None:
            steps = default_settings[STEPS]

        # Update lines.
        set_lines(
            lines=lines,
            mode=mode,
            width=width,
            height=height,
            crate_density=crate_density,
            coin_count=coin_count,
            steps=steps,
        )

    # Write file.
    with open(file_name, mode="w") as f:
        f.writelines(lines)


def restore(args: Namespace) -> None:
    file_name = f"{args.game_dir}/{SETTINGS_FILE_NAME}"

    # Read settings file.
    lines = []
    with open(file_name) as f:
        lines = f.readlines()

    # Reset settings to default values.
    mode = args.mode
    modes = [mode] if mode not in ("all", ["all"]) else [COIN_HEAVEN, LOOT_CRATE, CLASSIC]
    options = args.restore_options if args.restore_options not in ("all", ["all"]) else [
        "width", "height", "crate_density", "coin_count", "steps",
    ]
    for mode in modes:
        settings = DEFAULTS[mode]
        set_lines(
            lines=lines,
            mode=mode,
            width=settings[WIDTH] if "width" in options else None,
            height=settings[HEIGHT] if "height" in options else None,
            crate_density=settings[CRATE_DENSITY] if "crate_density" in options else None,
            coin_count=settings[COIN_COUNT] if "coin_count" in options else None,
            steps=settings[STEPS] if "steps" in options else None,
        )

    # Write file.
    with open(file_name, mode="w") as f:
        f.writelines(lines)


def parse_args() -> Namespace:
    current_directory = os.path.abspath(os.curdir)

    parser = ArgumentParser()
    subparsers = parser.add_subparsers(help="The action to perform.", dest="action")

    # Parse update arguments.
    update_parser = subparsers.add_parser("update", help="Update game settings.")
    update_parser.add_argument(
        "mode",
        type=str,
        choices=["all", "coin-heaven", "loot-crate", "classic"],
        help="The mode to update or restore.",
    )
    update_parser.add_argument(
        "--width",
        type=int,
        dest="width",
        help="Set the width of the playing field.",
    )
    update_parser.add_argument(
        "--height",
        type=int,
        dest="height",
        help="Set the height of the playing field.",
    )
    update_parser.add_argument(
        "--steps",
        type=int,
        dest="steps",
        help="Set the number of steps to play per game.",
    )
    update_parser.add_argument(
        "--coins",
        type=int,
        dest="coins",
        help="Set the number of coins to spawn.",
    )
    update_parser.add_argument(
        "--crate-density",
        type=float,
        dest="crate_density",
        help="Set the crate density.",
    )
    update_parser.add_argument(
        "--game_directory",
        type=str,
        default=f"{current_directory}/game",
        dest="game_dir",
        help=f"Set the location of the game directory. Default: {current_directory}/game",
    )

    # Parse restore arguments.
    restore_parser = subparsers.add_parser("restore", help="Restore game setting defaults.")
    restore_parser.add_argument(
        "mode",
        default="all",
        type=str,
        nargs="?",
        choices=["all", "coin-heaven", "loot-crate", "classic"],
        help="The mode to update or restore.",
    )
    restore_parser.add_argument(
        "restore_options",
        default="all",
        nargs="*",
        choices=["all", "width", "height", "steps", "coins", "crate-density"],
        help="The settings to restore to their default value.",
    )
    restore_parser.add_argument(
        "--game_directory",
        type=str,
        default=f"{current_directory}/game",
        dest="game_dir",
        help=f"Set the location of the game directory. Default: {current_directory}/game",
    )

    return parser.parse_args()


if __name__ == "__main__":

    # Parse arguments.
    args = parse_args()

    # Perform selected action.
    if args.action == "update":
        update(args)
    elif args.action == "restore":
        print(args)
        restore(args)
