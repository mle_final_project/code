import sys
from pathlib import Path

from matplotlib.axes import Axes
from matplotlib.figure import Figure

sys.path.append(str(Path("./").resolve()))  # noqa: RUF100
sys.path.append(str(Path("./game").resolve()))  # noqa: RUF100

import os
from argparse import ArgumentParser
from typing import List, Optional, Tuple, Union

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd

from src.events import BaseEvents, Event
from src.info import Actions


def smooth(scalars: List[float], weight: float) -> List[float]:  # Weight between 0 and 1
    # From https://stackoverflow.com/questions/5283649/plot-smooth-line-with-pyplot
    last = scalars[0]  # First value in the plot (first timestep)
    smoothed = []
    for point in scalars:
        smoothed_val = last * weight + (1 - weight) * point  # Calculate smoothed value
        smoothed.append(smoothed_val)                        # Save it
        last = smoothed_val                                  # Anchor the last smoothed value
    return smoothed


def plot(
    file: Union[str, os.PathLike],
    lines: List[Tuple[float, str]],
    keys: Optional[List[str]] = None,
    last_n: Optional[int] = None,
) -> Tuple[Axes, Figure]:

    # Get key values.
    file_name = f"{os.path.split(file)[-1]}"
    frame = pd.read_csv(file).set_index("ROUND")
    key_values = keys if keys not in (None, "ROUND") and len(keys) > 0 else frame.columns

    # Plot data.
    fig, ax = plt.subplots()
    data = frame.fillna(0)
    x = data.index.tolist()
    for key in key_values:
        if key not in key_values or key not in data.columns:
            continue
        values = data[key].values
        if last_n is not None:
            start_index = max(len(values) - last_n, 0)
            values_loc = values[start_index:].tolist()
            x_loc = x[start_index:]
        else:
            x_loc = x
            values_loc = values.tolist()
        ax.plot(x_loc, values_loc, alpha=0.5)
        ax.plot(x_loc, smooth(values_loc, 0.9), label=key, color=ax.lines[-1].get_color())
    for line_y, line_name in lines:
        ax.axhline(line_y, linestyle="dashed", alpha=0.5, label=line_name)
    ax.axhline(0, color="grey", linestyle="dashed", alpha=0.5)
    ax.grid(axis="both")
    ax.set_title(file_name)
    ax.set_xlabel("Round")
    ax.set_ylabel("Value")
    ax.legend()
    fig.tight_layout()

    return ax, fig


if __name__ == "__main__":
    current_directory = os.path.abspath(os.curdir)

    parser = ArgumentParser()
    parser.add_argument(
        "file",
        type=str,
        help="The file name to load from the data directory.",
    )
    parser.add_argument(
        "--plot_directory",
        dest="plot_dir",
        type=str,
        default=f"{current_directory}/plots",
        help=f"The directory to save plot data in. Default: {current_directory}/plots",
    )
    parser.add_argument(
        "--actions",
        action="store_true",
        dest="do_plot_actions",
        help="If set, plot performed actions.",
    )
    parser.add_argument(
        "--base-events",
        action="store_true",
        dest="do_plot_base_events",
        help="If set, plot base events.",
    )
    parser.add_argument(
        "--custom-events",
        action="store_true",
        dest="do_plot_custom_events",
        help="If set, plot custom events.",
    )
    parser.add_argument(
        "--total",
        action="store_true",
        dest="do_plot_total",
        help="If set, plot total.",
    )
    parser.add_argument(
        "--keys",
        nargs="+",
        dest="keys",
        help="Plot a specific list of events.",
        default=[],
    )
    parser.add_argument(
        "--last-n",
        type=int,
        dest="last_n",
        default=None,
        help="Only plot the last n values.",
    )
    parser.add_argument(
        "--lines",
        type=int,
        nargs="*",
        dest="lines_y",
        help="Y values to plot additional lines at.",
        default=[],
    )
    parser.add_argument(
        "--line_names",
        type=str,
        nargs="*",
        dest="lines_names",
        help="The names to use for the provided additional lines.",
        default=[],
    )
    args = parser.parse_args()

    # Use TK based backend since for some reason the default Qt one causes segmentation fault on
    # exit on my system...
    matplotlib.use("TkAgg")

    # Get keys.
    keys: List[str] = args.keys
    if args.do_plot_actions:
        keys.extend(Actions.ALL)
    if args.do_plot_base_events:
        keys.extend(BaseEvents.all_events())
    if args.do_plot_custom_events:
        keys.extend([e().name for e in Event.__subclasses__()])  # type: ignore
    if args.do_plot_total:
        keys.extend(["TOTAL_REWARD"])

    f = args.file

    # Get lines.
    lines = []
    y_values = args.lines_y
    names = args.lines_names
    for index, y in enumerate(y_values):
        lines.append((y, names[index] if index < len(names) else ""))

    # Plot data.
    ax, fig = plot(f, lines, keys, args.last_n)

    # Save plot.
    plot_dir = args.plot_dir
    if not os.path.isdir(plot_dir):
        i = input(f"Plot path '{plot_dir}' does not exist. Create it ([y]es/[n]o)? ")
        if i.lower() not in ("yes", "y"):
            sys.exit(0)
        os.mkdir(plot_dir)
    save_file = f"{plot_dir}/{Path(f).name.split('.')[0]}.svg"
    fig.savefig(save_file)
