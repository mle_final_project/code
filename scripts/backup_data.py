import os
import shutil
from argparse import ArgumentParser, Namespace, RawTextHelpFormatter
from os import PathLike


def backup_data(model_path: PathLike, data_path: PathLike, agent_name: str, backup_name: str) -> None:

    def backup_dir(directory: PathLike) -> None:
        target_dir = f"{directory}/{agent_name}_{backup_name}"
        if not os.path.exists(target_dir):
            os.mkdir(target_dir)
        else:
            response = input(
                f'Backup directory "{target_dir}" already exists. Overwrite contents? ([y]es/[n]o)',
            )
            if not response in ("y", "yes"):
                return
        for f in os.listdir(directory):
            source_path = f"{directory}/{f}"
            if not f.startswith(agent_name) or os.path.isdir(source_path):
                continue
            shutil.copy(source_path, f"{target_dir}/{f}")

    backup_dir(model_path)
    backup_dir(data_path)


def parse_args() -> Namespace:
    current_directory = os.path.abspath(os.curdir)
    parser = ArgumentParser(
        description=(
            "description:"
            "\n  backup data for a specific model"
            "\n\nbackup locations:"
            "\n  [MODEL_PATH]/[AGENT_NAME]_[BACKUP_NAME]/"
            "\n   [DATA_PATH]/[AGENT_NAME]_[BACKUP_NAME]/"
        ),
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "agent_name",
        type=str,
        help="the name of the model to backup data from",
    )
    parser.add_argument(
        "backup_name",
        type=str,
        help="the name to use for the backup",
    )
    parser.add_argument(
        "--model_path",
        type=str,
        dest="model_path",
        default=f"{current_directory}/models",
        help=f"set the model path to use (default: {current_directory}/models)",
    )
    parser.add_argument(
        "--data_path",
        type=str,
        dest="data_path",
        default=f"{current_directory}/data",
        help=f"set the data path to use  (default: {current_directory}/data)",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    backup_data(args.model_path, args.data_path, args.agent_name, args.backup_name)
