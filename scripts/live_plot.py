import sys
from pathlib import Path

from matplotlib.axis import XAxis, YAxis
from matplotlib.lines import Line2D

sys.path.append(str(Path("./").resolve()))  # noqa: RUF100
sys.path.append(str(Path("./game").resolve()))  # noqa: RUF100

import os
from argparse import ArgumentParser
from typing import Callable, Dict, List, Optional, Tuple, Union

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.animation import FuncAnimation

from src.events import BaseEvents, Event
from src.info import Actions


def smooth(scalars: List[float], weight: float) -> List[float]:  # Weight between 0 and 1
    # From https://stackoverflow.com/questions/5283649/plot-smooth-line-with-pyplot
    last = scalars[0]  # First value in the plot (first timestep)
    smoothed = []
    for point in scalars:
        smoothed_val = last * weight + (1 - weight) * point  # Calculate smoothed value
        smoothed.append(smoothed_val)                        # Save it
        last = smoothed_val                                  # Anchor the last smoothed value
    return smoothed


def generate_animate_func(
    file: Union[str, os.PathLike],
    additional_lines: List[Tuple[float, str]],
    keys: Optional[List[str]] = None,
    last_n: Optional[int] = None,
) -> Callable[[int], List[Line2D | YAxis | XAxis]]:

    # Get key values.
    file_name = f"{os.path.split(file)[-1]}"
    frame = pd.read_csv(file).set_index("ROUND")
    key_values = keys if keys not in (None, "ROUND") and len(keys) > 0 else frame.columns

    # Prepare empty plot.
    fig, ax = plt.subplots()
    lines: Dict[str, List[Line2D]] = {
        key: [
            ax.plot([], [], alpha=0.5)[0],
            ax.plot([], [], color=ax.lines[-1].get_color(), label=key)[0],
        ] for key in key_values
    }
    for line_y, line_name in additional_lines:
        ax.axhline(line_y, linestyle="dashed", alpha=0.5, label=line_name)
    ax.axhline(0, color="grey", linestyle="dashed", alpha=0.5)
    ax.grid(axis="both")
    ax.set_title(file_name)
    ax.set_xlabel("Round")
    ax.set_ylabel("Value")
    ax.legend()
    fig.tight_layout()

    def animate(_frame: int) -> List[Line2D | YAxis | XAxis]:
        try:
            data = pd.read_csv(file).fillna(0)
            for key in key_values:
                if key not in key_values or key not in data.columns:
                    continue
                values = data[key]
                x = data["ROUND"]
                if last_n is not None:
                    start_index = max(len(values) - last_n, 0)
                    values = values[start_index:].tolist()
                    x = x[start_index:]
                lines[key][0].set_data(x, values)
                lines[key][1].set_data(x, smooth(values, 0.9))  # type: ignore
        except Exception as e:  # noqa: BLE001
            print(f"Error while animating: {e}")
        ax.relim()
        ax.autoscale_view(tight=True)
        return [line for line_pairs in lines.values() for line in line_pairs] + [ax.xaxis, ax.yaxis]

    return animate


if __name__ == "__main__":
    current_directory = os.path.abspath(os.curdir)

    parser = ArgumentParser()
    parser.add_argument(
        "file",
        type=str,
        help="The file name to load from the data directory.",
    )
    parser.add_argument(
        "--directory",
        type=str,
        default=f"{current_directory}/data",
        help=f"The directory the data is located in. Default: {current_directory}/data",
    )
    parser.add_argument(
        "--actions",
        action="store_true",
        dest="do_plot_actions",
        help="If set, plot performed actions.",
    )
    parser.add_argument(
        "--base-events",
        action="store_true",
        dest="do_plot_base_events",
        help="If set, plot base events.",
    )
    parser.add_argument(
        "--custom-events",
        action="store_true",
        dest="do_plot_custom_events",
        help="If set, plot custom events.",
    )
    parser.add_argument(
        "--total",
        action="store_true",
        dest="do_plot_total",
        help="If set, plot total.",
    )
    parser.add_argument(
        "--keys",
        nargs="+",
        dest="keys",
        help="Plot a specific list of events.",
        default=[],
    )
    parser.add_argument(
        "--animation-rate",
        type=int,
        dest="animation_rate",
        default=1000,
        help="The time between animation updates in ms.",
    )
    parser.add_argument(
        "--last-n",
        type=int,
        dest="last_n",
        default=None,
        help="Only plot the last n values.",
    )
    parser.add_argument(
        "--lines",
        type=int,
        nargs="*",
        dest="lines_y",
        help="Y values to plot additional lines at.",
        default=[],
    )
    parser.add_argument(
        "--line_names",
        type=str,
        nargs="*",
        dest="lines_names",
        help="The names to use for the provided additional lines.",
        default=[],
    )
    parser.add_argument(
        "--dark",
        action="store_true",
        dest="use_dark_mode",
        help="If set, use dark pyplot theme.",
    )
    args = parser.parse_args()

    # Use TK based backend since for some reason the default Qt one causes segmentation fault on
    # exit on my system...
    matplotlib.use("TkAgg")

    # Set dark mode if specified.
    if args.use_dark_mode:
        plt.style.use("dark_background")

    # Get keys
    keys: List[str] = args.keys
    if args.do_plot_actions:
        keys.extend(Actions.ALL)
    if args.do_plot_base_events:
        keys.extend(BaseEvents.all_events())
    if args.do_plot_custom_events:
        keys.extend([e().name for e in Event.__subclasses__()])  # type: ignore
    if args.do_plot_total:
        keys.extend(["TOTAL_REWARD"])

    # Get lines.
    lines = []
    y_values = args.lines_y
    names = args.lines_names
    for index, y in enumerate(y_values):
        lines.append((y, names[index] if index < len(names) else ""))

    # Generate animation function
    f = f"{args.directory}/{args.file}"
    animate = generate_animate_func(f, lines, keys, args.last_n)

    # Play animation
    anim = FuncAnimation(
        fig=plt.gcf(),
        func=animate,
        frames=100,
        interval=args.animation_rate,
    )
    plt.show()
