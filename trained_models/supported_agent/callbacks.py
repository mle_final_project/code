"""This agent uses shortest path based features to determine the best action."""

# ----- Add local src path to Python PATH to make imports work in the tournament environment ----- #

import pathlib
import sys

import numpy as np

agent_file_path = pathlib.Path(__file__)  # noqa: RUF100
sys.path.append(f"{agent_file_path.parent}")  # noqa: RUF100
np.set_printoptions(edgeitems=30, linewidth=100000)  # noqa: RUF100

# ------------------------------------------------------------------------------------------------ #

from typing import List, Optional, Tuple

import torch
from src import events, features, preprocessing, setup
from src.agents import DQNAgent
from src.events import BaseEvents, Event
from src.info import actions

# Eps greedy settings.
EPS_START = 0.9
EPS_END = 0.01
EPS_DECAY = 1000

# Training settings.
GAMMA = 0.9975
TAU = 1
LR = 1e-4

# Network settings.
BATCH_SIZE = 32
N_INNER = 256
N_HIDDEN = 2

# Replay memory settings.
MEMORY_SIZE = 1_000_000
USE_PRIORITIZATION = True
PRIORITIZATION_ALPHA = 0.6
PRIORITIZATION_BETA = 0.4
PRIORITIZATION_EPS = 0.01

# Update settings.
UPDATE_INTERVAL = 4
TARGET_UPDATE_INTERVAL = 10_000

# Preprocessing to use.
PREPROCESSORS: List[preprocessing.StatePreprocessor] = []

# Features to use.
coin_influence = features.CoinInfluenceFeatureExtractor(
    base_value=1,
    min_value=0.001,
    max_value=50,
    influence_range=17 * 2,
    closest_bias=0.2,
    random_eps=0.0001,
    value_decrease_func=lambda x: 0.01 * x,
    radius=1,
    include_diagonals=False,
    normalize=True,
    cache_features=True,
)
crate_influence = features.CrateInfluenceFeatureExtractor(
    base_value=1,
    min_value=0.001,
    max_value=128,
    influence_range=17 * 2,
    closest_bias=0.2,
    random_eps=0.0001,
    value_decrease_func=lambda x: 0.01 * x,
    radius=1,
    include_diagonals=False,
    normalize=True,
    cache_features=True,
)

FEATURES: List[features.FeatureExtractor] = [
    # General state information and surrounding data.
    features.SurroundingFeatureExtractor(
        radius=4,
        normalize=True,
        include_diagonals=True,
        cache_features=True,
    ),
    features.LastActionsFeatureExtractor(
        n=1,
        normalize=True,
        cache_features=True,
    ),

    # Information relevant for bomb placement.
    features.CratesInBombRangeFeatureExtractor(
        normalize=True,
        cache_features=True,
    ),
    features.AgentsInBombRangeFeatureExtractor(
        normalize=True,
        cache_features=True,
    ),
    features.AgentBombStateFeatureExtractor(
        cache_features=False,
    ),

    # Information about the current danger situation around the agent.
    features.DangerInfoFeatureExtractor(
        radius=4,
        include_diagonals=True,
        normalize=True,
        cache_features=True,
    ),

    # Additional helper information intended to guide the agent towards sensible targets.
    coin_influence,
    crate_influence,
]

# Base events to use.
BASE_EVENTS: List[Tuple[str, Optional[float]]] = [
    # Penalize every valid move to encourage a faster play-style.
    (BaseEvents.MOVED_UP, -0.01),
    (BaseEvents.MOVED_DOWN, -0.01),
    (BaseEvents.MOVED_LEFT, -0.01),
    (BaseEvents.MOVED_RIGHT, -0.01),
    (BaseEvents.WAITED, -0.05),

    # Punish bombs per default to stop agent from spamming it. Sensible bombs are rewarded by
    # additional custom events.
    (BaseEvents.BOMB_DROPPED, -0.01),

    # Punish undesired behavior.
    (BaseEvents.INVALID_ACTION, -10),
    (BaseEvents.GOT_KILLED, -50),

    # Reward desired behavior. Coin discovery and crate destruction is not rewarded explicitly,
    # since the agent has no features that allow it to understand the reasoning behind them.
    (BaseEvents.COIN_COLLECTED, 150),
    (BaseEvents.OPPONENT_ELIMINATED, 200),
]

CRATE_REWARD = 15
MAX_CRATES_PER_BOMB = 12

AGENT_REWARD = 30
MAX_AGENTS_PER_BOMB = 3

# Custom events to use.
CUSTOM_EVENTS: List[Event] = [
    events.PlacedBombInRangeOfCrateEvent(
        base_reward=CRATE_REWARD,
        max_abs_reward=CRATE_REWARD * MAX_CRATES_PER_BOMB,
    ),
    events.PlacedBombInRangeOfAgentEvent(
        base_reward=AGENT_REWARD,
        max_abs_reward=AGENT_REWARD * MAX_AGENTS_PER_BOMB,
    ),
]

# Agent to use.
AGENT = DQNAgent(
    feature_extractor=features.FeatureExtractorCollection(FEATURES),
    action_set=actions.FullActionSet(),
    device=torch.device("cpu"),
    batch_size=BATCH_SIZE,
    gamma=GAMMA,
    eps_start=EPS_START,
    eps_end=EPS_END,
    eps_decay=EPS_DECAY,
    tau=TAU,
    lr=LR,
    n_inner=N_INNER,
    n_hidden=N_HIDDEN,
    update_interval=UPDATE_INTERVAL,
    target_update_interval=TARGET_UPDATE_INTERVAL,
    loss_function=DQNAgent.LossFunction.HUBER,
    memory_size=MEMORY_SIZE,
    use_prioritized_sampling=USE_PRIORITIZATION,
    prioritization_alpha=PRIORITIZATION_ALPHA,
    prioritization_beta=PRIORITIZATION_BETA,
    prioritization_eps=PRIORITIZATION_EPS,
    do_random_choices=False,
    do_auto_save=True,
    save_interval=10_000,
    name="supported_agent",
)

# Generate callbacks.
setup, act = setup.generate_callbacks(
    AGENT,
    custom_events=CUSTOM_EVENTS,
    base_events=BASE_EVENTS,
    preprocessors=PREPROCESSORS,
    log_interval=100,
)
