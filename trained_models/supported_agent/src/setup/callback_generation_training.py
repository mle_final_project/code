"""This module defines the callbacks used by the game for letting the agent train."""

from types import SimpleNamespace
from typing import Any, Callable, Dict, List, Optional, Tuple

from src.agents import Agent
from src.events import EventProcessor
from src.info import DataLogger
from src.info.state import State
from src.preprocessing.base.preprocessor import StatePreprocessor

SetupTrainingFuncType = Callable[[SimpleNamespace], None]
"""The type of the `setup_training` callback."""

GameEventsOccurredFuncType = Callable[
    [SimpleNamespace, Dict[str, Any], str, Dict[str, Any], List[str]], None,
]
"""The type of the `game_events_occurred` callback."""

EndOfRoundFuncType = Callable[[SimpleNamespace, Dict[str, Any], str, List[str]], None]
"""The type of the `end_of_round` callback."""


def generate_callbacks() -> Tuple[
    SetupTrainingFuncType,
    GameEventsOccurredFuncType,
    EndOfRoundFuncType,
]:
    """Create the callbacks the game expects in the `train.py` file for the given agent.

    Parameters
    ----------
    agent : Agent
        The agent to generate the callbacks for.

    Returns
    ----------
    setup_training : SetupTrainingFuncType
        The `setup_training` callback function.

    game_events_occurred : GameEventsOccurredFuncType
        The `game_events_occurred` callback function.

    end_of_round : EndOfRoundFuncType
        The `end_of_round` callback function.
    """

    def setup_training(self: SimpleNamespace) -> None:
        self.is_in_training = True
        agent: Agent = self.agent
        agent._setup_training_implementation()

    def game_events_occurred(
        self: SimpleNamespace,
        old_game_state: Dict[str, Any],
        self_action: str,
        new_game_state: Optional[Dict[str, Any]],
        events: List[str],
    ) -> None:
        """Generate event base, custom, and agent specific rewards, log data and perform an agent
        optimization step.

        Called every time a new game state is reached.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : str
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in last game transition.

        events : List[str]
            The events that occurred during the state transition.
        """
        # This is necessary since the game currently does not report the actual step as expected,
        # which results in both states having the same step value. Though the game internally
        # enumerates the steps from 0, we always assumed 1 as the first step number and as such
        # need to increment the new state's step instead.
        if new_game_state is not None:
            new_game_state[State.STEP] += 1

        # Preprocess game states.
        preprocessors: List[StatePreprocessor] = self.preprocessors
        for preprocessor in preprocessors:
            old_game_state = preprocessor.process(self, old_game_state)
            if new_game_state is None:
                continue
            new_game_state = preprocessor.process(self, new_game_state)

        # Get action ID.
        agent: Agent = self.agent
        self_action_id = agent.action_set.get_action_id(self_action)

        # Process events to determine rewards.
        event_processor: EventProcessor = self.event_processor
        reward, rewards_per_source = event_processor.process(
            old_game_state,
            self_action,
            new_game_state,
            events,
        )

        # Process additional agent specific events and rewards.
        reward += agent.process_rewards(
            self,
            old_game_state,
            self_action_id,
            new_game_state,
            reward,
            rewards_per_source,
        )
        self.total_round_reward += reward

        # Log rewards this step.
        self.data_logger.log_data(rewards_per_source.items())
        self.data_logger.log_datum("TOTAL_REWARD", reward)

        # Perform agent optimization step.
        should_optimize = agent.pre_optimize(
            self,
            old_game_state,
            self_action_id,
            new_game_state,
            reward,
            rewards_per_source,
        )
        if not should_optimize:
            return
        agent.optimize_model()
        agent.post_optimize(
            self,
            old_game_state,
            self_action_id,
            new_game_state,
            reward,
            rewards_per_source,
        )

    def end_of_round(
        self: SimpleNamespace,
        last_game_state: Dict[str, Any],
        last_action: str,
        events: List[str],
    ) -> None:
        """Perform a final `game_events_occurred` call, call `agent.on_end_of_round`, and write
        logged data to disk.

        Called every time a the final state of a game is reached.

        Parameters
        ----------
        last_game_state : Dict[str, Any]
            The game state at the end of the game.

        last_action : str
            The last action that was performed by the agent.

        events : List[str]
            The events that occurred during the state transition.
        """
        # Perform normal transition action without next state.
        game_events_occurred(self, last_game_state, last_action, None, events)

        # Update agent and save model.
        agent: Agent = self.agent
        agent.on_end_of_round(last_game_state, last_action, events)
        agent.save_model()

        # Write data to disk.
        data_path = self.data_path
        prefix = f"{self.store_prefix}_" if self.store_prefix != "" else ""
        data_logger: DataLogger = self.data_logger
        data_logger.log_datum(
            "AVG_REWARD",
            self.total_round_reward / State.get_step(last_game_state),
        )
        data_logger.log_datum("STEPS_SURVIVED", State.get_step(last_game_state))
        csv = data_logger.to_csv(every_n=self.log_interval)
        with open(f"{data_path}/{prefix}data.csv", mode="w") as f:
            f.write(csv)

        # Update logger.
        data_logger.next_round()

        # Reset total reward.
        self.total_round_reward = 0

        # Call on_end_of_session if the final game was played.
        current_round = State.get_round(last_game_state)
        if current_round == self.last_round:
            agent.on_end_of_session()

        # Log some status information.
        if current_round % self.log_interval == 0:
            self.logger.info(
                f"Round {current_round}: {agent.name} achieved score of "
                f"{State.get_self_values(last_game_state).score} after "
                f"{agent.rounds_since_creation} rounds, "
                f"{agent.optimization_steps_since_creation} optimization steps, and "
                f"{len(agent.memory)} transitions in memory.",
            )

    return setup_training, game_events_occurred, end_of_round
