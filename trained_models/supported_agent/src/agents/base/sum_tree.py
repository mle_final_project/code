"""This module contains the a sum tree implementation described in the paper "Prioritized Experience
Replay" found under https://arxiv.org/abs/1707.06347.
"""

from typing import Dict, List, Optional, cast


class Node:
    """Node type used in SumTree."""

    NEXT_INDEX = 0

    def __init__(
        self,
        left: Optional["Node"] = None,
        right: Optional["Node"] = None,
        value: float = 0,
    ) -> None:
        self.parent: Optional[Node] = None
        self.left: Optional[Node] = left
        if self.left is not None:
            self.left.parent = self
        self.right: Optional[Node] = right
        if self.right is not None:
            self.right.parent = self
        if Node.is_leaf(self):
            self.value: float = value
            self.index: int = Node.NEXT_INDEX
            Node.NEXT_INDEX += 1
        else:
            self.value: float = Node.get_value(self.left) + Node.get_value(self.right)

    @staticmethod
    def is_leaf(node: Optional["Node"]) -> bool:
        if node is None:
            return False
        return node.left is None and node.right is None

    @staticmethod
    def get_value(node: Optional["Node"]) -> float:
        return 0 if node is None else node.value

    @staticmethod
    def set_next_index(index: int) -> None:
        Node.NEXT_INDEX = index


class SumTree:
    """A tree structure. Each node contains the sum of the values of its child nodes."""

    def __init__(
        self,
        root: Node,
        leafs: List[Node],
    ) -> None:
        self.root = root
        self.leafs = leafs

    def __getstate__(self) -> object:
        return {"leafs": self.leafs, "root": self.root, "next_index": Node.NEXT_INDEX}

    def __setstate__(self, state: Dict[str, Node | List[Node] | int]) -> None:
        self.root = cast(Node, state["root"])
        self.leafs = cast(List[Node], state["leafs"])
        Node.set_next_index(cast(int, state["next_index"]))

    def query(self, value: float) -> Node:
        """Get the node corresponding to the given value.

        Parameters
        ----------
        value : float
            The value to query a leaf node for.

        Returns
        ----------
        node : Node
            The corresponding leaf node.
        """
        def query_internal(value: float, node: Node) -> Node:
            while not Node.is_leaf(node):
                left_priority = Node.get_value(node.left)
                if node.left is not None and left_priority >= value:
                    node = node.left
                elif node.right is not None:
                    value -= left_priority
                    node = node.right
                else:
                    raise RuntimeError("Both children of a non-leaf node are None!")
            return node

        return query_internal(value, self.root)

    def update_by(self, difference: float, node: Optional[Node]) -> None:
        """Iterative the given node's and its parent's values by the given difference.

        Parameters
        ----------
        difference : float
            The difference to update by.

        node : Optional[Node]
            The node to update tree values from.
        """
        while node is not None:
            node.value += difference
            node = node.parent

    def update(self, new_value: float, node: Node) -> None:
        """Iteratively update the given node's and its parent nodes' values.

        Parameters
        ----------
        new_value : float
            The value to update the given node's value to.

        node : Node
            The node to update.
        """
        difference = new_value - node.value
        node.value = new_value
        self.update_by(difference, node.parent)

    @staticmethod
    def create_tree(values: List[float]) -> "SumTree":
        """Create a new sum tree.

        Parameters
        ----------
        values : List[float]
            The values to create a tree from.

        Returns
        ----------
        tree : SumTree
            The created tree.
        """
        nodes = [Node(None, None, value) for value in values]
        leafs = nodes
        while len(nodes) > 1:
            nodes_iter = iter(nodes)
            nodes = [Node(*pair) for pair in zip(nodes_iter, nodes_iter)]
        return SumTree(nodes[0], leafs)
