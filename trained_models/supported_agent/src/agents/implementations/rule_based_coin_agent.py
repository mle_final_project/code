import os
from logging import Logger
from typing import Optional, Union

import numpy as np
import torch

from src.agents import Agent
from src.features import CoinInfluenceFeatureExtractor
from src.info.actions import Actions, ActionSet


class RuleBasedCoinAgent(Agent):
    """An agent that always takes the highest coin gradient value as next step."""

    def __init__(
        self,
        feature_extractor: CoinInfluenceFeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        memory_size: int = 10000,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
    ) -> None:
        has_correct_extractor = isinstance(feature_extractor, CoinInfluenceFeatureExtractor)
        wrong_feature_extractor_msg = "This agent only works with CoinInfluenceFeatureExtractor!"
        assert has_correct_extractor, wrong_feature_extractor_msg
        super().__init__(
            feature_extractor=feature_extractor,
            action_set=action_set,
            device=device,
            memory_size=memory_size,
            do_auto_save=do_auto_save,
            save_interval=save_interval,
            save_path=save_path,
            logger=logger,
        )
        if feature_extractor.include_diagonals:
            self.log_message("This agent does not work with diagonals. Disabling them.")
            feature_extractor.include_diagonals = False
        self.last_action = Actions.INVALID_NAME

    def _select_action_implementation(
        self,
        features: torch.Tensor,
        _current_round: int,
        _current_step: int,
    ) -> int:
        """Move towards largest gradient value."""
        index = np.argmax(features[0]).item()
        action_id = Actions.INVALID_ID
        if index == 0:
            action_id = self.action_set.get_action_id(Actions.WAIT)
        if index == 1:
            action_id = self.action_set.get_action_id(Actions.LEFT)
        if index == 2:
            action_id = self.action_set.get_action_id(Actions.RIGHT)
        if index == 3:
            action_id = self.action_set.get_action_id(Actions.UP)
        if index == 4:
            action_id = self.action_set.get_action_id(Actions.DOWN)
        action = self.action_set.get_action_name(action_id)
        self.last_action = action
        return action_id
