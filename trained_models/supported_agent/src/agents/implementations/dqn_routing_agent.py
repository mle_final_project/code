from collections import deque  # noqa: I001
import os
from logging import Logger
from types import SimpleNamespace
from typing import Any, Deque, Dict, Optional, Tuple, Union

import torch
from torch import Tensor

from src.agents import DQNAgent
from src.features import CoinInfoFeatureExtractor
from src.info.actions import ActionSet
from src.info.state import State
from src.utility import utility
from src.info.logging import DataLogger


class DQNRoutingAgent(DQNAgent):
    DISTANCE_PENALTY = "DISTANCE_PENALTY"
    SELECTED_COLLECTED = "SELECTED_COLLECTED"

    class TargetData:
        """Holds data related to a single routing target."""

        def __init__(
            self,
            index: int,
            position: Tuple[int, int],
            distance: int,
            is_collected: bool,
            accumulated_path_reward: float,
            accumulated_path_reward_sources: Dict[str, float],
            previous_game_state: Dict[str, Any],
        ) -> None:
            self.index = index
            self.position = position
            self.distance = distance
            self.is_collected = is_collected
            self.accumulated_path_reward = accumulated_path_reward
            self.accumulated_path_reward_sources = accumulated_path_reward_sources
            self.previous_game_state = previous_game_state

    def __init__(
        self,
        feature_extractor: CoinInfoFeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        batch_size: int,
        gamma: float,
        eps_start: float,
        eps_end: float,
        eps_decay: float,
        tau: float,
        lr: float,
        n_inner: int,
        n_hidden: int = 3,
        memory_size: int = 10000,
        do_random_choices: bool = True,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
        data_logger: Optional[DataLogger] = None,
        name: Optional[str] = None,
    ) -> None:
        # Ensure we use the correct feature extractor.
        wrong_extractor_msg = "This agent requires a CoinInfoFeatureExtractor!"
        assert isinstance(feature_extractor, CoinInfoFeatureExtractor), wrong_extractor_msg

        # Create internal DQN agent that determines the order in which coins are collected.
        super().__init__(
            feature_extractor=feature_extractor,
            action_set=action_set,
            device=device,
            batch_size=batch_size,
            gamma=gamma,
            eps_start=eps_start,
            eps_end=eps_end,
            eps_decay=eps_decay,
            tau=tau,
            lr=lr,
            n_inner=n_inner,
            n_hidden=n_hidden,
            n_out=feature_extractor.n_coins,
            memory_size=memory_size,
            do_random_choices=do_random_choices,
            do_auto_save=do_auto_save,
            save_interval=save_interval,
            save_path=save_path,
            logger=logger,
            data_logger=data_logger,
            name=name,
        )

        # Initialize state variables.
        self.feature_extractor: CoinInfoFeatureExtractor = feature_extractor
        self.target_data: Deque[DQNRoutingAgent.TargetData] = deque([], 2)
        self.last_action_game_state: Dict[str, Any] = {}
        self.reached_target_in_last_step: bool = False

    def process_rewards(
        self,
        _self_namespace: SimpleNamespace,
        _old_game_state: Dict[str, Any],
        _self_action: str,
        _new_game_state: Dict[str, Any] | None,
        total_reward: float,
        reward_per_source: Dict[str, float],
    ) -> float:
        """Track rewards while moving along computed path.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : int
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in final state transition.

        total_reward : float
            The total reward computed for this transition.

        reward_per_source : Dict[str, float]
            The individual rewards per processed event.

        Returns
        ----------
        additional_reward : float
            The total additional reward to add. For this agent, this is always equal to 0.
        """
        data_index = 0 if self.reached_target_in_last_step else -1
        target_data = self.target_data[data_index]
        target_data.accumulated_path_reward += total_reward
        for key, value in reward_per_source.items():
            current_value = target_data.accumulated_path_reward_sources.get(key, 0)
            target_data.accumulated_path_reward_sources[key] = current_value + value

        additional_reward = 0
        if not self.reached_target_in_last_step:
            return 0

        target_data = self.target_data[0]

        # Apply distance penalty.
        distance_penalty = -0.1 * target_data.distance
        target_data.accumulated_path_reward += distance_penalty
        target_data.accumulated_path_reward_sources[self.DISTANCE_PENALTY] = distance_penalty
        if self.data_logger is not None:
            self.data_logger.log_datum(self.DISTANCE_PENALTY, distance_penalty)

        # Apply penalty for choosing already collected coins.
        if target_data.is_collected:
            collected_penalty = -1
            target_data.accumulated_path_reward += collected_penalty
            target_data.accumulated_path_reward_sources[self.SELECTED_COLLECTED] = collected_penalty
            reward_per_source[self.SELECTED_COLLECTED] = collected_penalty
            additional_reward += collected_penalty

        return additional_reward

    def pre_optimize(
        self,
        _self_namespace: SimpleNamespace,
        old_game_state: Dict[str, Any],
        _self_action: int,
        _new_game_state: Dict[str, Any] | None,
        _total_reward: float,
        _reward_per_source: Dict[str, float],
    ) -> bool:
        """Only optimize if the agent did reach its current target position. If so, also push state
        transition into memory.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : int
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in final state transition.

        total_reward : float
            The total reward computed for this transition.

        reward_per_source : Dict[str, float]
            The individual rewards per processed event.

        Returns
        ----------
        should_optimize : bool
            If `False`, do not optimize the model in this step.
        """
        if not self.reached_target_in_last_step:
            return False

        target_data = self.target_data[0]

        # Push state transition into memory.
        return super().pre_optimize(
            _self_namespace,
            target_data.previous_game_state,
            target_data.index,
            old_game_state,
            target_data.accumulated_path_reward,
            target_data.accumulated_path_reward_sources,
        )

    def preprocess_features(self, features: Tensor, game_state: Dict[str, Any]) -> Tensor:
        self.last_action_game_state = game_state
        return features

    def _select_action_implementation(
        self,
        features: Tensor,
        current_round: int,
        current_step: int,
    ) -> int:
        # Check if the agent reached its target position and update accordingly.
        agent_position = self.feature_extractor.features_to_agent_position(features)
        if current_step == 1 or agent_position == self.target_data[-1].position:
            self.previous_game_state = self.last_action_game_state
            self.reached_target_in_last_step = current_step != 1
            target_index = super()._select_action_implementation(
                features,
                current_round,
                current_step,
            )
            coin_info = self.feature_extractor.features_to_coin_info_by_index(
                target_index,
                features,
            )
            assert coin_info is not None, "Selected invalid index!"
            target_data = DQNRoutingAgent.TargetData(
                target_index,
                coin_info.position,
                coin_info.distance_to_agent,
                coin_info.is_collected,
                0,
                {},
                self.last_action_game_state,
            )
            self.target_data.append(target_data)
            if self.data_logger is not None:
                if coin_info.is_collected:
                    self.data_logger.log_datum("SELECTED_INVALID_TARGET", 1)
                else:
                    self.data_logger.log_datum("SELECTED_VALID_TARGET", 1)
        else:
            self.reached_target_in_last_step = False

        # Determine next move by shortest path.
        target_position = self.target_data[-1].position
        tile_map = State.get_field(self.last_action_game_state)
        shortest_path_to_target = utility.shortest_path_bfs(
            agent_position,
            [target_position],
            tile_map,
        )[target_position]
        if len(shortest_path_to_target) > 1:
            next_position = shortest_path_to_target[1]
        else:
            next_position = target_position
        action = utility.adjacent_positions_to_movement_action(agent_position, next_position)

        return self.action_set.get_action_id(action)
