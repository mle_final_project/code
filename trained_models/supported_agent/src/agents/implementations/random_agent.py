import os
import random
from logging import Logger
from typing import Optional, Union

import torch

from src.agents import Agent
from src.features import FeatureExtractor
from src.info.actions import ActionSet


class RandomAgent(Agent):
    """An agent that simply performs random actions."""

    def __init__(
        self,
        feature_extractor: FeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        memory_size: int = 10000,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
    ) -> None:
        super().__init__(
            feature_extractor,
            action_set,
            device,
            memory_size,
            do_auto_save,
            save_interval,
            save_path,
            logger,
        )

    def _select_action_implementation(
        self,
        _state: torch.Tensor,
        _current_round: int,
        _current_step: int,
    ) -> int:
        """Perform a random action."""
        return random.randint(0, self.action_set.n_actions() - 1)
