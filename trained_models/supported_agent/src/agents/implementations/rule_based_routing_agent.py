import os
from logging import Logger
from typing import Any, Dict, Optional, Tuple, Union

import torch
from torch import Tensor

from src.agents import Agent
from src.features import CoinInfoFeatureExtractor
from src.features.implementations.coin_info import CoinInfo
from src.info.actions import Actions, ActionSet
from src.info.logging import DataLogger
from src.info.state import State
from src.utility import utility


class RuleBasedRoutingAgent(Agent):
    """Rule based routing agent that always selects the route to the nearest coin."""

    class TargetData:
        """Holds data related to a single routing target."""

        def __init__(
            self,
            index: int,
            position: Tuple[int, int],
            distance: int,
            is_collected: bool,
        ) -> None:
            self.index = index
            self.position = position
            self.distance = distance
            self.is_collected = is_collected

    def __init__(
        self,
        feature_extractor: CoinInfoFeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        memory_size: int = 10000,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
        data_logger: Optional[DataLogger] = None,
        name: Optional[str] = None,
    ) -> None:
        super().__init__(
            feature_extractor=feature_extractor,
            action_set=action_set,
            device=device,
            memory_size=memory_size,
            do_auto_save=do_auto_save,
            save_interval=save_interval,
            save_path=save_path,
            logger=logger,
            data_logger=data_logger,
            name=name,
        )
        # Ensure we use the correct feature extractor.
        wrong_extractor_msg = "This agent requires a CoinInfoFeatureExtractor!"
        assert isinstance(feature_extractor, CoinInfoFeatureExtractor), wrong_extractor_msg
        self.feature_extractor: CoinInfoFeatureExtractor = feature_extractor
        self.target_data: Optional[RuleBasedRoutingAgent.TargetData] = None
        self.last_action_game_state: Dict[str, Any] = {}

    def preprocess_features(self, features: Tensor, game_state: Dict[str, Any]) -> Tensor:
        self.last_action_game_state = game_state
        return features

    def _select_action_implementation(
        self,
        features: Tensor,
        _current_round: int,
        current_step: int,
    ) -> int:
        # Check if the agent reached its target position and update accordingly.
        agent_position = self.feature_extractor.features_to_agent_position(features)
        switch_target = current_step == 1
        if self.target_data is not None:
            switch_target = switch_target or agent_position == self.target_data.position
        if switch_target:
            smallest_coin_index: int = -1
            smallest_coin_info: Optional[CoinInfo] = None
            for coin_index in range(self.feature_extractor.n_coins):
                coin_info = self.feature_extractor.features_to_coin_info_by_index(
                    coin_index,
                    features,
                )
                if coin_info is None or coin_info.is_collected:
                    continue
                if smallest_coin_info is None:
                    smallest_coin_index = coin_index
                    smallest_coin_info = coin_info
                    continue
                if smallest_coin_info.distance_to_agent <= coin_info.distance_to_agent:
                    continue
                smallest_coin_index = coin_index
                smallest_coin_info = coin_info
            if smallest_coin_info is None:
                self.target_data = None
            else:
                self.target_data = RuleBasedRoutingAgent.TargetData(
                    smallest_coin_index,
                    smallest_coin_info.position,
                    smallest_coin_info.distance_to_agent,
                    smallest_coin_info.is_collected,
                )

        # If we collected all coins we are done.
        if self.target_data is None:
            return self.action_set.get_action_id(Actions.WAIT)

        # Determine next move by shortest path.
        target_position = self.target_data.position
        tile_map = State.get_field(self.last_action_game_state)
        shortest_path_to_target = utility.shortest_path_bfs(
            agent_position,
            [target_position],
            tile_map,
        )[target_position]
        if len(shortest_path_to_target) > 1:
            next_position = shortest_path_to_target[1]
        else:
            next_position = target_position
        action = utility.adjacent_positions_to_movement_action(agent_position, next_position)

        return self.action_set.get_action_id(action)
