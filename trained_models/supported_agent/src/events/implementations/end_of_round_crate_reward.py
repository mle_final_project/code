from typing import Any, Dict, List, Optional

from src.events.base.base_events import BaseEvents
from src.events.base.event import Event, Reward
from src.info.state import State


class EndOfRoundCrateRewardEvent(Event):
    """Event that rewards the agent for the collected number of crates destroyed at the end of a
    round.
    """

    def __init__(
        self,
        base_reward: float = 1,
        name: Optional[str] = None,
    ) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The reward value to use as base value for the event. Used as value per destroyed crate.

        name : Optional[str]
            The event name to use. If None, use the class name.
        """
        super().__init__(base_reward, name)
        self.destroyed_crates_this_round = 0

    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Optional[Dict[str, Any]],
        _self_action: str,
        events: List[str],
    ) -> Reward:
        """Reward the agent for the number of crates destroyed at the end of a round.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        new_game_state : Optional[Dict[str, Any]]
            The game state after the transition. Is `None` in last state transition.

        self_action : str
            The name of the action performed by the agent.

        events : List[str]
            The base game events that took place in the given transition.

        Returns
        ----------
        reward : Reward
            The reward the agent should get for this reward. None if the event did not take place
            during the given transition.
        """
        current_step = State.get_step(old_game_state)
        if current_step == 1:
            self.destroyed_crates_this_round = 0
        if BaseEvents.CRATE_DESTROYED in events:
            self.destroyed_crates_this_round += 1
        if new_game_state is not None:
            return None
        return self.destroyed_crates_this_round * self.base_reward
