from typing import Any, Dict, List, Optional

from src.events import BaseEvents, Event, Reward
from src.info.state import State


class CoinCollectedEarlyEvent(Event):
    """Reward the agent if it collected coins in early steps."""

    def __init__(
        self,
        base_reward: float = 1,
        name: Optional[str] = None,
        step_multiplier: float = 0.99,
    ) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The base penalty (negative reward) to use.

        name : Optional[str]
            The event name to use. If None, use the class name.

        step_multiplier : int
            The reduction of the reward per step.
        """
        super().__init__(base_reward, name)
        self.step_multiplier = step_multiplier

    def calculate_reward(
            self,
            old_game_state: Dict[str, Any],
            _new_game_state: Optional[Dict[str, Any]],
            _self_action: str,
            events: List[str],
    ) -> Reward:
        """Reward the agent if it collected coins in early steps.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        new_game_state : Optional[Dict[str, Any]]
            The game state after the transition. Is `None` in last state transition.

        self_action : str
            The name of the action performed by the agent.

        events : List[str]
            The base game events that took place in the given transition.

        Returns
        ----------
        reward : Reward
            The reward the agent should get for this reward. None if the event did not take place
            during the given transition.
        """
        if not BaseEvents.COIN_COLLECTED in events:
            return None
        steps = State.get_step(old_game_state)
        return self.base_reward * self.step_multiplier**steps
