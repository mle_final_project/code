from types import SimpleNamespace
from typing import Any, Dict, List, Optional

import numpy as np

from src.events.base.event import Event, Reward
from src.features.implementations.interest_rating import InterestRatingFeatureExtractor
from src.info.actions import Actions


class FollowedInterestRatingEvent(Event):
    def __init__(
        self,
        interest_rating: InterestRatingFeatureExtractor,
        base_reward: float = 1.0,
        name: Optional[str] = None,
    ) -> None:
        super().__init__(base_reward, name)
        self.interest_rating = interest_rating

    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Optional[Dict[str, Any]],
        self_action: str,
        _events: List[str],
    ) -> Reward:
        if new_game_state is None:
            return None
        old_features = self.interest_rating.extract_features(
            SimpleNamespace(),
            old_game_state,
        ).numpy()
        max_index = np.argmax(old_features, axis=1)
        if self_action == Actions.LEFT:
            return self.base_reward if max_index == 1 else -self.base_reward
        if self_action == Actions.RIGHT:
            return self.base_reward if max_index == 2 else -self.base_reward
        if self_action == Actions.UP:
            return self.base_reward if max_index == 3 else -self.base_reward
        if self_action == Actions.DOWN:
            return self.base_reward if max_index == 4 else -self.base_reward
        return None
