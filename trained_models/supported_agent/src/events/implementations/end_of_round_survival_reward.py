from typing import Any, Dict, List, Optional

from src.events.base.event import Event, Reward
from src.info.state import State


class EndOfRoundSurvivalRewardEvent(Event):
    """Rewards the agent for each survived step at the end of a round."""

    def __init__(
        self,
        base_reward: float = 1,
        name: Optional[str] = None,
    ) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The reward value to use as base value for the event.

        name : Optional[str]
            The event name to use. If None, use the class name.
        """
        super().__init__(base_reward, name)

    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Optional[Dict[str, Any]],
        _self_action: str,
        _events: List[str],
    ) -> Reward:
        if new_game_state is not None:
            return None
        return self.base_reward * State.get_step(old_game_state)
