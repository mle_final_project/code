from types import SimpleNamespace
from typing import Any, Dict, List, Optional

from src.events import BaseEvents, Event, Reward
from src.features.implementations.crates_in_range import CratesInBombRangeFeatureExtractor
from src.utility import utility


class PlacedBombInRangeOfCrateEvent(Event):
    """Reward the agent if it placed a bomb in range of a crate."""

    def __init__(
        self,
        base_reward: float = 1,
        max_abs_reward: float = 12,
        name: str | None = None,
    ) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The reward value to use as base value for the event.

        max_abs_reward : float
            The maximal absolut reward to give per transition.

        name : Optional[str]
            The event name to use. If None, use the class name.
        """
        super().__init__(base_reward, name)
        self.max_abs_reward = max_abs_reward
        self.feature_extractor = CratesInBombRangeFeatureExtractor(
            normalize=False,
            cache_features=True,
        )

    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        _new_game_state: Optional[Dict[str, Any]],
        _self_action: str,
        events: List[str],
    ) -> Reward:
        """Reward the agent if it placed a bomb in range of a crate. The base reward is multiplied
        by the number of crates in range.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        new_game_state : Optional[Dict[str, Any]]
            The game state after the transition. Is `None` in last state transition.

        self_action : str
            The name of the action performed by the agent.

        events : List[str]
            The base game events that took place in the given transition.

        Returns
        ----------
        reward : Reward
            The reward the agent should get for this reward. None if the event did not take place
            during the given transition.
        """
        if BaseEvents.BOMB_DROPPED not in events:
            return None
        crates_in_range = self.feature_extractor.extract_features(
            SimpleNamespace(),
            old_game_state,
        ).item()
        if crates_in_range == 0:
            return None
        return utility.clamp(
            self.base_reward * crates_in_range,
            -self.max_abs_reward,
            self.max_abs_reward,
        )
