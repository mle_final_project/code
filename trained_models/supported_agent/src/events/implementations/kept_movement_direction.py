from typing import Any, Callable, Dict, List, Optional

from src.events import BaseEvents, Event, Reward
from src.info.actions import Actions
from src.utility import utility


class KeptMovementDirectionEvent(Event):
    """Reward the agent if it is moving in the same direction over multiple turns."""

    def __init__(
        self,
        base_reward: float = 1,
        name: Optional[str] = None,
        round_multiplier_func: Callable[[float], float] = lambda _: 1,
        reduction_per_round: float = 100,
        max_abs_reward: float = 10,
    ) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The base reward to use.

        name : Optional[str]
            The event name to use. If None, use the class name.

        round_multiplier_func : Callable[[int], int]
            The function to apply to the round counter to determine the penalty value multiplier to
            use.

        reduction_per_round : int
            The reduction of the round counter each turn the event does not apply.

        max_abs_reward : float
            The maximum absolute reward to give.
        """
        super().__init__(base_reward, name)
        self.round_multiplier_func = round_multiplier_func
        self.reduction_per_round = reduction_per_round
        self.max_abs_reward = max_abs_reward
        self.last_action = ""
        self.active_rounds = 0

    def calculate_reward(
        self,
        _old_game_state: Dict[str, Any],
        _new_game_state: Optional[Dict[str, Any]],
        self_action: str,
        events: List[str],
    ) -> Reward:
        """Reward the agent if it is moving in the same direction over multiple turns.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        new_game_state : Optional[Dict[str, Any]]
            The game state after the transition. Is `None` in last state transition.

        self_action : str
            The name of the action performed by the agent.

        events : List[str]
            The base game events that took place in the given transition.

        Returns
        ----------
        reward : Reward
            The reward the agent should get for this reward. None if the event did not take place
            during the given transition.
        """
        last_action = self.last_action
        self.last_action = self_action
        if BaseEvents.INVALID_ACTION in events:
            return None  # Do not reward invalid action spam!
        if last_action == self_action and self_action != Actions.WAIT:
            self.active_rounds += 1
            reward = self.base_reward * self.round_multiplier_func(self.active_rounds)
            return utility.clamp(reward, -self.max_abs_reward, self.max_abs_reward)
        self.active_rounds = max(self.active_rounds - self.reduction_per_round, 0)
        return None
