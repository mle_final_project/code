from typing import Any, Dict, List, Optional, Tuple

from src.events import BaseEvents, Event


class EventProcessor:
    """Evaluates custom and base game events and computes rewards."""

    def __init__(
        self,
        base_events: Optional[List[Tuple[str, Optional[float]]]] = None,
        custom_events: Optional[List[Event]] = None,
    ) -> None:
        """Initialize an `EventManager` instance.

        Parameters
        ----------
        base_events : Optional[List[Tuple[str, Optional[float]]]]
            Use to only listen for a number of specific base events. If a `float` value is provided,
            override the event's reward in the `BaseEvents.REWARD_MAP`. If None, use all base events
            available instead.

        custom_events : Optional[List[Events]]
            Use to only listen for a number of specific custom events. If None, use all `Event`
            subclasses instead.
        """
        if custom_events is not None:
            self.custom_events = custom_events
        else:
            self.custom_events = [event() for event in Event.__subclasses__()]  # type: ignore
        if base_events is not None:
            base_event_names = []
            for event_name, reward in base_events:
                base_event_names.append(event_name)
                if reward is None:
                    continue
                self.update_base_reward(event_name, reward)
        else:
            base_event_names = BaseEvents.all_events()
        self.event_names = base_event_names + [event.name for event in self.custom_events]

    def update_base_reward(self, event_name: str, new_base_reward: float) -> None:
        """Update the `base_reward` value of the event with the given name.

        Parameters
        ----------
        event_name : str
            The name of the event to update.

        new_base_reward : float
            The new base reward to give.
        """
        if BaseEvents.is_base_event(event_name):
            BaseEvents.REWARD_MAP[event_name] = new_base_reward
            return
        for event in self.custom_events:
            if event.name != event_name:
                continue
            event.base_reward = new_base_reward
            return

    def process(
        self,
        old_game_state: Dict[str, Any],
        self_action: str,
        new_game_state: Optional[Dict[str, Any]],
        game_events: List[str],
    ) -> Tuple[float, Dict[str, float]]:
        """Evaluate game and custom events based on the given state transition and store results.

        If `do_save_rewards` is `True`, save rewards at end of round.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        action : str
            The name of the action performed by the agent.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the transition. Is `None` in last step.

        game_events : List[str]
            A list of events provided by the game that were performed during the transition.

        Returns
        ----------
        rewards : Tuple[float, Dict[str, float]]
            The total and individual reward(s) for this state transition.
        """
        total_reward = 0
        event_rewards = {}

        # Get base event rewards
        for event_name in game_events:
            if event_name not in self.event_names:
                continue
            reward = BaseEvents.REWARD_MAP[event_name]
            total_reward += reward
            event_rewards[event_name] = reward

        # Get custom event rewards
        for event in self.custom_events:
            reward = event.calculate_reward(
                old_game_state,
                new_game_state,
                self_action,
                game_events,
            )
            if reward is None:
                continue
            total_reward += reward
            event_rewards[event.name] = reward

        return total_reward, event_rewards
