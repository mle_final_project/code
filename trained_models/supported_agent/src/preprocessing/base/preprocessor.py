from abc import ABC, abstractmethod
from collections import deque
from types import SimpleNamespace
from typing import Any, Deque, Dict, Optional

from src.info.state import State


class StatePreprocessor(ABC):
    """Base class for implementing a state preprocessor.

    Preprocessing is used to filter or expand a given game state dictionary before further
    processing takes place.
    """

    def __init__(self, cache_states: bool = True) -> None:
        """Initialize preprocessor instance.

        Parameters
        ----------
        cache_states : bool
            If `True`, cache the last two processed states to speed up multiple processing calls
            during the same step.
        """
        self.do_cache_states = cache_states
        self.last_cached_round = 0
        self.last_cached_step = -2
        self.cached_states: Deque[Dict[str, Any]] = deque([], maxlen=2)

    def process(
        self,
        self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Dict[str, Any]:
        """Process the given game state.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        game_state : Dict[str, Any]
            The game state to process.

        Returns
        ----------
        processed_game_state : Dict[str, Any]
            The processed game state.
        """
        current_round = State.get_round(game_state)
        current_step = State.get_step(game_state)

        def get_cached() -> Optional[Dict[str, Any]]:
            if current_round != self.last_cached_round:
                return None
            if current_step == self.last_cached_step:
                return self.cached_states[-1]
            if current_step == self.last_cached_step - 1:
                if len(self.cached_states) < 2:
                    return None
                return self.cached_states[-2]
            return None

        # Return cached features if possible
        if self.do_cache_states:
            state = get_cached()
            if state is not None:
                return state

        # Else extract new ones
        state = self._process_implementation(self_namespace, game_state)

        # Update feature cache
        if self.do_cache_states:
            self.last_cached_round = current_round
            self.last_cached_step = current_step
            self.cached_states.append(state)
        return game_state

    @abstractmethod
    def _process_implementation(
        self,
        self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Dict[str, Any]:
        """Process the given game state.

        Override to add desired behavior.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        game_state : Dict[str, Any]
            The game state to process.

        Returns
        ----------
        processed_game_state : Dict[str, Any]
            The processed game state.
        """
        pass
