from types import SimpleNamespace
from typing import Any, Dict, Tuple

import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.features.implementations.coin_influence import CoinInfluenceFeatureExtractor
from src.features.implementations.crate_influence import CrateInfluenceFeatureExtractor
from src.features.implementations.enemy_influence import EnemyInfluenceFeatureExtractor
from src.utility import utility


class InterestRatingFeatureExtractor(FeatureExtractor):
    def __init__(
        self,
        coin_influence: CoinInfluenceFeatureExtractor,
        crate_influence: CrateInfluenceFeatureExtractor,
        enemy_influence: EnemyInfluenceFeatureExtractor,
        coin_multiplier: float = 2,
        crate_multiplier: float = 1,
        enemy_multiplier: float = 1,
        normalize: bool = True,
        cache_features: bool = True,
    ) -> None:
        """Initialize a new feature extractor instance.

        Parameters
        ----------
        coin_influence : CoinInfluenceFeatureExtractor
            The extractor to use for coins.

        crate_influence : CrateInfluenceFeatureExtractor
            The extractor to use for crates.

        enemy_influence : EnemyInfluenceFeatureExtractor
            The extractor to use for enemies.

        coin_multiplier : float
            The value to multiply coin influences with.

        crate_multiplier : float
            The value to multiply crate influences with.

        enemy_multiplier : float
            The value to multiply enemy influences with.

        normalize : bool
            Should the values be normalized to [-1, 1]?

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        FeatureExtractor.__init__(self, cache_features)
        n_coins = coin_influence.n_features()
        n_crates = crate_influence.n_features()
        n_enemies = enemy_influence.n_features()
        assert n_coins == n_crates == n_enemies
        assert not coin_influence.include_diagonals, "Only supports without diagonals for now."
        self.coins = coin_influence
        self.crates = crate_influence
        self.enemies = enemy_influence
        self.normalize = normalize
        self.coin_multiplier = coin_multiplier
        self.crate_multiplier = crate_multiplier
        self.enemy_multiplier = enemy_multiplier

    def n_features(self) -> Tuple[int, ...]:
        """Get the number of features this extractor produces.

        Returns
        ----------
        n_features : Tuple[int, int]
            The feature dimensions.
        """
        return self.coins.n_features()

    def _extract_features_implementation(
        self,
        self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        coin_influence = self.coins.extract_features(self_namespace, game_state).numpy()
        crate_influence = self.crates.extract_features(self_namespace, game_state).numpy()
        enemy_influence = self.enemies.extract_features(self_namespace, game_state).numpy()
        combined = (
            self.coin_multiplier * coin_influence
            + self.crate_multiplier * crate_influence
            + self.enemy_multiplier * enemy_influence
        )[0]
        if self.normalize:
            combined = utility.remap_values_to_range(combined, -1, 1)
        return torch.tensor(combined, dtype=torch.float32).unsqueeze(0)
