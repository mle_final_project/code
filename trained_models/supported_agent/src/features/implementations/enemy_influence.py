from types import SimpleNamespace
from typing import Any, Callable, Dict, Tuple

import numpy as np
import torch
from src.features.base.feature_extractor import FeatureExtractor
from src.info.state import State
from src.utility import utility
from torch import Tensor


class EnemyInfluenceFeatureExtractor(FeatureExtractor):
    """Let enemies 'emit' decreasing values along the shortest path to the agent and return the
    value map created that way as features.
    """

    def __init__(
        self,
        base_value: float = 1.0,
        min_value: float = 0.001,
        max_value: float = 2.0,
        influence_range: int = 17 * 2,
        closest_bias: float = 0.99,
        random_eps: float = 0.0001,
        value_decrease_func: Callable[[int], float] = lambda x: 0.5 * x,
        radius: int = 1,
        include_diagonals: bool = False,
        normalize: bool = True,
        cache_features: bool = True,
    ) -> None:
        """Initialize a new feature extractor instance.

        Parameters
        ----------
        base_value : float
            The starting value of an enemy.

        min_value : float
            The minimum value tiles along a shortest path get per enemy.

        max_value : float
            The maximum value tiles along a shortest path can get per enemy.

        influence_range : int
            The range over which enemies should spread values at most.

        closest_bias : float
            Determines how much the closest enemy dominates the resulting field values.

        random_eps : float
            Maximum factor to randomly offset values by.

        value_decrease_func : Callable[[int], float]
            The function used to determine the value decrease of an enemy over distance.

        radius : int
            The radius around the agent to include in the resulting feature array.

        include_diagonals : bool
            Should diagonals be included?

        normalize : bool
            Should the values be normalized to [-1, 1]?

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.base_value = base_value
        self.min_value = min_value
        self.max_value = max_value
        self.influence_range = influence_range
        self.closest_bias = closest_bias
        self.value_decrease_func = value_decrease_func
        self.radius = radius
        self.random_eps = random_eps
        self.include_diagonals = include_diagonals
        self.normalize = normalize

        # Pre-compute values.
        self.influence_values = np.array(
            [
                max(self.base_value - self.value_decrease_func(i), self.min_value)
                for i in range(self.influence_range)
            ],
            dtype=np.float32,
        )

    def n_features(self) -> Tuple[int, ...]:
        """Get the number of features this extractor produces.

        Returns
        ----------
        n_features : Tuple[int, int]
            The feature dimensions.
        """
        if self.include_diagonals:
            size = 2 * self.radius + 1
            return (1, size * size)
        return (1, 4 * self.radius + 1)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Let enemies 'emit' decreasing values along the shortest path to the agent and return the
        value map created that way as features.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace information.

        game_state : Dict[str, Any]
            The game state.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        # Get state info to use.
        agent_position = State.get_self_values(game_state).position
        enemy_values = State.get_other_values(game_state)
        enemy_positions = [enemy.position for enemy in enemy_values]
        tile_map = State.get_field(game_state)
        shortest_paths = utility.shortest_path_bfs(agent_position, enemy_positions, tile_map, False)

        # Create influence field.
        influence_field = np.zeros_like(tile_map, dtype=np.float32)
        sorted_paths = list(shortest_paths.values())
        sorted_paths.sort(key=len)
        for path in sorted_paths:
            if len(path) == 1:
                continue
            length = min(self.influence_range, len(path) - 1)
            path_indices = [*zip(*path[:-length - 1:-1])]
            influence_field[*path_indices] += np.where(
                influence_field[*path_indices] == 0,
                self.influence_values[:length],
                (1 - self.closest_bias) * self.influence_values[:length],
            )

        # Reduce to desired range.
        influence_field = utility.get_surrounding_tiles(
            influence_field,
            agent_position,
            self.radius,
            self.include_diagonals,
        ).flatten()

        # Clamp to maximum value.
        # influence_field = np.clip(influence_field, 0, self.max_value)

        # Add some random offsets to values to make ties less likely.
        nonzero_mask = np.nonzero(influence_field)
        if len(nonzero_mask) > 0:
            random_offset_values = np.random.random(size=influence_field.shape) * self.random_eps
            influence_field[nonzero_mask] += random_offset_values[nonzero_mask]

        # Normalize if desired.
        if self.normalize:
            influence_field = utility.remap_values_to_range(
                values=influence_field,
                map_to_min_value=0,
                map_to_max_value=1,
                min_value=0,
                max_value=self.max_value,
            )

        return torch.tensor(influence_field, dtype=torch.float32).unsqueeze(0)
