from types import SimpleNamespace
from typing import Any, Dict, Tuple

import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info import State, World


class AgentPositionFeatureExtractor(FeatureExtractor):
    """A feature extractor that extracts the agent's position as feature."""

    def __init__(
        self,
        use_position_index: bool = False,
        cache_features: bool = True,
    ) -> None:
        """Initialize extractor.

        Parameters
        ----------
        use_position_index : bool
            If `True`, return the position as index instead of (x,y) coordinates.

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.use_position_index = use_position_index

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1) if self.use_position_index else (1, 2)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        x, y = State.get_self_values(game_state).position
        if self.use_position_index:
            index = World.position_to_index((x, y))
            return torch.tensor([index], dtype=torch.float32).unsqueeze(0)
        return torch.tensor([x, y], dtype=torch.float32).unsqueeze(0)
