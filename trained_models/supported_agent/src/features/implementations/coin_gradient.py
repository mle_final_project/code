from types import SimpleNamespace
from typing import Any, Dict, Tuple

import numpy as np
import numpy.typing as npt
import torch
from scipy import ndimage
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info import State, Tiles
from src.utility import utility


def create_coin_gradient(
    tile_map: npt.NDArray[np.int16],
    coin_positions: npt.NDArray[np.int16],
) -> npt.NDArray[np.float32]:
    """TODO
    """
    # Initialize gradient array.
    coin_gradient = np.zeros_like(tile_map, dtype=np.float32)
    coin_gradient[*coin_positions.T] = 1

    # Kernel to use for the following convolutions. We want to make the value of coins spread over
    # neighboring tiles but reduce in strength over distance.
    kernel = [
        [0.5, 0.8, 0.5],
        [0.8, 1.0, 0.8],
        [0.5, 0.8, 0.5],
    ]

    # Convolve as long as we still have zeros.
    while not np.all(coin_gradient):
        coin_gradient = ndimage.convolve(
            input=coin_gradient,
            weights=kernel,
            mode="constant",
            cval=0,
        )

    # Zero out non-walkable tile values.
    coin_gradient[tile_map == Tiles.WALL] = -1

    return coin_gradient.astype(np.float32)


class CoinGradientFeatureExtractor(FeatureExtractor):
    """A feature extractor that extracts a coin gradient as feature."""

    def __init__(
        self,
        radius: int = 1,
        normalize: bool = True,
        include_diagonals: bool = False,
        cache_features: bool = True,
    ) -> None:
        """Initialize extractor.

        Parameters
        ----------
        radius : int
            The radius around the agent to extract as feature.

        normalize : bool
            If `True`, normalize the gradients to [-1, 1].

        include_diagonals : bool
            If `False`, only use up/down/left/right/center directions.

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.radius = radius
        self.normalize = normalize
        self.include_diagonals = include_diagonals

    def n_features(self) -> Tuple[int, ...]:
        if self.include_diagonals:
            size = 2 * self.radius
            return (1, size * size)
        return (1, 4 * self.radius)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract all world tiles as features.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state.

        self_namespace : SimpleNamespace
            The game's namespace information.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        # Create initial gradient and mark agent position
        tile_map = State.get_field(game_state)
        coin_positions = State.get_coins_numpy(game_state)
        agent_position = State.get_self_values(game_state).position
        coin_gradient = create_coin_gradient(tile_map, coin_positions)
        coin_gradient[agent_position] = -100

        # Get surrounding as configured on initialization
        coin_gradient = utility.get_surrounding_tiles(
            tile_map=coin_gradient,
            position=agent_position,
            radius=self.radius,
            include_diagonals=self.include_diagonals,
        )

        # Remove center tile
        coin_gradient = coin_gradient[coin_gradient != -100]

        # Normalize to [-1, 1]
        if self.normalize:
            coin_gradient = utility.remap_values_to_range(coin_gradient, -1, 1)

        return torch.tensor(coin_gradient, dtype=torch.float32).unsqueeze(0)
