"""This module contains some useful utility functions."""

from collections import deque
from typing import Dict, List, Optional, Tuple, Union

import numpy as np
import numpy.typing as npt

import src.info.actions as actions_mod
from src.info.world import Tiles


def remap_values_to_range(
    values: npt.NDArray[np.float32],
    map_to_min_value: float,
    map_to_max_value: float,
    min_value: Optional[float] = None,
    max_value: Optional[float] = None,
) -> npt.NDArray[np.float32]:
    """Remap a list of values to a different value range `[normalized_min_value,
    normalized_max_value]`.

    Parameters
    ----------
    values : NDArray[np.float32]
        The value to normalize.

    map_to_min_value : float
        The minimum value of the value range to map to.

    map_to_max_value : float
        The maximum value of the value range to map to.

    min_value : Optional[float]
        The minimum value of the original range. If `None`, infer from values.

    max_value : Optional[float]
        The maximum value of the original range. If `None`, infer from values.

    Returns
    ----------
    remapped_values : NDArray[np.float32]
        The remapped value.
    """
    assert len(values) >= 1, "At least one value is required."
    min_val = min_value if min_value is not None else min(values)
    max_val = max_value if max_value is not None else max(values)
    width = max_val - min_val
    if width == 0:
        return np.array([map_to_max_value for _ in range(len(values))])
    assert width > 0, f"Min and max difference has to be positive but found {width}!"
    remapped_width = map_to_max_value - map_to_min_value
    assert remapped_width > 0, f"Mapped value difference has to be positive but found {width}!"
    if remapped_width == 0:
        return np.zeros_like(values)
    return remapped_width * (values - min_val) / width + map_to_min_value


def remap_value_to_range(
    value: float,
    min_value: float,
    max_value: float,
    map_to_min_value: float,
    map_to_max_value: float,
) -> float:
    """Remap a given value in range `[min_value, max_value]` to a different value range
    `[normalized_min_value, normalized_max_value]`.

    Parameters
    ----------
    value : float
        The value to normalize.

    min_value : float
        The minimum value of the original value range.

    max_value : float
        The maximum value of the original value range.

    map_to_min_value : float
        The minimum value of the value range to map to.

    map_to_max_value : float
        The maximum value of the value range to map to.

    Returns
    ----------
    remapped_value : float
        The remapped value.
    """
    width = max_value - min_value
    assert width > 0, f"Min and max difference has to be positive but found {width}!"
    remapped_width = map_to_max_value - map_to_min_value
    assert remapped_width > 0, f"Mapped value difference has to be positive but found {width}!"
    return remapped_width * (value - min_value) / width + map_to_min_value


def adjacent_positions_to_movement_action(
    current_position: Tuple[int, int],
    target_position: Tuple[int, int],
) -> str:
    """Get the movement action from a grid position to reach the second tile in one step.

    Note: Positions are assumed to be adjacent horizontally OR vertically. Diagonals are
    not supported!
    """
    current_x, current_y = current_position
    target_x, target_y = target_position
    diff_x = target_x - current_x
    diff_y = target_y - current_y
    assert not (diff_x != 0 and diff_y != 0), "Diagonal movement is not supported!"
    if diff_x < 0:
        return actions_mod.Actions.LEFT
    if diff_x > 0:
        return actions_mod.Actions.RIGHT
    if diff_y > 0:
        return actions_mod.Actions.DOWN
    if diff_y < 0:
        return actions_mod.Actions.UP
    return actions_mod.Actions.WAIT


def adjacent_positions_to_movement_action_id(
    current_position: Tuple[int, int],
    target_position: Tuple[int, int],
) -> int:
    """Get the movement action id from a grid position to reach the second tile in one step.

    Note: Positions are assumed to be adjacent horizontally OR vertically. Diagonals are
    not supported!
    """
    current_x, current_y = current_position
    target_x, target_y = target_position
    diff_x = target_x - current_x
    diff_y = target_y - current_y
    assert not (diff_x != 0 and diff_y != 0), "Diagonal movement is not supported!"
    if diff_x < 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.LEFT)
    if diff_x > 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.RIGHT)
    if diff_y < 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.DOWN)
    if diff_y > 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.UP)
    return actions_mod.Actions.get_action_id(actions_mod.Actions.WAIT)


def shortest_path_bfs(
    start_pos: Tuple[int, int],
    target_positions: Union[List[Tuple[int, int]], npt.NDArray[np.int16]],
    tile_map: np.ndarray,
    randomize_direction_order: bool = False,
) -> Dict[Tuple[int, int], List[Tuple[int, int]]]:
    """Compute the shortes path from a given position to all given target positions using a BFS.

    Parameters
    ----------
    start_pos : Tuple[int, int]
        The start position to find paths from.

    target_positions : Union[List[Tuple[int, int]], npt.NDArray[np.int16]]
        The target positions to compute paths to.

    tile_map : np.ndarray
        The 2D map of the game world.

    randomize_direction_order : bool
        If `True`, the order in which directions are inserted into the BFS queue is randomized.

    Returns
    ----------
    paths : Dict[Tuple[int, int], List[Tuple[int, int]]]
        The computed shortest paths.
    """
    # Initialize data structures
    parents: Dict[Tuple[int, int], Tuple[int, int]] = {start_pos: start_pos}
    pos_queue = deque([start_pos])
    remaining_targets = set(target_positions)

    width, height = tile_map.shape

    def is_in_range(pos: Tuple[int, int]) -> bool:
        return pos[0] < width and pos[1] < height

    def check_direction(active_position: Tuple[int, int], direction: str) -> None:
        active_x, active_y = active_position
        if direction == "up":
            neighbor_pos = (active_x, active_y + 1)
        elif direction == "down":
            neighbor_pos = (active_x, active_y - 1)
        elif direction == "left":
            neighbor_pos = (active_x - 1, active_y)
        elif direction == "right":
            neighbor_pos = (active_x + 1, active_y)
        else:
            raise RuntimeError(f"Found unknown direction {direction}!")
        neighbor_tile = tile_map[neighbor_pos] if is_in_range(neighbor_pos) else Tiles.WALL
        if neighbor_tile != Tiles.WALL and neighbor_pos not in parents:
            parents[neighbor_pos] = active_position
            pos_queue.append(neighbor_pos)

    # Visit neighbors until all targets are visited
    directions = ["up", "down", "left", "right"]
    while len(pos_queue) > 0 and len(remaining_targets) > 0:
        # Get next position.
        active_position = pos_queue.popleft()

        # If desired, randomize direction insertion order.
        if randomize_direction_order:
            directions = np.random.permutation(directions).tolist()

        # Remove position from targets if it is one
        if active_position in remaining_targets:
            remaining_targets.remove(active_position)

        for direction in directions:
            check_direction(active_position, direction)

    # Compute paths from parent list
    paths: Dict[Tuple[int, int], List[Tuple[int, int]]] = {}
    for target in target_positions:
        path = [parents[target]]
        parent = parents[path[-1]]
        while parent != path[-1]:
            path.append(parent)
            parent = parents[path[-1]]
        path = path[::-1]
        if target != path[-1]:
            path.append(target)
        paths[target] = path

    return paths


def manhattan_distance(
    from_position: Tuple[int, int],
    to_positions: List[Tuple[int, int]],
) -> List[int]:
    """Compute the manhattan distance between a grid position and a number of other ones.

    Parameters
    ----------
    from_position : Tuple[int, int]
        The start position.

    to_positions : List[Tuple[int, int]]
        The positions to calculate distances to.

    Return
    ----------
    distances : List[float]
        The distances.
    """
    if not to_positions:
        return [0]
    a = np.array([from_position])
    b = np.array(to_positions)
    return np.abs(a[:, None] - b).sum(-1)


def clamp(value: float, min_value: float, max_value: float) -> float:
    """Clamp a value between a minimum and maximum.

    Parameters
    ----------
    value : float
        The value to clamp.

    min_value : float
        The minimum value.

    max_value : float
        The maximum value.

    Returns
    ----------
    clamped_value : float
        The value clamped in `[min_value, max_value]`.
    """
    return max(min(value, max_value), min_value)


def camel_case_to_snake_case(text: str, use_upper: bool = False) -> str:
    """Convert the given camel case text to snake case.

    Parameters
    ----------
    text : str
        The camel case text to convert.

    use_upper : bool
        If True, return converted text in upper case.

    Returns
    ----------
    snake_case_text : str
        The converted text.
    """
    text = "".join(["_"+i.lower() if i.isupper() else i for i in text]).lstrip("_")
    return text.upper() if use_upper else text


def combine_world_data(
    tile_map: npt.NDArray[np.int16],
    coin_positions: npt.NDArray[np.int16],
    bomb_positions: npt.NDArray[np.int16],
    explosion_positions: npt.NDArray[np.int16],
    self_agent_position: npt.NDArray[np.int16],
    other_agent_positions: npt.NDArray[np.int16],
    normalize: bool = True,
) -> np.ndarray:
    """Create a 2D map of the game world containing information about all possible tile contents.

    If a tile contains multiple object classes, the following priorities are applied to determine
    the class to use:

    1) Explosions (highest)
    2) Bombs
    3) Other agents
    4) Coins
    5) Own agent position (lowest)

    Parameters
    ----------
    tile_map : npt.NDArray[np.int16],
        The tile map.

    coin_positions : npt.NDArray[np.int16],
        The current coin positions.

    bomb_positions : npt.NDArray[np.int16],
        The current bomb positions.

    explosion_positions : npt.NDArray[np.int16],
        The current explosion positions.

    self_agent_position : npt.NDArray[np.int16],
        The current agent position.

    other_agent_positions : npt.NDArray[np.int16],
        The positions of the other agents.

    normalize : bool
        If `True`, the result is normalized to [-1, 1].

    Returns
    ----------
    combined_map : np.ndarray
        The 2D world map combining the given information.
    """
    def tile_value(tile_id: int) -> float:
        return Tiles.normalize(tile_id) if normalize else tile_id

    combined_data = tile_map.copy().astype("float32")
    combined_data[*self_agent_position] = tile_value(Tiles.SELF_AGENT)
    if coin_positions.size > 0:
        combined_data[*coin_positions.T] = tile_value(Tiles.COIN)
    if other_agent_positions.size > 0:
        combined_data[*other_agent_positions.T] = tile_value(Tiles.OTHER_AGENT)
    if bomb_positions.size > 0:
        combined_data[*bomb_positions.T] = tile_value(Tiles.BOMB)
    if explosion_positions.size > 0:
        combined_data[*explosion_positions.T] = tile_value(Tiles.EXPLOSION)
    return combined_data


def get_surrounding_tiles(
    tile_map: np.ndarray,
    position: Tuple[int, int],
    radius: int = 1,
    include_diagonals: bool = False,
    default_value: int = Tiles.WALL,
) -> np.ndarray:
    """Get the surrounding tiles of a specific position.

    The surroundings are returned in one of the following orders:

    - Row-wise from bottom to top (with diagonals)
    - Center tile, horizontal surrounding (left to right), vertical surrounding (bottom to up)
      (without diagonals)

    Parameters
    ----------
    tile_map : np.ndarray
        The tile map to get values from.

    position : Tuple[int, int]
        The position the surrounding should be extracted.

    radius : int
        The radius of the returned surrounding in each direction.

    include_diagonals : bool
        If `False`, only include up/down/left/right/center tiles.

    default_value : int
        The value to use for tiles out of the playing field.

    Returns
    ----------
    surrounding : np.ndarray
        The (flattened) surrounding of the given position. Tiles outside the game world are
        interpreted as walls.
    """
    assert tile_map.ndim == 2, "Tile map must be 2-dimensional!"
    assert radius >= 1, "Range must be at least 1!"
    width, height = tile_map.shape
    pos_x, pos_y = position
    if include_diagonals:
        surrounding = [
            tile_map[pos_x + offset_x, pos_y + offset_y]
            if pos_x + offset_x < width and pos_y + offset_y < height else default_value
            for offset_x in range(-radius, radius + 1)
            for offset_y in range(-radius, radius + 1)
        ]
    else:
        horizontal = [
            tile_map[pos_x + offset_x, pos_y]
            if pos_x + offset_x < width else default_value
            for offset_x in range(-radius, radius + 1)
            if offset_x != 0
        ]
        vertical = [
            tile_map[pos_x, pos_y + offset_y]
            if pos_y + offset_y < height else default_value
            for offset_y in range(-radius, radius + 1)
            if offset_y != 0
        ]
        surrounding = [tile_map[pos_x, pos_y], *horizontal, *vertical]
    return np.array(surrounding)
