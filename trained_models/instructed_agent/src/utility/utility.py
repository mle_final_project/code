"""This module contains some useful utility functions."""

from collections import deque
from typing import Dict, List, Optional, Tuple, Union

import numpy as np
import numpy.typing as npt

import src.info.actions as actions_mod
from src.info.world import Tiles
from src.info.state import State


def remap_values_to_range(
    values: npt.NDArray[np.float32],
    map_to_min_value: float,
    map_to_max_value: float,
    min_value: Optional[float] = None,
    max_value: Optional[float] = None,
) -> npt.NDArray[np.float32]:
    """Remap a list of values to a different value range `[normalized_min_value,
    normalized_max_value]`.

    Parameters
    ----------
    values : NDArray[np.float32]
        The value to normalize.

    map_to_min_value : float
        The minimum value of the value range to map to.

    map_to_max_value : float
        The maximum value of the value range to map to.

    min_value : Optional[float]
        The minimum value of the original range. If `None`, infer from values.

    max_value : Optional[float]
        The maximum value of the original range. If `None`, infer from values.

    Returns
    ----------
    remapped_values : NDArray[np.float32]
        The remapped value.
    """
    assert len(values) >= 1, "At least one value is required."
    min_val = min_value if min_value is not None else min(values)
    max_val = max_value if max_value is not None else max(values)
    width = max_val - min_val
    if width == 0:
        return np.array([map_to_max_value for _ in range(len(values))])
    assert width > 0, f"Min and max difference has to be positive but found {width}!"
    remapped_width = map_to_max_value - map_to_min_value
    assert remapped_width > 0, f"Mapped value difference has to be positive but found {width}!"
    if remapped_width == 0:
        return np.zeros_like(values)
    return remapped_width * (values - min_val) / width + map_to_min_value


def remap_value_to_range(
    value: float,
    min_value: float,
    max_value: float,
    map_to_min_value: float,
    map_to_max_value: float,
) -> float:
    """Remap a given value in range `[min_value, max_value]` to a different value range
    `[normalized_min_value, normalized_max_value]`.

    Parameters
    ----------
    value : float
        The value to normalize.

    min_value : float
        The minimum value of the original value range.

    max_value : float
        The maximum value of the original value range.

    map_to_min_value : float
        The minimum value of the value range to map to.

    map_to_max_value : float
        The maximum value of the value range to map to.

    Returns
    ----------
    remapped_value : float
        The remapped value.
    """
    width = max_value - min_value
    assert width > 0, f"Min and max difference has to be positive but found {width}!"
    remapped_width = map_to_max_value - map_to_min_value
    assert remapped_width > 0, f"Mapped value difference has to be positive but found {width}!"
    return remapped_width * (value - min_value) / width + map_to_min_value


def adjacent_positions_to_movement_action(
    current_position: Tuple[int, int],
    target_position: Tuple[int, int],
) -> str:
    """Get the movement action from a grid position to reach the second tile in one step.

    Note: Positions are assumed to be adjacent horizontally OR vertically. Diagonals are
    not supported!
    """
    current_x, current_y = current_position
    target_x, target_y = target_position
    diff_x = target_x - current_x
    diff_y = target_y - current_y
    assert not (diff_x != 0 and diff_y !=
                0), "Diagonal movement is not supported!"
    if diff_x < 0:
        return actions_mod.Actions.LEFT
    if diff_x > 0:
        return actions_mod.Actions.RIGHT
    if diff_y > 0:
        return actions_mod.Actions.DOWN
    if diff_y < 0:
        return actions_mod.Actions.UP
    return actions_mod.Actions.WAIT


def adjacent_positions_to_movement_action_id(
    current_position: Tuple[int, int],
    target_position: Tuple[int, int],
) -> int:
    """Get the movement action id from a grid position to reach the second tile in one step.

    Note: Positions are assumed to be adjacent horizontally OR vertically. Diagonals are
    not supported!
    """
    current_x, current_y = current_position
    target_x, target_y = target_position
    diff_x = target_x - current_x
    diff_y = target_y - current_y
    assert not (diff_x != 0 and diff_y !=
                0), "Diagonal movement is not supported!"
    if diff_x < 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.LEFT)
    if diff_x > 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.RIGHT)
    if diff_y < 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.DOWN)
    if diff_y > 0:
        return actions_mod.Actions.get_action_id(actions_mod.Actions.UP)
    return actions_mod.Actions.get_action_id(actions_mod.Actions.WAIT)


def shortest_path_bfs(
    start_pos: Tuple[int, int],
    target_positions: Union[List[Tuple[int, int]], npt.NDArray[np.int16]],
    tile_map: np.ndarray,
    randomize_direction_order: bool = False,
) -> Dict[Tuple[int, int], List[Tuple[int, int]]]:
    """Compute the shortes path from a given position to all given target positions using a BFS.

    Parameters
    ----------
    start_pos : Tuple[int, int]
        The start position to find paths from.

    target_positions : Union[List[Tuple[int, int]], npt.NDArray[np.int16]]
        The target positions to compute paths to.

    tile_map : np.ndarray
        The 2D map of the game world.

    randomize_direction_order : bool
        If `True`, the order in which directions are inserted into the BFS queue is randomized.

    Returns
    ----------
    paths : Dict[Tuple[int, int], List[Tuple[int, int]]]
        The computed shortest paths.
    """
    # Initialize data structures
    parents: Dict[Tuple[int, int], Tuple[int, int]] = {start_pos: start_pos}
    pos_queue = deque([start_pos])
    remaining_targets = set(target_positions)

    width, height = tile_map.shape

    def is_in_range(pos: Tuple[int, int]) -> bool:
        return pos[0] < width and pos[1] < height

    def check_direction(active_position: Tuple[int, int], direction: str) -> None:
        active_x, active_y = active_position
        if direction == "up":
            neighbor_pos = (active_x, active_y + 1)
        elif direction == "down":
            neighbor_pos = (active_x, active_y - 1)
        elif direction == "left":
            neighbor_pos = (active_x - 1, active_y)
        elif direction == "right":
            neighbor_pos = (active_x + 1, active_y)
        else:
            raise RuntimeError(f"Found unknown direction {direction}!")
        neighbor_tile = tile_map[neighbor_pos] if is_in_range(neighbor_pos) else Tiles.WALL
        if neighbor_tile != Tiles.WALL and neighbor_pos not in parents:
            parents[neighbor_pos] = active_position
            pos_queue.append(neighbor_pos)

    # Visit neighbors until all targets are visited
    directions = ["up", "down", "left", "right"]
    while len(pos_queue) > 0 and len(remaining_targets) > 0:
        # Get next position.
        active_position = pos_queue.popleft()

        # If desired, randomize direction insertion order.
        if randomize_direction_order:
            directions = np.random.permutation(directions).tolist()

        # Remove position from targets if it is one
        if active_position in remaining_targets:
            remaining_targets.remove(active_position)

        for direction in directions:
            check_direction(active_position, direction)

    # Compute paths from parent list
    paths: Dict[Tuple[int, int], List[Tuple[int, int]]] = {}
    for target in target_positions:
        path = [parents[target]]
        parent = parents[path[-1]]
        while parent != path[-1]:
            path.append(parent)
            parent = parents[path[-1]]
        path = path[::-1]
        if target != path[-1]:
            path.append(target)
        paths[target] = path

    return paths


def manhattan_distance(
    from_position: Tuple[int, int],
    to_positions: List[Tuple[int, int]],
) -> List[int]:
    """Compute the manhattan distance between a grid position and a number of other ones.

    Parameters
    ----------
    from_position : Tuple[int, int]
        The start position.

    to_positions : List[Tuple[int, int]]
        The positions to calculate distances to.

    Return
    ----------
    distances : List[float]
        The distances.
    """
    if not to_positions:
        return [0]
    a = np.array([from_position])
    b = np.array(to_positions)
    return np.abs(a[:, None] - b).sum(-1)


def clamp(value: float, min_value: float, max_value: float) -> float:
    """Clamp a value between a minimum and maximum.

    Parameters
    ----------
    value : float
        The value to clamp.

    min_value : float
        The minimum value.

    max_value : float
        The maximum value.

    Returns
    ----------
    clamped_value : float
        The value clamped in `[min_value, max_value]`.
    """
    return max(min(value, max_value), min_value)


def camel_case_to_snake_case(text: str, use_upper: bool = False) -> str:
    """Convert the given camel case text to snake case.

    Parameters
    ----------
    text : str
        The camel case text to convert.

    use_upper : bool
        If True, return converted text in upper case.

    Returns
    ----------
    snake_case_text : str
        The converted text.
    """
    text = "".join(["_"+i.lower() if i.isupper()
                   else i for i in text]).lstrip("_")
    return text.upper() if use_upper else text


def combine_world_data(
    tile_map: npt.NDArray[np.int16],
    coin_positions: npt.NDArray[np.int16],
    bomb_positions: npt.NDArray[np.int16],
    explosion_positions: npt.NDArray[np.int16],
    self_agent_position: npt.NDArray[np.int16],
    other_agent_positions: npt.NDArray[np.int16],
    normalize: bool = True,
) -> np.ndarray:
    """Create a 2D map of the game world containing information about all possible tile contents.

    If a tile contains multiple object classes, the following priorities are applied to determine
    the class to use:

    1) Explosions (highest)
    2) Bombs
    3) Other agents
    4) Coins
    5) Own agent position (lowest)

    Parameters
    ----------
    tile_map : npt.NDArray[np.int16],
        The tile map.

    coin_positions : npt.NDArray[np.int16],
        The current coin positions.

    bomb_positions : npt.NDArray[np.int16],
        The current bomb positions.

    explosion_positions : npt.NDArray[np.int16],
        The current explosion positions.

    self_agent_position : npt.NDArray[np.int16],
        The current agent position.

    other_agent_positions : npt.NDArray[np.int16],
        The positions of the other agents.

    normalize : bool
        If `True`, the result is normalized to [-1, 1].

    Returns
    ----------
    combined_map : np.ndarray
        The 2D world map combining the given information.
    """
    def tile_value(tile_id: int) -> float:
        return Tiles.normalize(tile_id) if normalize else tile_id

    combined_data = tile_map.copy().astype("float32")
    combined_data[*self_agent_position] = tile_value(Tiles.SELF_AGENT)
    if len(coin_positions) > 0:
        combined_data[*coin_positions.T] = tile_value(Tiles.COIN)
    if len(other_agent_positions) > 0:
        combined_data[*other_agent_positions.T] = tile_value(Tiles.OTHER_AGENT)
    if len(bomb_positions) > 0:
        combined_data[*bomb_positions.T] = tile_value(Tiles.BOMB)
    if len(explosion_positions) > 0:
        combined_data[explosion_positions.T] = tile_value(Tiles.EXPLOSION)
    return combined_data


def get_surrounding_tiles(
    tile_map: np.ndarray,
    position: Tuple[int, int],
    radius: int = 1,
    include_diagonals: bool = False,
) -> np.ndarray:
    """Get the surrounding tiles of a specific position.

    The surroundings are returned in one of the following orders:

    - Row-wise from bottom to top (with diagonals)
    - Center tile, horizontal surrounding (left to right), vertical surrounding (bottom to up)
      (without diagonals)

    Parameters
    ----------
    tile_map : np.ndarray
        The tile map to get values from.

    position : Tuple[int, int]
        The position the surrounding should be extracted.

    radius : int
        The radius of the returned surrounding in each direction.

    include_diagonals : bool
        If `False`, only include up/down/left/right/center tiles.

    Returns
    ----------
    surrounding : np.ndarray
        The (flattened) surrounding of the given position. Tiles outside the game world are
        interpreted as walls.
    """
    assert tile_map.ndim == 2, "Tile map must be 2-dimensional!"
    assert radius >= 1, "Range must be at least 1!"
    width, height = tile_map.shape
    pos_x, pos_y = position
    if include_diagonals:
        surrounding = [
            tile_map[pos_x + offset_x, pos_y + offset_y]
            if pos_x + offset_x < width and pos_y + offset_y < height else Tiles.WALL
            for offset_x in range(-radius, radius + 1)
            for offset_y in range(-radius, radius + 1)
        ]
    else:
        horizontal = [
            tile_map[pos_x + offset_x, pos_y]
            if pos_x + offset_x < width else Tiles.WALL
            for offset_x in range(-radius, radius + 1)
            if offset_x != 0
        ]
        vertical = [
            tile_map[pos_x, pos_y + offset_y]
            if pos_y + offset_y < height else Tiles.WALL
            for offset_y in range(-radius, radius + 1)
            if offset_y != 0
        ]
        surrounding = [tile_map[pos_x, pos_y], *horizontal, *vertical]
    return np.array(surrounding)


def closest_coin_direction(field, coins: List[Tuple[int, int]],
                           position) -> Tuple[Tuple[int, int],
                                              str, int]:

    # print(position)

    if not coins:
        print("No Coins")
        return ((0, 0), actions_mod.Actions.INVALID_NAME, -1)

    x, y = position
    coin_position = ((0, 0), actions_mod.Actions.INVALID_NAME, -1)
    tiles_around = get_walkable_tiles_and_direction_around_position(
        x, y, field)
    tiles_to_check: List[Tuple[Tuple[int, int], str, int]] = []

    for tile in tiles_around:
        tiles_to_check.append((*tile, 1))

    checked_tiles: List[Tuple[int, int]] = []

    i = 0
    while tiles_to_check:
        i += 1
        if i == 1000:
            print("1000")
            tiles_to_check = []
            break

        tile = tiles_to_check.pop(0)
        if tile[0] in checked_tiles:
            print("double tiles")
            print(tile)
            print(checked_tiles)
            raise Exception('ge')
        checked_tiles.append(tile[0])

        if tile[0] in coins:
            coin_position = tile
            tiles_to_check = []
            break

        new_tiles = get_walkable_tiles_around_position(
            tile[0][0], tile[0][1], field)

        for new_tile in new_tiles:
            tiles_to_check_position: List[Tuple[int, int]] = []
            for tile_to_check in tiles_to_check:
                tiles_to_check_position.append(tile_to_check[0])

            if new_tile in checked_tiles or new_tile in tiles_to_check_position:
                continue
            tiles_to_check.append((new_tile, tile[1], tile[2]+1))

    # print(coin_position)
    return coin_position


def get_walkable_tiles_around_position(x: int, y: int, field: np.ndarray) -> List[Tuple[int, int]]:
    walkable_tiles = []
    if field[x+1][y] != Tiles.WALL:
        walkable_tiles.append((x+1, y))
    if (field[x-1][y] != Tiles.WALL):
        walkable_tiles.append((x-1, y))
    if field[x][y+1] != Tiles.WALL:
        walkable_tiles.append((x, y+1))
    if field[x][y-1] != Tiles.WALL:
        walkable_tiles.append((x, y-1))

    return walkable_tiles


def get_walkable_tiles_and_direction_around_position(
        x: int, y: int, field: np.ndarray) -> List[
        Tuple[Tuple[int, int],
              str]]:
    walkable_tiles = []
    if field[x+1][y] != Tiles.WALL and field[x+1][y] != Tiles.CRATE and field[x+1][y] != Tiles.OTHER_AGENT:
        walkable_tiles.append(((x+1, y), actions_mod.Actions.RIGHT))
    if field[x-1][y] != Tiles.WALL and field[x-1][y] != Tiles.CRATE and field[x-1][y] != Tiles.OTHER_AGENT:
        walkable_tiles.append(((x-1, y), actions_mod.Actions.LEFT))
    if field[x][y+1] != Tiles.WALL and field[x][y+1] != Tiles.CRATE and field[x][y+1] != Tiles.OTHER_AGENT:
        walkable_tiles.append(((x, y+1), actions_mod.Actions.DOWN))
    if field[x][y-1] != Tiles.WALL and field[x][y-1] != Tiles.CRATE and field[x][y-1] != Tiles.OTHER_AGENT:
        walkable_tiles.append(((x, y-1), actions_mod.Actions.UP))

    return walkable_tiles


def all_coins_direction_and_distance(field: np.ndarray, coins: List[Tuple[int, int]],
                                     position: Tuple[int, int]) -> Dict[Tuple[int, int],
                                                                        Tuple[str, int]]:

    if not coins:
        # print("No Coins")
        return {}

    x, y = position
    tiles_around = get_walkable_tiles_and_direction_around_position(
        x, y, field)
    tiles_to_check: List[Tuple[Tuple[int, int], str, int]] = []

    for tile in tiles_around:
        tiles_to_check.append((*tile, 1))

    checked_tiles: List[Tuple[int, int]] = []

    coins_pos_dir_dist: Dict[Tuple[int, int], Tuple[str, int]] = {}

    while tiles_to_check:

        tile = tiles_to_check.pop(0)

        checked_tiles.append(tile[0])

        if tile[0] in coins:
            coins_pos_dir_dist[tile[0]] = (tile[1], tile[2])

        new_tiles = get_walkable_tiles_around_position(
            tile[0][0], tile[0][1], field)

        for new_tile in new_tiles:
            tiles_to_check_position: List[Tuple[int, int]] = []
            for tile_to_check in tiles_to_check:
                tiles_to_check_position.append(tile_to_check[0])

            if new_tile in checked_tiles or new_tile in tiles_to_check_position:
                continue
            tiles_to_check.append((new_tile, tile[1], tile[2]+1))

    # print(coin_position)
    return coins_pos_dir_dist


def all_crates_direction_and_distance(field: np.ndarray, position: Tuple[int, int]) -> Dict[Tuple[int, int], Tuple[str, int]]:

    creates: List[Tuple[int, int]] = []

    width, height = field.shape
    for x in range(width):
        for y in range(height):
            if field[x, y] == 1:
                creates.append((x, y))

    if not creates:
        # print("No Coins")
        return {}

    x, y = position
    tiles_around = get_walkable_tiles_and_direction_around_position(
        x, y, field)
    tiles_to_check: List[Tuple[Tuple[int, int], str, int]] = []

    for tile in tiles_around:
        tiles_to_check.append((*tile, 1))

    checked_tiles: List[Tuple[int, int]] = []

    coins_pos_dir_dist: Dict[Tuple[int, int], Tuple[str, int]] = {}

    while tiles_to_check:

        tile = tiles_to_check.pop(0)

        checked_tiles.append(tile[0])

        if tile[0] in creates:
            coins_pos_dir_dist[tile[0]] = (tile[1], tile[2])

        new_tiles = get_walkable_tiles_around_position(
            tile[0][0], tile[0][1], field)

        for new_tile in new_tiles:
            tiles_to_check_position: List[Tuple[int, int]] = []
            for tile_to_check in tiles_to_check:
                tiles_to_check_position.append(tile_to_check[0])

            if new_tile in checked_tiles or new_tile in tiles_to_check_position:
                continue
            tiles_to_check.append((new_tile, tile[1], tile[2]+1))

    # print(coin_position)
    return coins_pos_dir_dist


def get_direction_and_distance_from_coins(x: int, y: int, field: np.ndarray,
                                          coins: List[Tuple[int, int]]) -> List[Tuple[str, int]]:

    coins_distance_and_direction: List[Tuple[str, int]] = []
    coins_pos_dir_dist = all_coins_direction_and_distance(
        field, coins, (x, y))
    for coin in coins:
        if coin == (-1, -1):
            # print("Collected Coin")
            coins_distance_and_direction.append(
                (actions_mod.Actions.INVALID_NAME, -1))
        else:
            if coin in coins_pos_dir_dist:
                coins_distance_and_direction.append(coins_pos_dir_dist[coin])

    return coins_distance_and_direction


def get_danger_map(
        field: np.ndarray, bombs: List[Tuple[Tuple[int, int],
                                             int]],
        explosion_map: np.ndarray, game_state) -> np.ndarray:

    for bomb in bombs:
        x = bomb[0][0]
        y = bomb[0][1]
        value = bomb[1]
        width, height = field.shape
        field[x, y] = -1

        # Down
        for i in range(1, 4):
            if x + i < height:
                if field[x + i, y] == -1:
                    break
                if field[x + i, y] == 1:
                    continue
                field[x + i, y] = value+1
        # Up
        for i in range(1, 4):
            if x - i > 0:
                if field[x - i, y] == -1:
                    break
                if field[x - i, y] == 1:
                    continue
                field[x - i, y] = value+1
        # Right
        for i in range(1, 4):
            if y + i < width:
                if field[x, y + i] == -1:
                    break
                if field[x, y + i] == 1:
                    continue
                field[x, y + i] = value+1
        # Left
        for i in range(1, 4):
            if y - i > 0:
                if field[x, y - i] == -1:
                    break
                if field[x, y - i] == 1:
                    continue
                field[x, y - i] = value+1

    field[explosion_map == 1] = -1
    field[field == 1] = -1
    others = [values.position for values in State.get_other_values(game_state)]
    for other in others:
        field[other[0], other[1]] = Tiles.OTHER_AGENT
    # print(field.T)
    return field


def closest_safety_direction(field: np.ndarray, position: Tuple[int, int]) -> str:

    x, y = position
    safety_position = ((0, 0), actions_mod.Actions.INVALID_NAME, -1)

    if field[x, y] == 0:
        return safety_position[1]

    tiles_around = get_walkable_tiles_and_direction_around_position(
        x, y, field)
    tiles_to_check: List[Tuple[Tuple[int, int], str, int]] = []

    for tile in tiles_around:
        tiles_to_check.append((*tile, 1))

    checked_tiles: List[Tuple[int, int]] = []

    i = 0
    while tiles_to_check:
        i += 1
        if i == 1000:
            print("1000")
            tiles_to_check = []
            break

        tile = tiles_to_check.pop(0)
        if tile[0] in checked_tiles:
            print("double tiles")
            print(tile)
            print(checked_tiles)
            raise Exception('ge')
        checked_tiles.append(tile[0])

        if field[tile[0]] == 0:
            safety_position = tile
            tiles_to_check = []
            break

        new_tiles = get_walkable_tiles_around_position(
            tile[0][0], tile[0][1], field)

        for new_tile in new_tiles:
            tiles_to_check_position: List[Tuple[int, int]] = []
            for tile_to_check in tiles_to_check:
                tiles_to_check_position.append(tile_to_check[0])

            if new_tile in checked_tiles or new_tile in tiles_to_check_position:
                continue
            tiles_to_check.append((new_tile, tile[1], tile[2]+1))

    return safety_position[1]
