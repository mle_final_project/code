# Base
from .base.collection import FeatureExtractorCollection
from .base.feature_extractor import FeatureExtractor

# Implementations
from .implementations.instructions import (
    CoinDistanceFeatureExtractor,
    InstructionExtractor,
    InstructionFeatureExtractor,
    SafeBombsExtractor,
    ShortestDirectionToSafetyExtractor,
    SpamBombsExtractor,
)
