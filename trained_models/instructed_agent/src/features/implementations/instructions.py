from random import random
from types import SimpleNamespace
from typing import Any, Dict, List, Tuple

import numpy as np
import torch

from src.features import FeatureExtractor
from src.info.actions import Actions
from src.info.state import State
from src.utility import utility
import numpy.typing as npt


class InstructionFeatureExtractor(FeatureExtractor):

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)

    def extract_features(self, game_state: Dict[str, Any]) -> torch.Tensor:

        direction = utility.closest_coin_direction(State.get_field(
            game_state), State.get_coins(game_state), State.get_self_values(game_state).position)[1]

        return torch.tensor([Actions.get_action_id(direction)], dtype=torch.float32).unsqueeze(0)


class CoinDistanceFeatureExtractor(FeatureExtractor):

    def __init__(self) -> None:
        super().__init__()
        self.cached_coins: List[Tuple[int, int]] = []
        self.explosion_map: npt.NDArray[np.int16] = np.zeros(shape=(17, 17), dtype=np.int16)
        self.last_step = -1

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)  # game.settings.SCENARIOS["coin-heaven"]["COIN_COUNT"]

    def _extract_features_implementation(
            self,
            _self_namespace: SimpleNamespace,
            game_state: Dict[str, Any],
    ) -> torch.Tensor:

        field = State.get_field(game_state)
        field_walled = field.copy()
        field_walled[field_walled == 1] = -1
        position = State.get_self_values(game_state).position

        all_coins = utility.all_coins_direction_and_distance(
            field_walled, State.get_coins(game_state), position)
        field_coins = []
        # print("all coins ", all_coins)
        for coin in all_coins.items():
            field_coins.append(coin[0])

        # print("fild coins ", field_coins)
        if len(field_coins) == 0:
            field = State.get_field(game_state)
            width, height = field.shape
            for x in range(width):
                for y in range(height):
                    if field[x, y] == 1:
                        field_coins.append((x, y))
        else:
            field[field == 1] = -1

        # print("field coins crates", field_coins)
        if len(field_coins) == 0:
            others = [values.position for values in State.get_other_values(game_state)]
            field_coins.extend(others)

        # print("field coins agents", field_coins)

        # print(field_coins)
        coin_distances_and_directions = utility.get_direction_and_distance_from_coins(
            *position, field, field_coins)
        # hallo = State.get_field(game_state)
        # hallo[position[0]][position[1]] = 5
        # print(hallo.T)
        direction_value: List[float] = [0, 0, 0, 0]
        # print(distances_and_directions)
        # print("cdad: ", coin_distances_and_directions)
        for distance_and_direction in coin_distances_and_directions:
            if (distance_and_direction[0] == Actions.LEFT):
                direction_value[0] += ((1 / distance_and_direction[1]) + random() * 0) ** 2
            if (distance_and_direction[0] == Actions.RIGHT):
                direction_value[1] += ((1 / distance_and_direction[1]) + random() * 0) ** 2
            if (distance_and_direction[0] == Actions.UP):
                direction_value[2] += ((1 / distance_and_direction[1]) + random() * 0) ** 2
            if (distance_and_direction[0] == Actions.DOWN):
                direction_value[3] += ((1 / distance_and_direction[1]) + random() * 0) ** 2

        # print("last step: ", self.last_step, " ", Actions.get_action_name(int(self.last_step)))
        # print("Direction before: ", direction_value)
        if self.last_step == 0:
            direction_value[1] *= 0.5
        if self.last_step == 1:
            direction_value[0] *= 0.5
        if self.last_step == 2:
            direction_value[3] *= 0.5
        if self.last_step == 3:
            direction_value[2] *= 0.5

        # print("Direction after: ", direction_value)

        index = np.argmax(direction_value).item()

        # print(direction_value)
        for i in range(4):
            if i == index:
                if direction_value[i] != 0:
                    direction_value[i] = 1
            else:
                direction_value[i] = 0

        self.explosion_map = State.get_explosion_map(game_state)
        danger_map = utility.get_danger_map(
            State.get_field(game_state),
            State.get_bombs(game_state),
            self.explosion_map, game_state)
        # print(danger_map.T)
        map = State.get_field(game_state)

        x, y = position
        map[map == 1] = -1
        if map[x-1, y] == -1 or danger_map[x-1, y] == -1:
            direction_value[0] = -1
        if map[x+1, y] == -1 or danger_map[x+1, y] == -1:
            direction_value[1] = -1
        if map[x, y-1] == -1 or danger_map[x, y-1] == -1:
            direction_value[2] = -1
        if map[x, y+1] == -1 or danger_map[x, y+1] == -1:
            direction_value[3] = -1

        # print("Left:", direction_value[0])
        # print("Right:", direction_value[1])
        # print("Up:", direction_value[2])
        # print("Down:", direction_value[3])

        move_direction: int = int(np.argmax(direction_value))

        # print("move direction: ", move_direction)
        # print("dir value: ", direction_value)
        # print("res value: ", direction_value[move_direction])
        if direction_value[move_direction] == -1:
            move_direction = -1
        # print("move direction: ", move_direction)

        self.last_step = np.argmax(direction_value)

        # print("Left: ", Actions.get_action_id(Actions.LEFT))
        # print("Right: ", Actions.get_action_id(Actions.RIGHT))
        # print("Up: ", Actions.get_action_id(Actions.UP))
        # print("Down: ", Actions.get_action_id(Actions.DOWN))
        # print("move_direction: ", move_direction, Actions.get_action_name(move_direction))

        if move_direction == 0:
            move_direction = Actions.get_action_id(Actions.LEFT)
        elif move_direction == 1:
            move_direction = Actions.get_action_id(Actions.RIGHT)
        elif move_direction == 2:
            move_direction = Actions.get_action_id(Actions.UP)
        elif move_direction == 3:
            move_direction = Actions.get_action_id(Actions.DOWN)

        return torch.tensor([move_direction], dtype=torch.float32).unsqueeze(0)


class ShortestDirectionToSafetyExtractor(FeatureExtractor):

    def __init__(self) -> None:
        super().__init__()
        self.explosion_map: npt.NDArray[np.int16] = np.zeros(shape=(17, 17), dtype=np.int16)

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)

    def _extract_features_implementation(
            self,
            _self_namespace: SimpleNamespace,
            game_state: Dict[str, Any],
    ) -> torch.Tensor:

        self.explosion_map = State.get_explosion_map(game_state)
        danger_map = utility.get_danger_map(
            State.get_field(game_state),
            State.get_bombs(game_state),
            self.explosion_map, game_state)
        position = State.get_self_values(game_state).position

        direction_to_safety = utility.closest_safety_direction(danger_map, position)

        return torch.tensor(
            [Actions.get_action_id(direction_to_safety)],
            dtype=torch.float32).unsqueeze(0)


class SpamBombsExtractor(FeatureExtractor):

    def __init__(self) -> None:
        super().__init__()

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)  # game.settings.SCENARIOS["coin-heaven"]["COIN_COUNT"]

    def _extract_features_implementation(
            self,
            _self_namespace: SimpleNamespace,
            game_state: Dict[str, Any],
    ) -> torch.Tensor:

        is_bomb_available = State.get_self_values(game_state).is_bomb_available

        b = 1 if is_bomb_available else 0

        return torch.tensor([b], dtype=torch.float32).unsqueeze(0)


class SafeBombsExtractor(FeatureExtractor):

    def __init__(self) -> None:
        super().__init__()

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)

    def check_crates_left(self, game_state) -> bool:
        field = State.get_field(game_state)
        width, height = field.shape
        for x in range(width):
            for y in range(height):
                if field[x, y] == 1:
                    return True
        return False

    def _extract_features_implementation(
            self,
            _self_namespace: SimpleNamespace,
            game_state: Dict[str, Any],
    ) -> torch.Tensor:

        bomb_check_range = 2
        is_bomb_available = State.get_self_values(game_state).is_bomb_available
        if not is_bomb_available:
            return torch.tensor([0], dtype=torch.float32).unsqueeze(0)

        position = State.get_self_values(game_state).position
        field = State.get_field(game_state)
        field[field == 1] = -1
        reachable_coins = utility.all_coins_direction_and_distance(
            field, State.get_coins(game_state), position)

        if reachable_coins:
            return torch.tensor([0], dtype=torch.float32).unsqueeze(0)

        new_bomb: Tuple[Tuple[int, int], int] = ((position[0], position[1]), 1)
        bombs = State.get_bombs(game_state)
        bombs.append(new_bomb)
        danger_map = utility.get_danger_map(
            State.get_field(game_state),
            bombs, State.get_explosion_map(game_state),
            game_state)

        direction_to_safety = utility.closest_safety_direction(danger_map, position)

        if direction_to_safety == Actions.INVALID_NAME:
            return torch.tensor([0], dtype=torch.float32).unsqueeze(0)

        field = State.get_field(game_state)
        others: List[Tuple[int, int]] = [values.position
                                         for values in State.get_other_values(game_state)]
        x, y = State.get_self_values(game_state).position
        # Down
        for i in range(1, bomb_check_range):
            if field[x + i, y] == -1:
                break
            if field[x + i, y] == 1:
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)
            if (x + i, y) in others and not self.check_crates_left(game_state):
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)
        # Up
        for i in range(1, bomb_check_range):
            if field[x - i, y] == -1:
                break
            if field[x - i, y] == 1:
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)
            if (x - i, y) in others and not self.check_crates_left(game_state):
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)
        # Right
        for i in range(1, bomb_check_range):
            if field[x, y + i] == -1:
                break
            if field[x, y + i] == 1 or [x, y + i] in others:
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)
            if (x, y + i) in others and not self.check_crates_left(game_state):
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)

        # Left
        for i in range(1, bomb_check_range):
            if field[x, y - i] == -1:
                break
            if field[x, y - i] == 1:
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)
            if (x, y - i) in others and not self.check_crates_left(game_state):
                return torch.tensor([1], dtype=torch.float32).unsqueeze(0)
        return torch.tensor([0], dtype=torch.float32).unsqueeze(0)


class InstructionExtractor(FeatureExtractor):

    def __init__(self) -> None:
        super().__init__(cache_features=False)
        self.coin_distance_feature_extractor: CoinDistanceFeatureExtractor = CoinDistanceFeatureExtractor()
        self.shortest_direction_to_safety_extractor: ShortestDirectionToSafetyExtractor = ShortestDirectionToSafetyExtractor()
        self.safe_bombs_extractor: SafeBombsExtractor = SafeBombsExtractor()

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)

    def _extract_features_implementation(
            self,
            _self_namespace: SimpleNamespace,
            game_state: Dict[str, Any],
    ) -> torch.Tensor:

        features: List[float] = []

        features.extend(self.coin_distance_feature_extractor.extract_features(
            _self_namespace, game_state)[0].tolist())

        features.extend(
            self.shortest_direction_to_safety_extractor.extract_features(
                _self_namespace,
                game_state)[0].tolist())

        features.extend(self.safe_bombs_extractor.extract_features(
            _self_namespace, game_state)[0].tolist())

        # print("features: ", features)

        if features[4] != -1:
            return torch.tensor([features[4]], dtype=torch.float32).unsqueeze(0)

        field = State.get_field(game_state)
        field[field == 1] = -1
        position = State.get_self_values(game_state).position
        reachable_coins = utility.all_coins_direction_and_distance(
            field, State.get_coins(game_state), position)

        if features[5] == 1 and not reachable_coins:
            return torch.tensor(
                [Actions.ACTION_TO_ID[Actions.BOMB]],
                dtype=torch.float32).unsqueeze(0)
        move_features = features[0:4]

        if all(x == -1 for x in move_features):
            return torch.tensor(
                [Actions.ACTION_TO_ID[Actions.WAIT]],
                dtype=torch.float32).unsqueeze(0)

        for x in range(4):
            if move_features[x] == 0:
                move_features[x] += random() * 0.1

        """Move towards largest direction value."""
        index = np.argmax(move_features).item()
        if index == 0:
            return torch.tensor(
                [Actions.ACTION_TO_ID[Actions.LEFT]],
                dtype=torch.float32).unsqueeze(0)
        if index == 1:
            return torch.tensor(
                [Actions.ACTION_TO_ID[Actions.RIGHT]],
                dtype=torch.float32).unsqueeze(0)
        if index == 2:
            return torch.tensor(
                [Actions.ACTION_TO_ID[Actions.UP]],
                dtype=torch.float32).unsqueeze(0)
        if index == 3:
            return torch.tensor(
                [Actions.ACTION_TO_ID[Actions.DOWN]],
                dtype=torch.float32).unsqueeze(0)

        return torch.tensor([0], dtype=torch.float32).unsqueeze(0)


class NewInstructionExtractor(FeatureExtractor):

    def __init__(self) -> None:
        super().__init__(cache_features=False)
        self.coin_distance_feature_extractor: CoinDistanceFeatureExtractor = CoinDistanceFeatureExtractor()
        self.shortest_direction_to_safety_extractor: ShortestDirectionToSafetyExtractor = ShortestDirectionToSafetyExtractor()
        self.safe_bombs_extractor: SafeBombsExtractor = SafeBombsExtractor()

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)

    def _extract_features_implementation(
            self,
            _self_namespace: SimpleNamespace,
            game_state: Dict[str, Any],
    ) -> torch.Tensor:

        features: List[float] = []

        features.extend(self.coin_distance_feature_extractor.extract_features(
            _self_namespace, game_state)[0].tolist())

        features.extend(
            self.shortest_direction_to_safety_extractor.extract_features(
                _self_namespace,
                game_state)[0].tolist())

        features.extend(self.safe_bombs_extractor.extract_features(
            _self_namespace, game_state)[0].tolist())

        # print("features: ", features)

        if features[1] != -1:
            return torch.tensor([features[1]], dtype=torch.float32).unsqueeze(0)

        if features[2] == 1:
            return torch.tensor(
                [Actions.ACTION_TO_ID[Actions.BOMB]],
                dtype=torch.float32).unsqueeze(0)

        if features[0] != -1:
            return torch.tensor([features[0]], dtype=torch.float32).unsqueeze(0)

        return torch.tensor([0], dtype=torch.float32).unsqueeze(0)
