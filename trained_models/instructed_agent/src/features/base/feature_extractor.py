"""This module contains functions and classes to extract and process features from game states."""

from abc import ABC, abstractmethod
from collections import deque
from types import SimpleNamespace
from typing import Any, Deque, Dict, Optional, Tuple

from torch import Tensor

from src.info.state import State


class FeatureExtractor(ABC):
    """Base class/interface for a feature extractor."""

    def __init__(self, cache_features: bool = True) -> None:
        """Initialize a new feature extractor instance.

        Parameters
        ----------
        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        self.name = type(self).__name__
        self.do_cache_features = cache_features
        self.last_cached_round = 0
        self.last_cached_step = -2
        self.cached_features: Deque[Tensor] = deque([], maxlen=2)

    @abstractmethod
    def n_features(self) -> Tuple[int, ...]:
        """Get the number of features this extractor produces.

        Override to implement the desired behavior.
        """
        pass

    def extract_features(
        self,
        self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract features from the given game state.

        Override `_extract_features_implementation` to implement the desired behavior.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace information.

        game_state : Dict[str, Any]
            The game state.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        current_round = State.get_round(game_state)
        current_step = State.get_step(game_state)

        def get_cached() -> Optional[Tensor]:
            if current_round != self.last_cached_round:
                return None
            if current_step == self.last_cached_step:
                return self.cached_features[-1]
            if current_step == self.last_cached_step - 1:
                if len(self.cached_features) < 2:
                    return None
                return self.cached_features[-2]
            return None

        # Return cached features if possible
        if self.do_cache_features:
            features = get_cached()
            if features is not None:
                return features

        # Else extract new ones
        features = self._extract_features_implementation(self_namespace, game_state)
        dims_error_msg = f"Features have dimension {features.shape}, expected {self.n_features()}!"
        assert features.shape == self.n_features(), dims_error_msg

        # Update feature cache
        if self.do_cache_features:
            self.last_cached_round = current_round
            self.last_cached_step = current_step
            self.cached_features.append(features)

        return features

    @abstractmethod
    def _extract_features_implementation(
        self,
        self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract features from the given game state.

        Override to implement the desired behavior.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace information.

        game_state : Dict[str, Any]
            The game state.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        pass
