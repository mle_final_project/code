"""This module contains classes and functions to work with game states."""

from typing import Any, ClassVar, Dict, List, NamedTuple, Tuple, cast

import numpy as np
import numpy.typing as npt

from src.info.world import Tiles


class AgentValues(NamedTuple):
    """Named tuple for the agent values reported in the game state."""

    name: str
    score: int
    is_bomb_available: bool
    position: Tuple[int, int]


class State:
    """The fields that can be accessed in the game state."""

    ROUND = "round"
    """The number of rounds since launching the environment, starting at 1."""

    STEP = "step"
    """The number of steps in the episode so far, starting at 1."""

    FIELD = "field"
    """A 2D numpy array describing the tiles of the game board."""

    BOMBS = "bombs"
    """A list of tuples ((x,y),t) of coordinates and countdowns for all active bombs (0 means \
    about to explode)."""

    EXPLOSION_MAP = "explosion_map"
    """A 2D numpy array stating, for each tile, for how many more steps an explosion will be \
    present. A value of 0 means no explosion."""

    COINS = "coins"
    """A list of coordinates (x,y) for all currently collectable coins."""

    SELF = "self"
    """A tuple (n,s,b,(x.y)) describing your own agent. n is the agent's name, s its current \
    score, b is a boolean indicating whether the 'BOMB' action is possible, and (x,y) is its \
    coordinate on the field."""

    OTHERS = "others"
    """A list of tuples like the one in 'SELF' for all opponents that are still in the game."""

    USER_INPUT = "user_input"
    """User input via the GUI."""

    ALL: ClassVar[List[str]] = [
        ROUND,
        STEP,
        FIELD,
        BOMBS,
        EXPLOSION_MAP,
        COINS,
        SELF,
        OTHERS,
        USER_INPUT,
    ]
    """A list of all game state fields."""

    STATE_TO_INDEX: ClassVar[Dict[str, int]] = {name: index for index, name in enumerate(ALL)}
    """Dictionary for converting an state field name to an integer index."""

    INDEX_TO_STATE: ClassVar[Dict[int, str]] = dict(
        zip(STATE_TO_INDEX.values(), STATE_TO_INDEX.keys()),
    )
    """Dictionary for converting an state field index to its name."""

    @staticmethod
    def get_state_field_index(field_name: str) -> int:
        """Convert the given state filed name to the corresponding integer index.

        Parameters
        ----------
        field_name : str
            The name of the field to get the index for.

        Returns
        ----------
        index : int
            The field index.
        """
        return State.STATE_TO_INDEX.get(field_name, -1)

    @staticmethod
    def get_state_field_name(index: int) -> str:
        """Convert the given integer index to the corresponding state field name.

        Parameters
        ----------
        index : int
            The index to get the name for.

        Returns
        ----------
        field_name : str
            The field name.
        """
        return State.INDEX_TO_STATE.get(index, "Invalid")

    @staticmethod
    def get_round(game_state: Dict[str, Any]) -> int:
        """Get the current round from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        round : int
            The number of rounds since launching the environment, starting at 1.
        """
        return game_state[State.ROUND]

    @staticmethod
    def get_step(game_state: Dict[str, Any]) -> int:
        """Get the current step from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        step : int
            The number of steps in the episode so far, starting at 1.
        """
        return game_state[State.STEP]

    @staticmethod
    def get_field(game_state: Dict[str, Any]) -> npt.NDArray[np.int16]:
        """Get the current field state from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        field : npt.NDArray[np.int16]
            A 2D numpy array describing the tiles of the game board.
        """
        return game_state[State.FIELD].copy()

    @staticmethod
    def get_bombs(game_state: Dict[str, Any]) -> List[Tuple[Tuple[int, int], int]]:
        """Get the current bomb information from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        bombs : List[Tuple[Tuple[int, int], int]]
            A list of tuples ((x,y),t) of coordinates and countdowns for all active bombs (0 means
            about to explode).
        """
        return game_state[State.BOMBS].copy()

    @staticmethod
    def get_bombs_numpy(game_state: Dict[str, Any]) -> npt.NDArray[np.int16]:
        """Get the current bomb information from a game state as a 2D numpy array.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        bombs : npt.NDArray[np.int16]
            A 2D numpy array with entries [x,y,t] of coordinates and countdowns for all active bombs
            (0 means about to explode).
        """
        bombs = game_state[State.BOMBS]
        bombs_flat = [[x, y, t] for (x, y), t in bombs]
        return np.array(bombs_flat, dtype=np.int16)

    @staticmethod
    def get_explosion_map(game_state: Dict[str, Any]) -> npt.NDArray[np.int16]:
        """Get the current explosion map from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        explosion_map : npt.NDArray[np.int16]
            A 2D numpy array stating, for each tile, for how many more steps an explosion will be
            present. A value of 0 means no explosion.
        """
        return game_state[State.EXPLOSION_MAP].copy()

    @staticmethod
    def get_coins(game_state: Dict[str, Any]) -> List[Tuple[int, int]]:
        """Get the current coin positions from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        coins : List[Tuple[int, int]]
            A list of coordinates (x,y) for all currently collectable coins.
        """
        return game_state[State.COINS].copy()

    @staticmethod
    def get_coins_numpy(game_state: Dict[str, Any]) -> npt.NDArray[np.int16]:
        """Get the current coin positions from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        coins : npt.NDArray[np.int16]
            A numpy array of coordinates [x,y] for all currently collectable coins.
        """
        return np.array(game_state[State.COINS], dtype=np.int16)

    @staticmethod
    def get_crates(game_state: Dict[str, Any]) -> List[Tuple[int, int]]:
        """Get the current crate positions from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        coins : List[Tuple[int, int]]
            A list of coordinates (x,y) for all currently present crates.
        """
        tile_map = State.get_field(game_state)
        crate_indices = cast(
            Tuple[npt.NDArray[np.int16], npt.NDArray[np.int16]],
            np.where(tile_map == Tiles.CRATE),
        )
        return [(x, y) for (x, y) in zip(*crate_indices)]

    @staticmethod
    def get_crates_numpy(game_state: Dict[str, Any]) -> npt.NDArray[np.int16]:
        """Get the current crate positions from a game state as numpy array.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        coins : npt.NDArray[np.int16]
            A list of coordinates (x,y) for all currently present crates.
        """
        return np.array(State.get_crates(game_state))

    @staticmethod
    def get_self_values(game_state: Dict[str, Any]) -> AgentValues:
        """Get the current agent data from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        self_values : AgentValues
            A tuple (n,s,b,(x.y)) describing your own agent. n is the agent's name, s its current
            score, b is a boolean indicating whether the 'BOMB' action is possible, and (x,y) is its
            coordinate on the field.
        """
        return AgentValues(*game_state[State.SELF])

    @staticmethod
    def get_other_values(game_state: Dict[str, Any]) -> List[AgentValues]:
        """Get the current agent data from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        other_values : List[AgentValues]
            A list of tuples (n,s,b,(x.y)) describing the other active agents. n is the agent's
            name, s its current score, b is a boolean indicating whether the 'BOMB' action is
            possible, and (x,y) is its coordinate on the field.
        """
        return [AgentValues(*values) for values in game_state[State.OTHERS]]

    @staticmethod
    def get_user_input(game_state: Dict[str, Any]) -> str:
        """Get the current user input from a game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state to extract data from.

        Returns
        ----------
        round : int
            User input via the GUI.
        """
        return game_state[State.USER_INPUT]
