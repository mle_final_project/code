import csv
from collections import deque
from io import StringIO
from typing import Deque, Dict, List, Tuple


class DataLogger:
    """Logger for storing `key, value` pairs together with the current round and step."""

    def __init__(self, start_at_round: int = 1, capacity: int = 10000) -> None:
        """Initialize logger instance.

        Parameters
        ----------
        start_at_round : int
            The round to start counting from.

        capacity : int
            The maximum number of entries to store.
        """
        self.data: Deque[Dict[str, float]] = deque([], maxlen=capacity)
        self.known_keys = {"ROUND"}
        self.current_round = start_at_round
        self.next_round()

    def next_round(self) -> None:
        self.current_round += 1
        self.data.append({})

    def log_datum(self, key: str, value: float) -> None:
        """Log the given datum.

        Parameters
        ----------
        key : str
            The key to store.

        value : float
            The value to store.
        """
        self.known_keys.add(key)
        round_data = self.data[-1]
        round_data[key] = round_data.get(key, 0) + value

    def log_data(self, data: List[Tuple[str, float]]) -> None:
        """Log the given data.

        Parameters
        ----------
        data : List[Tuple[str, float]]
            The data to log.
        """
        for key, value in data:
            self.log_datum(key, value)

    def to_csv(self, every_n: int = 1, include_header: bool = True) -> str:
        """Convert the stored data to a csv formatted string.

        Parameters
        ----------
        every_n : int
            Only include every n-th round.

        include_header : bool
            Should the csv header be included?

        Returns
        ----------
        csv_string : str
            The csv formatted data string.
        """
        output = StringIO()
        writer = csv.DictWriter(output, self.known_keys)
        if include_header:
            writer.writeheader()
        start_index = self.current_round - len(self.data) + 1
        n_rows_sum = {}
        for row in self.data:
            n_rows_sum = {key: n_rows_sum.get(key, 0) + row.get(key, 0) for key in row}
            if start_index % every_n == 0:
                n_rows_avg = {key: value / every_n for key, value in n_rows_sum.items()}
                n_rows_avg["ROUND"] = start_index
                n_rows_sum = {}
                writer.writerow(n_rows_avg)
            start_index += 1
        return output.getvalue()
