"""This module contains classes and functions to work with the actions an agent can perform."""


from abc import ABC, abstractmethod
from typing import ClassVar, Dict, List

from src.utility import utility

# --------------------------------------------------------------------------------------------------
# General action information
# --------------------------------------------------------------------------------------------------


class Actions:
    """The actions an agent can perform."""

    UP: str = "UP"
    """Move one tile up."""

    DOWN: str = "DOWN"
    """Move one tile down."""

    LEFT: str = "LEFT"
    """Move one tile left."""

    RIGHT: str = "RIGHT"
    """Move one tile right."""

    BOMB: str = "BOMB"
    """Place a bomb."""

    WAIT: str = "WAIT"
    """Do nothing."""

    ALL: ClassVar[List[str]] = [UP, DOWN, LEFT, RIGHT, BOMB, WAIT]
    """A list of all actions."""

    INVALID_ID = -1
    """Returned if a given action name is invalid."""

    INVALID_NAME = "Invalid"
    """Returned if a given id is invalid."""

    ACTION_TO_ID: ClassVar[Dict[str, int]] = {
        name: index for index, name in enumerate(ALL)}
    """Dictionary for converting an action name to an integer ID."""

    ID_TO_ACTION: ClassVar[Dict[int, str]] = dict(
        zip(ACTION_TO_ID.values(), ACTION_TO_ID.keys()))
    """Dictionary for converting an action ID to its name."""

    MIN_ID: int = min(ACTION_TO_ID.values())
    """The minimum action ID."""

    MAX_ID: int = max(ACTION_TO_ID.values())
    """The maximum action ID."""

    @staticmethod
    def get_action_id(action: str) -> int:
        """Convert the given action name to the corresponding integer ID."""
        return Actions.ACTION_TO_ID.get(action, Actions.INVALID_ID)

    @staticmethod
    def get_action_name(action_id: int) -> str:
        """Convert the given integer ID to the corresponding action name."""
        return Actions.ID_TO_ACTION.get(action_id, Actions.INVALID_NAME)

    @staticmethod
    def get_normalized_action_value_from_name(action: str) -> float:
        """Get the value of the given action in the action space normalized to [-1, 1]."""
        action_id = Actions.get_action_id(action)
        return Actions.get_normalized_action_value_from_id(action_id)

    @staticmethod
    def get_normalized_action_value_from_id(action_id: int) -> float:
        """Get the value of the given action ID in the action space normalized to [-1, 1]."""
        return utility.remap_value_to_range(action_id, Actions.MIN_ID, Actions.MAX_ID, -1, 1)

    @staticmethod
    def get_action_id_from_normalized_value(normalized_action: float) -> int:
        """Get the action ID for the normalized action value."""
        assert - \
            1 <= normalized_action <= 1, "Value is expected to be in [-1, 1]!"
        return round(
            utility.remap_value_to_range(
                normalized_action, -1, 1, Actions.MIN_ID, Actions.MAX_ID),
        )

    @staticmethod
    def get_action_name_from_normalized_value(normalized_action: float) -> str:
        """Get the action name for the normalized action value."""
        action_id = Actions.get_action_id_from_normalized_value(
            normalized_action)
        return Actions.get_action_name(action_id)

# --------------------------------------------------------------------------------------------------
# Base action set class
# --------------------------------------------------------------------------------------------------


class ActionSet(ABC):
    """Used to define a subset of available actions."""

    def __init__(self) -> None:
        self.actions = []
        self.add_actions()
        self.action_to_id = {name: index for index,
                             name in enumerate(self.actions)}
        self.id_to_action = dict(
            zip(self.action_to_id.values(), self.action_to_id.keys()))
        self.min_id = min(self.action_to_id.values())
        self.max_id = max(self.action_to_id.values())

    def n_actions(self) -> int:
        """Get the number of available actions.

        Returns
        ----------
        n_actions : int
            The number of actions in this set.
        """
        return len(self.actions)

    def min_action_id(self) -> int:
        """Get the minimum ID of the available actions.

        Returns
        ----------
        min_action_id : int
            The minimum action ID.
        """
        return self.min_id

    def max_action_id(self) -> int:
        """Get the maximum ID of the available actions.

        Returns
        ----------
        max_action_id : int
            The maximum action ID.
        """
        return self.max_id

    @abstractmethod
    def add_actions(self) -> None:
        """Add the desired actions to `self.actions`.

        Override to implement the desired functionality.
        """
        pass

    def get_all_actions(self) -> List[str]:
        """Get all registered actions.

        Returns
        ----------
        actions : List[str]
            The action names of this set.
        """
        return self.actions.copy()

    def get_action_id(self, action_name: str) -> int:
        """Get the id of the given action in this set.

        Parameters
        ----------
        action_name : str
            The name of the action.

        Returns
        ----------
        action_id : int
            The action's id in this set.
        """
        return self.action_to_id.get(action_name, Actions.INVALID_ID)

    def get_normalized_action_value_from_name(self, action_name: str) -> float:
        """Get a normalized representation in [-1, 1] of the given action.

        Parameters
        ----------
        action_name : str
            The name of the action to normalize.

        Returns
        ----------
        normalized_action_value : float
            The normalized action value.
        """
        action_id = self.get_action_id(action_name)
        return self.get_normalized_action_value_from_id(action_id)

    def get_normalized_action_value_from_id(self, action_id: int) -> float:
        """Get a normalized representation in [-1, 1] of the given action ID.

        Parameters
        ----------
        action_id : int
            The action ID to normalize.

        Returns
        ----------
        normalized_action_value : float
            The normalized action value.
        """
        return utility.remap_value_to_range(action_id, self.min_id, self.max_id, -1, 1)

    def get_action_id_from_normalized_value(self, action_value: float) -> int:
        """Get the ID of a normalized action value.

        Parameters
        ----------
        action_value : float
            The action value in [-1, 1]

        Returns
        ----------
        action_id : int
            The ID of the action.
        """
        assert -1 <= action_value <= 1, "Value is expected to be in [-1, 1]!"
        return round(utility.remap_value_to_range(action_value, -1, 1, self.min_id, self.max_id))

    def get_action_name_from_normalized_value(self, action_value: float) -> str:
        """Get the name of a normalized action value.

        Parameters
        ----------
        action_value : float
            The action value in [-1, 1]

        Returns
        ----------
        action_name : str
            The name of the action.
        """
        action_id = self.get_action_id_from_normalized_value(action_value)
        return self.get_action_name(action_id)

    def get_action_name(self, action_id: int) -> str:
        """Get the name of the action with the given id in this set.

        Parameters
        ----------
        action_id : int
            The id of the action in this set.

        Returns
        ----------
        action_name : str
            The action's name.
        """
        return self.id_to_action.get(action_id, Actions.INVALID_NAME)


# --------------------------------------------------------------------------------------------------
# Action set implementations
# --------------------------------------------------------------------------------------------------

class OnlyMoveActionSet(ActionSet):
    """Action set only including movement actions (without wait!)."""

    def add_actions(self) -> None:
        self.actions.extend([Actions.UP, Actions.DOWN, Actions.LEFT, Actions.RIGHT])


class PeacefulActionSet(ActionSet):
    """Action set only including movement actions."""

    def add_actions(self) -> None:
        self.actions.extend(
            [Actions.UP, Actions.DOWN, Actions.LEFT, Actions.RIGHT, Actions.WAIT])


class FullActionSet(ActionSet):
    """Action set including all actions."""

    def add_actions(self) -> None:
        self.actions.extend(Actions.ALL)
