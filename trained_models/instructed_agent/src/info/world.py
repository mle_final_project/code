"""This module contains constants and functions to work with the game world."""

# ----------------------------------- Add root folder to PATH. ----------------------------------- #

import os  # noqa: RUF100
import sys  # noqa: RUF100

sys.path.append(os.getcwd())  # noqa: RUF100

# ------------------------------------------------------------------------------------------------ #

import settings
from typing import Tuple
from src.utility import utility


class Tiles:
    """The different tile types of the game filed."""

    WALL = -1
    """A stone wall tile."""

    FREE = 0
    """A free, walkable tile."""

    CRATE = 1
    """A tile containing a destructible crate."""

    COIN = 2
    """A tile containing a coin."""

    BOMB = 3
    """A tile containing a bomb."""

    EXPLOSION = 4
    """A tile containing an explosion."""

    SELF_AGENT = 5
    """A tile containing the agent."""

    OTHER_AGENT = 6
    """A tile containing another agent."""

    MIN_VALUE = WALL
    """The minimum value a tile can have. Use for normalization."""

    MAX_VALUE = OTHER_AGENT
    """The maximum value a tile can have. Use for normalization."""

    @staticmethod
    def normalize(tile_type: int) -> float:
        """Get a normalized representation in [-1, 1] of the given tile type."""
        return utility.remap_value_to_range(tile_type, Tiles.MIN_VALUE, Tiles.MAX_VALUE, -1, 1)

    @staticmethod
    def unnormalize(tile_value: float) -> int:
        """Get the integer ID for the given normalized representation in [-1, 1]."""
        assert -1 <= tile_value <= 1, "Value is expected to be in [-1, 1]!"
        return round(
            utility.remap_value_to_range(tile_value, -1, 1, Tiles.MIN_VALUE, Tiles.MAX_VALUE),
        )


class World:
    """Information about the game world."""

    @staticmethod
    def get_world_width() -> int:
        """Get the width of the game world in tiles (including border)."""
        return settings.COLS

    @staticmethod
    def get_world_height() -> int:
        """Get the height of the game world in tiles (including border)."""
        return settings.ROWS

    @staticmethod
    def get_world_size() -> Tuple[int, int]:
        return (settings.ROWS, settings.COLS)

    @staticmethod
    def get_tile_count() -> int:
        """Get the total number of tiles in the game world (including border)."""
        return settings.COLS * settings.ROWS

    @staticmethod
    def position_to_index(position: Tuple[int, int]) -> int:
        """Convert the given position to an flat tile index.

        Parameters
        ----------
        position : Tuple[int, int]
            The position to convert.

        Returns
        ----------
        index : int
            The flat tile index.
        """
        x, y = position
        width = World.get_world_width()
        height = World.get_world_height()
        assert 0 <= x < width, f"x position {x} is out of range for world with width {width}!"
        assert 0 <= y < height, f"y position {y} is out of range for world with height {height}!"
        return x * height + y

    @staticmethod
    def index_to_position(index: int) -> Tuple[int, int]:
        """Convert the given flat tile index to a position.

        Parameters
        ----------
        index : int
            The flat index to convert.

        Returns
        ----------
        position : Tuple[int, int]
            The position.
        """
        width = World.get_world_width()
        height = World.get_world_height()
        n_tiles = width * height
        out_of_range_msg = f"Index {index} is out of range for world with {n_tiles} tiles!"
        assert 0 <= index < width * height, out_of_range_msg
        return index // width, index % width


class Settings:
    """Information about the game settings."""

    @staticmethod
    def get_max_steps() -> int:
        """Get the number of maximum steps per game.

        Returns
        ----------
        steps : int
            The number of steps.
        """
        return settings.MAX_STEPS

    @staticmethod
    def get_bomb_timer() -> int:
        """Get the time a bomb ticks before exploding.

        Returns
        ----------
        timer : int
            The bomb timer.
        """
        return settings.BOMB_TIMER

    @staticmethod
    def get_bomb_power() -> int:
        """The distance a bomb explosion reaches.

        Returns
        ----------
        bomb_power : int
            The power of a bomb's explosion.
        """
        return settings.BOMB_POWER

    @staticmethod
    def get_explosion_timer() -> int:
        """The time an explosion stays on the game field.

        Returns
        ----------
        timer : int
            The explosion timer.
        """
        return settings.EXPLOSION_TIMER
