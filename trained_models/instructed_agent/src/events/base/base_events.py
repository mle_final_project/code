"""This module contains the base events defined by the game and provides functionality to create and
use additional ones.
"""

from typing import ClassVar, Dict, List


class BaseEvents:
    """The names of the events defined by the game."""

    MOVED_LEFT: str = "MOVED_LEFT"
    MOVED_RIGHT: str = "MOVED_RIGHT"
    MOVED_UP: str = "MOVED_UP"
    MOVED_DOWN: str = "MOVED_DOWN"
    WAITED: str = "WAITED"
    INVALID_ACTION: str = "INVALID_ACTION"
    BOMB_DROPPED: str = "BOMB_DROPPED"
    BOMB_EXPLODED: str = "BOMB_EXPLODED"
    CRATE_DESTROYED: str = "CRATE_DESTROYED"
    COIN_FOUND: str = "COIN_FOUND"
    COIN_COLLECTED: str = "COIN_COLLECTED"
    KILLED_OPPONENT: str = "KILLED_OPPONENT"
    KILLED_SELF: str = "KILLED_SELF"
    GOT_KILLED: str = "GOT_KILLED"
    OPPONENT_ELIMINATED: str = "OPPONENT_ELIMINATED"
    SURVIVED_ROUND: str = "SURVIVED_ROUND"

    REWARD_MAP: ClassVar[Dict[str, float]] = {
        MOVED_LEFT: 0,
        MOVED_RIGHT: 0,
        MOVED_UP: 0,
        MOVED_DOWN: 0,
        WAITED: 0,
        INVALID_ACTION: -1,
        BOMB_DROPPED: 5,
        BOMB_EXPLODED: 5,
        CRATE_DESTROYED: 5,
        COIN_FOUND: 1,
        COIN_COLLECTED: 5,
        KILLED_OPPONENT: 0,
        KILLED_SELF: -1000,
        GOT_KILLED: -1000,
        OPPONENT_ELIMINATED: 10,
        SURVIVED_ROUND: 0,
    }
    """The rewards that are associated with the default game events."""

    @staticmethod
    def movement_events() -> List[str]:
        """Get all movement related events.

        Returns
        ----------
        movement_event_names : List[str]
            The movement event names.
        """
        return [
            BaseEvents.MOVED_LEFT,
            BaseEvents.MOVED_RIGHT,
            BaseEvents.MOVED_UP,
            BaseEvents.MOVED_DOWN,
            BaseEvents.WAITED,
        ]

    @staticmethod
    def bomb_events() -> List[str]:
        """Get all bomb related events.

        Returns
        ----------
        movement_event_names : List[str]
            The bomb related event names.
        """
        return [
            BaseEvents.BOMB_DROPPED,
            BaseEvents.BOMB_EXPLODED,
            BaseEvents.CRATE_DESTROYED,
            BaseEvents.COIN_FOUND,
            BaseEvents.KILLED_OPPONENT,
            BaseEvents.KILLED_SELF,
            BaseEvents.OPPONENT_ELIMINATED,
        ]

    @staticmethod
    def coin_events() -> List[str]:
        """Get all coin related events.

        Returns
        ----------
        movement_event_names : List[str]
            The coin related event names.
        """
        return [
            BaseEvents.COIN_FOUND,
            BaseEvents.COIN_COLLECTED,
        ]

    @staticmethod
    def all_events() -> List[str]:
        """Get all base event names.

        Returns
        ----------
        base_event_names : List[str]
            The base event names.
        """
        return [
            BaseEvents.MOVED_LEFT,
            BaseEvents.MOVED_RIGHT,
            BaseEvents.MOVED_UP,
            BaseEvents.MOVED_DOWN,
            BaseEvents.WAITED,
            BaseEvents.INVALID_ACTION,
            BaseEvents.BOMB_DROPPED,
            BaseEvents.BOMB_EXPLODED,
            BaseEvents.CRATE_DESTROYED,
            BaseEvents.COIN_FOUND,
            BaseEvents.COIN_COLLECTED,
            BaseEvents.KILLED_OPPONENT,
            BaseEvents.KILLED_SELF,
            BaseEvents.GOT_KILLED,
            BaseEvents.OPPONENT_ELIMINATED,
            BaseEvents.SURVIVED_ROUND,
        ]

    @staticmethod
    def is_base_event(event_name: str) -> bool:
        """Check if the given event name is a event provided by the game.

        Parameters
        ----------
        event_name : str
            The name of the event to check.

        Returns
        ----------
        is_base_event : bool
            True if the event is a event provided by the game, else False.
        """
        return event_name in BaseEvents.REWARD_MAP
