from abc import ABC, abstractmethod
from typing import Any, Dict, List, Optional, Union

from src.utility.utility import camel_case_to_snake_case

Reward = Union[float, None]


class Event(ABC):
    def __init__(self, base_reward: float, name: Optional[str] = None) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The reward value to use as base value for the event.

        name : Optional[str]
            The event name to use. If None, use the class name.
        """
        self.base_reward = base_reward
        if name is not None:
            self.name = name
        else:
            self.name = camel_case_to_snake_case(type(self).__name__, True).split("_EVENT")[0]

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return self.name

    @abstractmethod
    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Optional[Dict[str, Any]],
        self_action: str,
        events: List[str],
    ) -> Reward:
        """Compute the reward associated with this event for the given state transition.

        Override this method to implement the event's functionality.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        new_game_state : Optional[Dict[str, Any]]
            The game state after the transition. Is `None` in last state transition.

        self_action : str
            The name of the action performed by the agent.

        events : List[str]
            The base game events that took place in the given transition.

        Returns
        ----------
        reward : Reward
            The reward the agent should get for this reward. None if the event did not take place
            during the given transition.
        """
        pass
