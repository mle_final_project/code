from types import SimpleNamespace
from typing import Any, Dict, List, Optional

from src.events import BaseEvents, Event, Reward
from src.features.implementations.instructions import (
    CoinDistanceFeatureExtractor,
    InstructionExtractor,
    SafeBombsExtractor,
    ShortestDirectionToSafetyExtractor,
)
from src.info.actions import Actions
from src.info.state import State
from src.info.world import Tiles
from src.utility import utility


class FollowInstructionsEvent(Event):
    """Reward the agent if it follows the instructions."""

    def __init__(
        self,
        base_reward: float = 1,
        max_abs_reward: float = 12,
        name: str | None = None,

    ) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The reward value to use as base value for the event.

        max_abs_reward : float
            The maximal absolut reward to give per transition.

        name : Optional[str]
            The event name to use. If None, use the class name.
        """
        super().__init__(base_reward, name)
        self.max_abs_reward = max_abs_reward
        self.coin_distance_feature_extractor: CoinDistanceFeatureExtractor = CoinDistanceFeatureExtractor()
        self.shortest_direction_to_safety_extractor: ShortestDirectionToSafetyExtractor = ShortestDirectionToSafetyExtractor()
        self.safe_bombs_extractor: SafeBombsExtractor = SafeBombsExtractor()
        self.error_id = 0

    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        _new_game_state: Optional[Dict[str, Any]],
        _self_action: str,
        events: List[str],

    ) -> Reward:
        """Reward the agent if it followed the instruction.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        new_game_state : Optional[Dict[str, Any]]
            The game state after the transition. Is `None` in last state transition.

        self_action : str
            The name of the action performed by the agent.

        events : List[str]
            The base game events that took place in the given transition.

        Returns
        ----------
        reward : Reward
            The reward the agent should get for this reward. None if the event did not take place
            during the given transition.
        """
        '''
        features: List[int] = []

        cdfe : CoinDistanceFeatureExtractor = CoinDistanceFeatureExtractor()
        features.extend(cdfe.extract_features(SimpleNamespace(), old_game_state)[0].tolist())

        sdtse : ShortestDirectionToSafetyExtractor = ShortestDirectionToSafetyExtractor()
        features.extend(sdtse.extract_features(SimpleNamespace(), old_game_state)[0].tolist())

        sbe : SafeBombsExtractor = SafeBombsExtractor()
        features.extend(sbe.extract_features(SimpleNamespace(), old_game_state)[0].tolist())


        if features[4] != -1:
            if features[4] == actions.Actions.ACTION_TO_ID[_self_action]:
                return 1
            return -1


        if features[5] == 1:
            if _self_action == actions.Actions.BOMB:
                return 1
            return -1


        move_features = features[0:4]

        """Move towards largest direction value."""
        index = np.argmax(move_features).item()
        if index == 0:
            if _self_action == actions.Actions.LEFT:
                return 1
            return -1
        if index == 1:
            if _self_action == actions.Actions.RIGHT:
                return 1
            return -1
        if index == 2:
            if _self_action == actions.Actions.UP:
                return 1
            return -1
        if index == 3:
            if _self_action == actions.Actions.DOWN:
                return 1
            return -1

        return None
        '''

        """
        features: InstructionExtractor = InstructionExtractor()
        feature = int(float(features.extract_features(SimpleNamespace(), old_game_state)[0]))
        # print("event:", Actions.get_action_name(feature))

        if (feature == Actions.get_action_id(_self_action)):
            return 1
        return -1
        """

        features = [0, 0, 0]

        features[0] = int(
            self.coin_distance_feature_extractor.extract_features(
                SimpleNamespace(),
                old_game_state)[0])
        features[1] = int(
            self.shortest_direction_to_safety_extractor.extract_features(
                SimpleNamespace(),
                old_game_state)[0])
        features[2] = int(
            self.safe_bombs_extractor.extract_features(
                SimpleNamespace(),
                old_game_state)[0])

        # print("fratuire 1", int(features[1]))
        if features[1] != -1:
            # print("e: ", Actions.get_action_name(int(features[1])))
            if Actions.get_action_name(features[1]) == _self_action:
                return 1
            self.error_id += 1
            print("exp: Run ", Actions.get_action_name(
                features[1]), " got: ", _self_action, " \t", self.error_id)
            return -1

        if features[2] == 1:
            # print("e: Bomb")
            if _self_action == Actions.BOMB:
                return 1
            self.error_id += 1
            print("exp: ", Actions.BOMB, " got: ", _self_action, " \t", self.error_id)
            return -1

        if features[0] != -1:
            # print("e: ", Actions.get_action_name(features[0]))
            if Actions.get_action_name(features[0]) == _self_action:
                return 1
            self.error_id += 1
            print("exp: Walk", Actions.get_action_name(
                features[0]), " got: ", _self_action, " \t", self.error_id)
            return -1

        # print("e: wait")
        if _self_action == Actions.WAIT:
            return 1
        self.error_id += 1
        print("exp: ", Actions.WAIT, " got: ", _self_action, " \t", self.error_id)
        return -1
