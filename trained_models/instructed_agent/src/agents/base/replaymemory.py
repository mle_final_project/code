"""This module contains the a circular memory buffer implementation adapted from
https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html.
"""

import random
from typing import List, NamedTuple, Optional

from torch import Tensor


class Transition(NamedTuple):
    """A game state transition."""
    old_features: Tensor
    action: Tensor
    new_features: Optional[Tensor]
    reward: Tensor


class ReplayMemory:
    """Cyclic memory buffer with fixed capacity."""

    def __init__(self, capacity: int) -> None:
        self.capacity = capacity
        self.memory = []

    def push(self, transition: Transition) -> None:
        """Push a list of transitions into memory.

        Parameters
        ----------
        args : Transition
            The transitions to push.
        """
        if len(self.memory) == self.capacity:
            self.memory.pop(0)
        self.memory.append(transition)

    def sample(self, batch_size: int) -> List[Transition]:
        """Sample a number of Transitions from memory.

        Parameters
        ----------
        batch_size : int
            The number of Transitions to sample.

        Returns
        ----------
        samples : List[Transition]
            The sampled Transitions.
        """
        return random.sample(self.memory, batch_size)

    def __len__(self) -> int:
        return len(self.memory)
