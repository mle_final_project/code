"""This module contains the agent implementation."""

import os
import pickle
from abc import ABC, abstractmethod
from logging import Logger
from types import SimpleNamespace
from typing import Any, Dict, List, Optional, Union

import torch
from torch import Tensor

from src.agents.base.prioritized_replaymemory import PrioritizedReplayMemory
from src.agents.base.replaymemory import ReplayMemory
from src.features import FeatureExtractor
from src.info.actions import ActionSet
from src.info.logging import DataLogger


class Agent(ABC):
    """Base Agent class used to implement custom game agents."""

    USE_INTERNAL_STEP_VALUE: int = -1
    """Use in select_action to use internally tracked step."""

    def __init__(
        self,
        feature_extractor: FeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        memory_size: int = 10000,
        use_prioritized_sampling: bool = False,
        prioritization_alpha: float = 0.6,
        prioritization_beta: float = 0.4,
        prioritization_eps: float = 1e-2,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
        data_logger: Optional[DataLogger] = None,
        name: Optional[str] = None,
    ) -> None:
        """Initialize an agent instance.

        Parameters
        ----------
        feature_extractor : FeatureExtractor
            The feature extractor to use.

        action_set : ActionSet
            The set of actions the agent can use.

        device : torch.device
            The device to use for this agent.

        memory_size : int
            The size of the internal replay memory.

        use_prioritized_sampling : bool
            Determines if a prioritized replay buffer is used.

        prioritization_alpha : float
            The alpha value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        prioritization_beta : float
            The beta value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        prioritization_eps : float
            The epsilon value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        do_auto_save : bool
            If `True`, save the model automatically.

        save_interval : int
            If `do_auto_save` is `True`, save the model after this number of optimization steps have
            passed.

        save_path : Union[str, os.PathLike]
            The path to save the model to.

        logger : Optional[Logger]
            The logger to use for logging messages.

        data_logger : Optional[DataLogger]
            The data logger to use for logging data.

        name: Optional[str]
            The name of this agent.
        """
        self.feature_extractor = feature_extractor
        self.action_set = action_set
        self.memory_size = memory_size
        self.use_prioritized_sampling = use_prioritized_sampling
        if self.use_prioritized_sampling:
            self.memory = PrioritizedReplayMemory(
                capacity=memory_size,
                alpha=prioritization_alpha,
                beta=prioritization_beta,
                eps=prioritization_eps,
            )
        else:
            self.memory = ReplayMemory(memory_size)
        self.do_auto_save = do_auto_save
        self.save_interval = save_interval
        self.save_path = save_path
        self.data_logger = data_logger
        self.logger = logger
        self.rounds_since_creation = 0
        self.steps_since_creation = 0
        self.last_round = 0
        self.total_choices = 0
        self.optimization_steps_since_creation = 0
        self.name = name if name else type(self).__name__
        self.device = device

    # ----------------------------------------------------------------------------------------------
    # Public interface
    # ----------------------------------------------------------------------------------------------

    def set_device(self, device: torch.device) -> None:
        """Set the device to use.

        Parameters
        ----------
        device : torch.device
            The device to use.
        """
        self.device = device
        self.on_device_changed()

    def log_message(self, message: str, log_as_error: bool = False) -> None:
        """Log a message.

        Uses the internal logger or prints the message if none was specified.

        Parameters
        ----------
        message : str
            The message to log.

        is_error : bool
            Mark this message as error.
        """
        if self.logger is not None:
            self.logger.info(message) if not log_as_error else self.logger.error(message)
        else:
            print(message) if not log_as_error else print(f"Error: {message}")

    def preprocess_features(self, features: Tensor, _game_state: Dict[str, Any]) -> Tensor:
        """Preprocess a set of features extracted by a `FeatureExtractor`.

        Called automatically in the game callbacks before further processing takes place.

        Override this method to define agent specific features or add additional feature data you
        might need for your implementation.

        Parameters
        ----------
        features : Tensor
            The already extracted features.

        game_state : Dict[str, Any]
            The game state the features were extracted from.

        Returns
        ----------
        processed_features : Tensor
            The processed features.
        """
        return features

    def select_action(
        self,
        features: torch.Tensor,
        current_round: int,
        current_step: int,
    ) -> int:
        """Select an action based on the provided state features and the current time step.

        Override `on_select_action` to implement the desired behavior.

        Parameters
        ----------
        features : torch.Tensor
            The feature tensor.

        current_round : int
            The current game round.

        current_step : int
            The step in the current round.

        Returns
        ----------
        action_id : int
            The ID of the selected action.
        """
        if self.last_round != current_round:
            self.rounds_since_creation += 1
            self.last_round = current_round
        self.steps_since_creation += 1
        self.total_choices += 1
        return self._select_action_implementation(features, current_round, current_step)

    def process_rewards(
        self,
        _self_namespace: SimpleNamespace,
        _old_game_state: Dict[str, Any],
        _self_action: int,
        _new_game_state: Optional[Dict[str, Any]],
        _total_reward: float,
        _reward_per_source: Dict[str, float],
    ) -> float:
        """Add additional agent specific events and rewards.

        Override to implement the desired behavior.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : int
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in final state transition.

        total_reward : float
            The total reward computed for this transition.

        reward_per_source : Dict[str, float]
            The individual rewards per processed event.

        Returns
        ----------
        additional_reward : float
            The total additional reward to add.
        """
        return 0

    def pre_optimize(
        self,
        _self_namespace: SimpleNamespace,
        _old_game_state: Dict[str, Any],
        _self_action: int,
        _new_game_state: Optional[Dict[str, Any]],
        _total_reward: float,
        _reward_per_source: Dict[str, float],
    ) -> bool:
        """Perform additional steps before the `optimize_model` method is called.

        Override to implement the desired behavior.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : int
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in final state transition.

        total_reward : float
            The total reward computed for this transition.

        reward_per_source : Dict[str, float]
            The individual rewards per processed event.

        Returns
        ----------
        should_optimize : bool
            If `False`, do not optimize the model in this step.
        """
        return True

    def optimize_model(self) -> None:
        """Perform one network optimization.

        Override `on_optimize_model` to implement the desired behavior.
        """
        self._optimize_model_implementation()
        self.optimization_steps_since_creation += 1
        if self.data_logger is not None:
            self.data_logger.log_datum("OPTIMIZATION", 1)

    def post_optimize(
        self,
        _self_namespace: SimpleNamespace,
        _old_game_state: Dict[str, Any],
        _self_action: int,
        _new_game_state: Optional[Dict[str, Any]],
        _total_reward: float,
        _reward_per_source: Dict[str, float],
    ) -> None:
        """Perform additional steps after the `optimize_model` method was called.

        Override to implement the desired behavior.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : int
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in final state transition.

        total_reward : float
            The total reward computed for this transition.

        reward_per_source : Dict[str, float]
            The individual rewards per processed event.
        """
        return

    def reset_choice_counter(self) -> None:
        """Reset the internal choice counter to zero."""
        self.total_choices = 0

    def save_model(self, path: Optional[Union[str, os.PathLike]] = None) -> None:
        """Save the current model state.

        Override `on_save_model` to implement the desired behavior.
        """
        if path is None:
            path = self.save_path
        if self.do_auto_save and self.rounds_since_creation % self.save_interval == 0:
            self._save_model_implementation(path, f"{self.name}_{self.rounds_since_creation}")
        self._save_model_implementation(path, self.name)

    def load_model(self, path: Optional[Union[str, os.PathLike]] = None) -> None:
        """Load a previous model state.

        Override `on_load_model` to implement the desired behavior.
        """
        if path is None:
            path = self.save_path
        self._load_model_implementation(path, self.name)

    def try_save_replays(self, path: Optional[Union[str, os.PathLike]] = None) -> None:
        """Save the current replay memory state."""
        if path is None:
            path = self.save_path
        try:
            with open(f"{path}/{self.name}_memory", mode="wb") as memory_file:
                pickle.dump(self.memory, memory_file)
        except (pickle.PickleError, Exception) as e:
            self.log_message(f"Could not save replays. Error message: {e}", log_as_error=True)
        else:
            self.log_message(f"Saved {len(self.memory)} replays.")

    def try_load_replays(self, path: Optional[Union[str, os.PathLike]] = None) -> None:
        """Load a previously saved replay memory state."""
        if path is None:
            path = self.save_path
        try:
            with open(f"{path}/{self.name}_memory", mode="rb") as memory_file:
                self.memory = pickle.load(memory_file)
        except (FileNotFoundError, pickle.UnpicklingError) as e:
            self.log_message(f"Failed to load replay memory. Error message: {e}", log_as_error=True)
        else:
            self.log_message(f"Loaded {len(self.memory)} transitions into replay buffer.")

    def clear_model(self) -> None:
        """Reset the model to its default state.

        Override `on_clear_model` to implement the desired behavior.
        """
        self._clear_model_implementation()

    # ----------------------------------------------------------------------------------------------
    # Event reactions
    # ----------------------------------------------------------------------------------------------

    def on_device_changed(self) -> None:
        """React to a device change.

        Override this method if the agent needs to perform any additional steps when the internal
        device is changed.
        """
        return

    def on_end_of_round(
        self,
        _last_game_state: Dict[str, Any],
        _last_action: str,
        _events: List[str],
    ) -> None:
        """Perform additional steps after a round was completed.

        Parameters
        ----------
        last_game_state : Dict[str, Any]
            The game state at the end of the game.

        last_action : str
            The last action that was performed by the agent.

        events : List[str]
            The events that occurred during the state transition.
        """
        return

    def on_end_of_session(self) -> None:
        """Called when the final game is completed.

        Override this method to add additional steps before deletion takes place.
        """
        return

    # ----------------------------------------------------------------------------------------------
    # Internal methods
    # ----------------------------------------------------------------------------------------------

    @abstractmethod
    def _select_action_implementation(
        self,
        features: torch.Tensor,
        current_round: int,
        current_step: int,
    ) -> int:
        """Select and action based on the given feature tensor and the current time step.

        Parameters
        ----------
        features : torch.Tensor
            The features to use.

        current_round : int
            The current game round.

        current_step : int
            The current round step.

        Returns
        ----------
        action_id : int
            The ID of the selected action.
        """
        pass

    def _optimize_model_implementation(self) -> None:
        """Perform one model optimization step."""
        return

    def _save_model_implementation(
        self,
        _directory: Union[str, os.PathLike],
        _file_name_base: str,
    ) -> None:
        """Save the current model state.

        Parameters
        ----------
        directory : PathLike
            The directory to save to.

        file_name_base : str
            The base of the file name to use. Can be extended but should not be prefixed.
        """
        return

    def _load_model_implementation(
        self,
        _directory: Union[str, os.PathLike],
        _file_name_base: str,
    ) -> None:
        """Load a previous model state.

        Parameters
        ----------
        directory : PathLike
            The directory to load from.

        file_name_base : str
            The base of the file name to use. Can be extended but should not be prefixed.
        """
        return

    def _clear_model_implementation(self) -> None:
        """Reset the model state to its default values."""
        return

    def _setup_training_implementation(self) -> None:
        """Perform additional training initialization steps."""
        return
