"""This module contains the definition of the agent's Q-network."""

import torch
from torch import nn

from src.agents.base.dqnetwork import DQNetwork


class FullyConnectedDQNetwork(DQNetwork):
    """A fully connected neural network to use as Q approximator."""

    def __init__(
        self,
        n_in: int,
        n_out: int,
        n_inner: int,
        n_hidden: int = 3,
    ) -> None:
        """Create a new network instance.

        Parameters
        ----------
        n_in : int
            The number of inputs.

        n_out : int
            The number of outputs.

        n_inner : int
            The number of neurons inside the hidden layers.

        n_hidden : int
            The number of hidden layers to use. The network will consist of at least one input and
            one output layer.
        """
        super().__init__()
        self.n_hidden = n_hidden
        self.output_count = n_out

        # If no hidden layers are used, we do not need non-linearity.
        if n_hidden == 0:
            self.layers = nn.Sequential(nn.Linear(n_in, n_out))
            return

        # Else, add linear layer blocks.
        self.layers = nn.Sequential()
        for i in range(n_hidden):
            self.layers.append(
                nn.Sequential(
                    nn.Linear(n_inner if i != 0 else n_in, n_inner),
                    nn.ReLU(),
                ),
            )

        # Lastly, append linear output layer for regression.
        self.layers.append(nn.Linear(n_inner, n_out))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Perform a forward pass on the given feature vector.

        Parameters
        ----------
        x : Tensor
            The feature vector.

        Returns
        ----------
        y : Tensor
            The predicted values.
        """
        for layer in self.layers:
            x = layer(x)
        return x

    def save_model(self, path: str) -> None:
        """Save the model state to the given path.

        Parameters
        ----------
        path : str
            The path to use.
        """
        torch.save(self.state_dict(), path)

    def load_model(self, path: str) -> None:
        """Load the model state from the given path.

        Parameters
        ----------
        path : str
            The path to use.
        """
        self.load_state_dict(torch.load(path))

    def n_out(self) -> int:
        """Get the number of outputs of this network.

        Returns
        ----------
        n_out : int
            The number of network outputs.
        """
        return self.output_count
