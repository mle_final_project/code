"""This module defines the callbacks used by the game for letting the agent act."""

# ----- Add local src path to Python PATH to make imports work in the tournament environment ----- #

import pathlib
import sys

agent_file_path = pathlib.Path(__file__)  # noqa: RUF100
sys.path.append(f"{agent_file_path.parent}")  # noqa: RUF100

# ------------------------------------------------------------------------------------------------ #

from typing import List, Optional, Tuple

import torch

from src import events, features
from src.agents import DQNAgent
from src.events import BaseEvents, Event
from src.info.actions import FullActionSet
from src.setup.callback_generation import generate_callbacks

GAMMA = 0.9975

EPS_START = 0.9
EPS_END = 0.0
EPS_DECAY = 60

TAU = 1
LR = 1e-3

BATCH_SIZE = 32
N_INNER = 64
N_HIDDEN = 1
MEMORY_SIZE = 1000

UPDATE_INTERVAL = 1
TARGET_UPDATE_INTERVAL = 1

BASE_EVENTS: List[Tuple[str, Optional[float]]] = [
    (BaseEvents.INVALID_ACTION, 0),
    (BaseEvents.COIN_COLLECTED, 0),
    (BaseEvents.MOVED_UP, 0),
    (BaseEvents.MOVED_DOWN, 0),
    (BaseEvents.MOVED_LEFT, 0),
    (BaseEvents.MOVED_RIGHT, 0),
    (BaseEvents.WAITED, 0),
    (BaseEvents.BOMB_DROPPED, 0),
    (BaseEvents.KILLED_SELF, 0),
    (BaseEvents.CRATE_DESTROYED, 0),
]

CUSTOM_EVENTS: List[Event] = [
    events.FollowInstructionsEvent(),
]

FEATURES: List[features.FeatureExtractor] = [
    features.CoinDistanceFeatureExtractor(),
    features.ShortestDirectionToSafetyExtractor(),
    features.SafeBombsExtractor(),
]

AGENT = DQNAgent(
    feature_extractor=features.FeatureExtractorCollection(FEATURES),
    action_set=FullActionSet(),
    device=torch.device("cpu"),
    batch_size=BATCH_SIZE,
    gamma=GAMMA,
    eps_start=EPS_START,
    eps_end=EPS_END,
    eps_decay=EPS_DECAY,
    tau=TAU,
    lr=LR,
    n_inner=N_INNER,
    n_hidden=N_HIDDEN,
    memory_size=MEMORY_SIZE,
    do_random_choices=False,
    do_auto_save=False,
    update_interval=UPDATE_INTERVAL,
    target_update_interval=TARGET_UPDATE_INTERVAL,
    name="instructed_agent",
)

setup, act = generate_callbacks(
    AGENT,
    custom_events=CUSTOM_EVENTS,
    base_events=BASE_EVENTS,
)
