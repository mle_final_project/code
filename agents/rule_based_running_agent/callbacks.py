"""This module defines the callbacks used by the game for letting the agent act."""

from typing import List

import torch

from src import features
from src.agents import RuleBasedRunningAgent
from src.features import FeatureExtractorCollection
from src.features import MovementAllowedFeatureExtractor as MovAllow
from src.info.actions import PeacefulActionSet
from src.setup import generate_callbacks

FEATURES: List[features.FeatureExtractor] = [
    MovAllow(True),
]

AGENT = RuleBasedRunningAgent(
    feature_extractor=FeatureExtractorCollection(FEATURES),
    action_set=PeacefulActionSet(),
    device=torch.device("cpu"),
)

setup, act = generate_callbacks(AGENT, custom_events=[])
