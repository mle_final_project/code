"""This module defines the callbacks used by the game for letting the agent act."""

import torch

from src.agents import RuleBasedCoinAgent
from src.features import CoinInfluenceFeatureExtractor
from src.info import PeacefulActionSet
from src.setup import generate_callbacks

AGENT = RuleBasedCoinAgent(
    feature_extractor=CoinInfluenceFeatureExtractor(
        base_value=1,
        min_value=0.001,
        max_value=2,
        closest_bias=0.99,
        influence_range=17 * 2,
        random_eps=0.0001,
        value_decrease_func=lambda x: 0.1 * x,
        radius=1,
        include_diagonals=False,
        normalize=True,
        cache_features=True,
    ),
    action_set=PeacefulActionSet(),
    device=torch.device("cpu"),
)

setup, act = generate_callbacks(AGENT, custom_events=[])
