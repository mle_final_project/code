"""This module defines the callbacks used by the game for letting the agent train."""


from src.setup import generate_callbacks_training

setup_training, game_events_occurred, end_of_round = generate_callbacks_training()
