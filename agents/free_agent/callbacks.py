"""This module defines the callbacks used by the game for letting the agent act."""

import os
from typing import List, Optional, Tuple

import torch

from src import events, features, setup
from src.agents import DQNAgentCpot4split
from src.events import BaseEvents, Event
from src.features import (
    FeatureExtractorCollection,
    FourSplitFeatureExtractor,
    FourSplitSurroundingFeatureExtractor,
)
from src.features import MovementAllowedFeatureExtractor as MovAllow
from src.info.actions import PeacefulActionSet

GAMMA = 0.99

EPS_START = 0.8
EPS_END = 0.05
EPS_DECAY = 10000

TAU = 0.005
LR = 0.0000001

BATCH_SIZE = 4
N_INNER = 16
N_LAYERS = 2
MEMORY_SIZE = 10000
DEVICE = "cpu"
USE_4SPLIT = True#False
CONV_KERNEL_SHAPE = (3, 3)
CONV_INPUT_SHAPE = (3, 3)
N_CONV_CHANNELS = 4
N_CONV_LAYERS = 1
BATCH_NORMALIZATION = False
RELU = True

UPDATE_INTERVAL = 4
TARGET_UPDATE_INTERVAL = 500

BASE_EVENTS: List[Tuple[str, Optional[float]]] = [
    (BaseEvents.INVALID_ACTION, -0.5),
    (BaseEvents.COIN_COLLECTED, 1),
    (BaseEvents.WAITED, -0.5),
    (BaseEvents.MOVED_UP, 0.0),
    (BaseEvents.MOVED_DOWN, 0.0),
    (BaseEvents.MOVED_LEFT, 0.0),
    (BaseEvents.MOVED_RIGHT, 0.0),
]

CUSTOM_EVENTS: List[Event] = [
    events.CoinPotentialAuxiliaryReward(
        round_multiplier_func=lambda x: x,
        max_abs_reward=0.5,
        #extend=1,
    ),
]
FEATURES: List[features.FeatureExtractor] = [
    FourSplitSurroundingFeatureExtractor(
        radius=1,
        include_diagonals=True,
        cache_features=False,
        using_4split=USE_4SPLIT,
        normalize=True,
    ),
    # FourSplitFeatureExtractor(),
    MovAllow(USE_4SPLIT),
    # CMap(USE_4SPLIT, 4), # more than accuracy 4 introduces a lot of flakyness (large state space)
]

AGENT = DQNAgentCpot4split(
    feature_extractor=FeatureExtractorCollection(FEATURES),
    action_set=PeacefulActionSet(),
    device=torch.device(DEVICE),
    batch_size=BATCH_SIZE,
    gamma=GAMMA,
    eps_start=EPS_START,
    eps_end=EPS_END,
    eps_decay=EPS_DECAY,
    tau=TAU,
    lr=LR,
    n_inner=N_INNER,
    conv_input_shape=CONV_INPUT_SHAPE,
    n_conv_channels=N_CONV_CHANNELS,
    conv_kernel_shape=CONV_KERNEL_SHAPE,
    n_conv_layers=N_CONV_LAYERS,
    batch_normalization=BATCH_NORMALIZATION,
    relu=RELU,
    memory_size=MEMORY_SIZE,
    do_random_choices=False,#True,
    do_auto_save=False,
    save_path=os.path.abspath(os.curdir) + "/models",
    use_4split=USE_4SPLIT,
    name="free_agent",
    update_interval=UPDATE_INTERVAL,
    target_update_interval=TARGET_UPDATE_INTERVAL,
)

setup, act = setup.generate_callbacks(
    AGENT,
    custom_events=CUSTOM_EVENTS,
    base_events=BASE_EVENTS,
    device=torch.device(DEVICE),
)
