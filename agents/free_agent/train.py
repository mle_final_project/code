"""This module defines the callbacks used by the game for letting the agent train."""

from src.setup import callback_generation_training

setup_training, game_events_occurred, end_of_round = callback_generation_training.generate_callbacks()
