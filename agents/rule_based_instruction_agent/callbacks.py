"""This module defines the callbacks used by the game for letting the agent act."""

from typing import List
import torch

from src.agents import RuleBasedITiger
from src import features
from src.events.implementations.follow_instructions import FollowInstructionsEvent
from src.info.actions import FullActionSet
from src.setup import generate_callbacks

FEATURES: List[features.FeatureExtractor] = [
    features.CoinDistanceFeatureExtractor(),
    features.ShortestDirectionToSafetyExtractor(),
    features.SafeBombsExtractor(),
]

AGENT = RuleBasedITiger(
    feature_extractor=features.FeatureExtractorCollection(FEATURES),
    action_set=FullActionSet(),
    device=torch.device("cpu"),
    name="rule_based_i_tiger",
)

setup, act = generate_callbacks(AGENT, custom_events=[FollowInstructionsEvent()])
