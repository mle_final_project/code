# MLE 2023 Final Project - Reinforcement Learning in Bomberman

This is the repository containing the code we have written over the three weeks we worked on the final project for the _Machine Learning Essentials_ lecture.

## Project Structure

The `src/` directory contains all the "backend" code we have written.

Our scripts for launching the game and our live-plotter implementation can be found in `scripts/`.

In the `agents/` folder, we include some of our other agent setups we used for experimenting with different feature configurations. They can be used by running `scripts/launch_game.py` from inside the project root, but do **not work** when put directly into the game folder due to some import related issues.

For portable versions of our final agents, please use the pre-trained models in `trained_models/` ready to be placed into the `agent_code/` directory of the [Bomberman game](https://github.com/ukoethe/bomberman_rl) that was provided to use for this project.
