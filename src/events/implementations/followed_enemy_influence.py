from types import SimpleNamespace
from typing import Any, Dict, List, Optional

import numpy as np

from src.events.base.event import Event, Reward
from src.features.implementations.interest_rating import EnemyInfluenceFeatureExtractor
from src.info.actions import Actions


class FollowedEnemyInfluenceEvent(Event):
    def __init__(
        self,
        enemy_influence: EnemyInfluenceFeatureExtractor,
        base_reward: float = 1.0,
        name: Optional[str] = None,
    ) -> None:
        super().__init__(base_reward, name)
        self.enemy_influence = enemy_influence

    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Optional[Dict[str, Any]],
        self_action: str,
        _events: List[str],
    ) -> Reward:
        if new_game_state is None:
            return None

        old_features = self.enemy_influence.extract_features(
            SimpleNamespace(),
            old_game_state,
        ).numpy()[0]

        # We do not want to punish if all values are equal.
        if np.all(old_features == old_features[0]):
            return None

        # Get maximal index of old features to determine which action should have been taken.
        max_index = np.argmax(old_features)

        # Else check if the maximum action was performed.
        reward = None
        if self_action == Actions.LEFT:
            reward = self.base_reward if max_index == 1 else -self.base_reward
        if self_action == Actions.RIGHT:
            reward = self.base_reward if max_index == 2 else -self.base_reward
        if self_action == Actions.UP:
            reward = self.base_reward if max_index == 3 else -self.base_reward
        if self_action == Actions.DOWN:
            reward = self.base_reward if max_index == 4 else -self.base_reward

        return reward
