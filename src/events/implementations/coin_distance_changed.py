from typing import Any, Dict, List

import numpy as np

from src.events.base.event import Event, Reward
from src.info.state import State
from src.utility import utility


class CoinDistanceChangedEvent(Event):
    """Event that rewards the agent for closing the distance to one or more coins during a state
    transition and punishes it for moving away.
    """

    def __init__(
        self,
        base_reward: float = 1,
        max_abs_reward: float = 100,
        name: str | None = None,
    ) -> None:
        """Initialize an event instance.

        Parameters
        ----------
        base_reward : float
            The reward value to use as base value for the event. Used as multiplier for the total
            distance difference.

        max_abs_reward : float
            The maximum absolute reward to give for a single state transition.

        name : Optional[str]
            The event name to use. If None, use the class name.
        """
        super().__init__(base_reward, name)
        self.max_abs_reward = max_abs_reward

    def calculate_reward(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Dict[str, Any] | None,
        _self_action: str,
        _events: List[str],
    ) -> Reward:
        """Reward the agent if it approached one or more coins during the given state transition
        measured by the overall distance reduction. If the reduction is negative, punish it instead.

        Parameters
        ----------
        old_game_state : Dict[str, Any]
            The previous game state.

        new_game_state : Optional[Dict[str, Any]]
            The game state after the transition. Is `None` in last state transition.

        self_action : str
            The name of the action performed by the agent.

        events : List[str]
            The base game events that took place in the given transition.

        Returns
        ----------
        reward : Reward
            The reward the agent should get for this reward. None if the event did not take place
            during the given transition.
        """
        if new_game_state is None:
            return None
        old_coin_positions = State.get_coins(old_game_state)
        old_agent_position = State.get_self_values(old_game_state).position
        new_agent_position = State.get_self_values(new_game_state).position
        old_coin_distances = utility.manhattan_distance(old_agent_position, old_coin_positions)
        new_coin_distances = utility.manhattan_distance(new_agent_position, old_coin_positions)
        total_difference = np.sum(old_coin_distances) - np.sum(new_coin_distances)
        return utility.clamp(
            self.base_reward * total_difference,
            -self.max_abs_reward,
            self.max_abs_reward,
        )
