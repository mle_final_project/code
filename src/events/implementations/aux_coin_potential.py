
from typing import Any, Callable, Dict, List, Optional

import numpy as np

from src.events import Event, Reward
from src.events.base.base_events import BaseEvents
from src.info import State


class CoinPotentialAuxiliaryReward(Event):

    def __init__(
        self,
        base_reward: float = 1,
        name: Optional[str] = None,
        round_multiplier_func: Callable[[float], float] = lambda _: 1,
        reduction_per_round: float = 100,
        max_abs_reward: float = 10,
        extend: int = 10,

    ) -> None:
        super().__init__(base_reward, name)
        self.round_multiplier_func = round_multiplier_func
        self.reduction_per_round = reduction_per_round
        self.max_abs_reward = max_abs_reward
        self.last_action = ""
        self.active_rounds = 0
        self.coin_dict = {}
        self.current_round = 1
        self.extend = extend

    def calculate_reward(
            self,
            old_game_state: Dict[str, Any],
            new_game_state: Dict[str, Any],
            _self_action: str,
            events: List[str],
    ) -> Reward:
        if old_game_state is None or new_game_state is None:
            return None

        def get_coin_potentials(coins: set, extend: int) -> dict:
            potentials = dict.fromkeys(coins)
            for coin in coins:
                pot = np.zeros((17, 17), float)
                for i in range(extend, -extend - 1, -1):
                    for j in range(extend, -extend - 1, -1):
                        if i == 0 and j == 0:
                            continue
                        if coin[0] + i >= 17 or coin[0] + i < 0:
                            continue
                        if coin[1] + j >= 17 or coin[1] + j < 0:
                            continue
                        pot[coin[0] + i, coin[1] + j] = 1.0 / (abs(i) + abs(j))
                potentials[coin] = pot
            return potentials

        if BaseEvents.COIN_COLLECTED in events:
            return None
        coins = State.get_coins(old_game_state)
        missing_keys = set(coins)-(self.coin_dict.keys())
        self.coin_dict.update(get_coin_potentials(missing_keys, self.extend))
        potential = np.sum([self.coin_dict[c] for c in coins], 0)
        reward = (potential[State.get_self_values(new_game_state).position] - potential[State.get_self_values(old_game_state).position])
        if reward > 0:
            reward /= 3
        return reward
