from types import SimpleNamespace
from typing import Any, Dict, List, NamedTuple, Optional, Tuple

import numpy as np
import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info import State
from src.utility import utility


class CoinInfo(NamedTuple):
    """Holds some information about a coin on the playing field."""
    position: Tuple[int, int]
    distance_to_agent: int
    is_collected: bool


class CoinInfoFeatureExtractor(FeatureExtractor):
    """Feature extractor that provides some information about the coins present in a round."""

    N_VALUES_PER_COIN = 4
    N_ADDITIONAL_VALUES = 2
    NOT_COLLECTED = 0.0
    COLLECTED = -1.0

    def __init__(self, n_coins: int, cache_features: bool = True) -> None:
        """Initialize a new collector instance.

        Parameters
        ----------
        n_coins : int
            The number of coins to assume.

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.n_coins = n_coins
        self.n_remaining = self.n_coins
        self.initial_coin_positions: List[Tuple[int, int]] = []

    def n_features(self) -> Tuple[int, ...]:
        return (1, self.n_coins * self.N_VALUES_PER_COIN + self.N_ADDITIONAL_VALUES)

    def coin_position_to_index(self, position: Tuple[int, int]) -> Optional[int]:
        """Convert the given position to a coin index.

        Parameters
        ----------
        position : Tuple[int, int]
            The position to convert.

        Returns
        ----------
        index : Optional[int]
            The index if there is or was a coin at the given position. Else `None`.
        """
        for index, coin_position in enumerate(self.initial_coin_positions):
            if coin_position != position:
                continue
            return index
        return None

    def coin_index_to_position(self, index: int) -> Optional[Tuple[int, int]]:
        """Convert the given coin index to the corresponding position.

        Parameters
        ----------
        index : int
            The index to convert.

        Returns
        ----------
        position : Optional[Tuple[int, int]]
            The coin position if there is or was a coin with the given index. Else `None`.
        """
        if index >= len(self.initial_coin_positions):
            return None
        return self.initial_coin_positions[index]

    def features_to_coin_info_by_index(
        self,
        coin_index: int,
        feature_tensor: Tensor,
    ) -> Optional[CoinInfo]:
        """Get information about a specific coin from a previously extracted feature tensor.

        Parameters
        ----------
        coin_index : int
            The index of the coin to extract info for.

        feature_tensor : Tensor
            The previously extracted feature tensor.

        Returns
        ----------
        coin_info : CoinInfo
            The extracted coin information.
        """
        if coin_index >= len(self.initial_coin_positions):
            return None
        assert feature_tensor.shape == (1, self.n_features()), "Tensor has wrong shape!"
        data = feature_tensor[0]
        start_index = coin_index * self.N_VALUES_PER_COIN + self.N_ADDITIONAL_VALUES
        coin_position_x = int(data[start_index])
        coin_position_y = int(data[start_index + 1])
        distance_to_agent = int(data[start_index + 2])
        is_collected = data[start_index + 3].item() == self.COLLECTED
        return CoinInfo((coin_position_x, coin_position_y), distance_to_agent, is_collected)

    def features_to_coin_info_by_position(
        self,
        coin_position: Tuple[int, int],
        feature_tensor: Tensor,
    ) -> Optional[CoinInfo]:
        """Get information about a specific coin from a previously extracted feature tensor.

        Parameters
        ----------
        coin_position : Tuple[int, int]
            The position of the coin to extract info for.

        feature_tensor : Tensor
            The previously extracted feature tensor.

        Returns
        ----------
        coin_info : CoinInfo
            The extracted coin information.
        """
        coin_index = self.coin_position_to_index(coin_position)
        if coin_index is None:
            return None
        return self.features_to_coin_info_by_index(coin_index, feature_tensor)

    def features_to_agent_position(
        self,
        feature_tensor: Tensor,
    ) -> Tuple[int, int]:
        """Get the agent position from a previously extracted feature tensor.

        Parameters
        ----------
        feature_tensor : Tensor
            The previously extracted feature tensor.

        Returns
        ----------
        agent_position : Tuple[int, int]
            The extracted agent position.
        """
        assert feature_tensor.shape == (1, self.n_features()), "Tensor has wrong shape!"
        data = feature_tensor[0]
        agent_position_x = int(data[0])
        agent_position_y = int(data[1])
        return agent_position_x, agent_position_y

    def _on_first_step_in_round(self, game_state: Dict[str, Any]) -> None:
        """Assert the number of coins did not change and update initial coin positions.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state at the start of the new round.
        """
        coin_positions = State.get_coins(game_state)
        wrong_coin_count_msg = f"Expected {self.n_coins} coins but found {len(self.initial_coin_positions)}!"
        assert len(coin_positions) == self.n_coins, wrong_coin_count_msg
        self.initial_coin_positions = coin_positions
        self.n_remaining = self.n_coins

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract information about all coins.

        The resulting vector contains the following information for each coin:

        `[position_x, position_y, distance_to_agent, is_collected]`

        Additionally, the first two entries are reserved for the agent position.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace object.

        game_state : Dict[str, Any]
            The current game state.

        Returns
        ----------
        features : Tensor
            The extracted features containing the information listed above.
        """
        # Check if a new round has started and update accordingly.
        current_step = State.get_step(game_state)
        if current_step == 1:
            self._on_first_step_in_round(game_state)

        # Compute shortest paths to every coin.
        agent_position = State.get_self_values(game_state).position
        tile_map = State.get_field(game_state)
        shortest_paths = utility.shortest_path_bfs(
            agent_position,
            self.initial_coin_positions,
            tile_map,
        )

        # Build coin info tensor.
        current_coin_positions = State.get_coins(game_state)
        current_coin_positions = set(current_coin_positions)
        coin_info = np.zeros(shape=self.n_features(), dtype=np.float32)

        # Add agent position to features first.
        coin_info[0], coin_info[1] = agent_position

        # Add additional coin information.
        for coin_index, coin_position in enumerate(self.initial_coin_positions):
            coin_position_x, coin_position_y = coin_position
            distance_to_agent = len(shortest_paths[coin_position]) - 1
            first_index = self.N_ADDITIONAL_VALUES + coin_index * self.N_VALUES_PER_COIN
            is_collected = coin_position not in current_coin_positions
            coin_info[first_index] = coin_position_x
            coin_info[first_index + 1] = coin_position_y
            coin_info[first_index + 2] = distance_to_agent
            coin_info[first_index + 3] = self.COLLECTED if is_collected else self.NOT_COLLECTED

        return torch.tensor(coin_info, dtype=torch.float32).unsqueeze(0)
