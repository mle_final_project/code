from types import SimpleNamespace
from typing import Any, Dict, Tuple

import numpy as np
import torch
from torch import Tensor

from src.features import FeatureExtractor
from src.info.state import State
from src.utility import utility


class CoinMinimapFeatureExtractor(FeatureExtractor):
    def __init__(self, using_4split: bool, accuracy_levels: float) -> None:
        self.using_4split = using_4split
        self.accuracy_levels = accuracy_levels
        self.coin_dict = {}
        self.current_round = 1
        super().__init__()

    def n_features(self) -> Tuple[int, ...]:
        return (1, 4)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        if self.current_round != State.get_round(game_state):
            self.coin_dict = {}
            self.current_round = State.get_round(game_state)
        minimap = np.zeros((4,))  # q0, q1, q2, q3
        # print("initial minimap: ", minimap)
        coins = State.get_coins(game_state)
        missing_keys = set(coins)-(self.coin_dict.keys())
        for key in missing_keys:
            self.coin_dict[key] = utility.get_current_quadrant(
                position=key,
                transposed=True)
        for coin in coins:
            minimap[self.coin_dict[coin]] += 1.0
        minimap = np.round(
            self.accuracy_levels * (minimap / float(len(coins))),
            0) / self.accuracy_levels
        # print("Original Minimap: \n|", minimap[0], "|", minimap[1], "|\n|", minimap[3], "|", minimap[2], "|")
        if self.using_4split:
            agent_position = State.get_self_values(game_state).position
            current_quadrant = utility.get_current_quadrant(
                position=agent_position,
                transposed=True)
            minimap = np.roll(minimap, -current_quadrant)
        # print("Rotated Minimap: \n|", minimap[0], "|", minimap[1], "|\n|", minimap[3], "|", minimap[2], "|")
        return torch.tensor(minimap, dtype=torch.float32).unsqueeze(0)
