from types import SimpleNamespace
from typing import Any, Dict, Tuple

import numpy as np
import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info import State
from src.utility import utility


class SurroundingFeatureExtractor(FeatureExtractor):
    """A feature extractor that extracts the surroundings of the agent's position."""

    def __init__(
        self,
        radius: int = 1,
        include_danger: bool = True,
        normalize: bool = True,
        include_diagonals: bool = False,
        cache_features: bool = True,
    ) -> None:
        """Initialize extractor.

        Parameters
        ----------
        radius : int
            The radius of the surrounding to extract.

        include_danger : bool
            If `True`, also include bombs and explosions. Else, only wall, crate, coin, and agent
            data is added.

        normalize : bool
            If `True`, normalize the different tile values.

        include_diagonals : bool
            If `False`, only include up/down/left/right/center positions.

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.radius: int = radius
        self.include_danger = include_danger
        self.normalize = normalize
        self.include_diagonals = include_diagonals

    def n_features(self) -> Tuple[int, ...]:
        if self.include_diagonals:
            size = 2 * self.radius + 1
            return (1, size * size)
        return (1, 4 * self.radius + 1)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract the agent's surrounding tiles as features.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state.

        self_namespace : SimpleNamespace
            The game's namespace information.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        tile_map = State.get_field(game_state)
        coin_positions = State.get_coins_numpy(game_state)
        agent_position = State.get_self_values(game_state).position
        if self.include_danger:
            bomb_info = State.get_bombs_numpy(game_state)
            bomb_positions = bomb_info[:, :2] if len(bomb_info) > 0 else bomb_info
            explosion_positions = np.array(np.nonzero(State.get_explosion_map(game_state)))
        else:
            bomb_positions = np.array([])
            explosion_positions = np.array([])
        other_agent_positions = [values.position for values in State.get_other_values(game_state)]
        combined_world_data = utility.combine_world_data(
            tile_map=tile_map,
            coin_positions=coin_positions,
            bomb_positions=bomb_positions,
            explosion_positions=explosion_positions,
            self_agent_position=np.array(agent_position, dtype=np.int16),
            other_agent_positions=np.array(other_agent_positions, dtype=np.int16),
            normalize=self.normalize,
        )
        surrounding = utility.get_surrounding_tiles(
            tile_map=combined_world_data,
            position=agent_position,
            radius=self.radius,
            include_diagonals=self.include_diagonals,
        )
        return torch.tensor(surrounding, dtype=torch.float32).unsqueeze(0)
