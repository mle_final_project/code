from types import SimpleNamespace
from typing import Any, Dict, Tuple

import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info.state import State
from src.info.world import Settings, Tiles
from src.utility import utility


class AgentsInBombRangeFeatureExtractor(FeatureExtractor):
    "Extract the number of enemy agents in range of a bomb as feature."

    def __init__(
        self,
        normalize: bool = True,
        cache_features: bool = True,
    ) -> None:
        """Initialize a new feature extractor instance.

        Parameters
        ----------
        normalize : bool
            If `True`, normalize values to [-1, 1].

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.normalize = normalize

    def n_features(self) -> Tuple[int, ...]:
        """Get the number of features this extractor produces.

        Returns
        ----------
        n_features : Tuple[int, ...]
            The dimensions of the extracted features.
        """
        return (1, 1)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Get the number of enemy agents in range of a bomb as feature.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace information.

        game_state : Dict[str, Any]
            The game state.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        agent_position = State.get_self_values(game_state).position
        agent_x, agent_y = agent_position
        tile_map = State.get_field(game_state)
        width, height = tile_map.shape
        left_done = False
        right_done = False
        up_done = False
        down_done = False
        n_agents_in_range = 0
        for offset in range(1, Settings.get_bomb_power() + 1):
            # Check left.
            x_left = agent_x - offset
            if x_left < 0 or tile_map[x_left, agent_y] == Tiles.WALL:
                left_done = True
            if not left_done and tile_map[x_left, agent_y] == Tiles.OTHER_AGENT:
                n_agents_in_range += 1
            # Check right.
            x_right = agent_x + offset
            if x_right >= width or tile_map[x_right, agent_y] == Tiles.WALL:
                right_done = True
            if not right_done and tile_map[x_right, agent_y] == Tiles.OTHER_AGENT:
                n_agents_in_range += 1
            # Check up.
            y_up = agent_y + offset
            if y_up >= height or tile_map[agent_x, y_up] == Tiles.WALL:
                up_done = True
            if not up_done and tile_map[agent_x, y_up] == Tiles.OTHER_AGENT:
                n_agents_in_range += 1
            # Check down.
            y_down = agent_y - offset
            if y_down < 0 or tile_map[agent_x, y_down] == Tiles.WALL:
                down_done = True
            if not down_done and tile_map[agent_x, y_down] == Tiles.OTHER_AGENT:
                n_agents_in_range += 1
        if self.normalize:
            n_agents_in_range = utility.remap_value_to_range(n_agents_in_range, 0, 12, -1, 1)
        return torch.tensor([n_agents_in_range], dtype=torch.float32).unsqueeze(0)
