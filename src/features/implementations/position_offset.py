from types import SimpleNamespace
from typing import Any, Dict, Tuple

import numpy as np
import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info import World
from src.info.state import State


class PositionOffsetFeatureExtractor(FeatureExtractor):
    """Provides the position offsets to the agent as features."""

    VALUES_PER_TILE = 2

    def __init__(self, cache_features: bool = True) -> None:
        super().__init__(cache_features)
        width, height = World.get_world_size()
        self.tile_positions = np.array([[x, y] for x in range(width) for y in range(height)])
        self.n_tiles = width * height

    def n_features(self) -> Tuple[int, ...]:
        """Get the number of features this extractor produces. Equal to the number of world tiles
        times two.

        Returns
        ----------
        n_features : int
            The number of features.
        """
        return (1, self.VALUES_PER_TILE * self.n_tiles)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract the position offset relative to the agent for all world tiles.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace information.

        game_state : Dict[str, Any]
            The game state.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        agent_position = np.array(State.get_self_values(game_state).position)
        offsets = self.tile_positions - agent_position
        return torch.tensor(offsets.flatten(), dtype=torch.int32).unsqueeze(0)
