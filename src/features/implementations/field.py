from types import SimpleNamespace
from typing import Any, Dict, Tuple

import numpy as np
import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info import State, World
from src.utility import utility


class FieldFeatureExtractor(FeatureExtractor):
    """A feature extractor that extracts the whole playing field as feature."""

    def __init__(
        self,
        include_coins: bool = True,
        include_self_position: bool = True,
        include_bomb_positions: bool = True,
        include_agent_positions: bool = True,
        normalize: bool = True,
        flatten: bool = True,
        cache_features: bool = True,
    ) -> None:
        """Initialize extractor.

        Parameters
        ----------
        include_coins : bool
            Should coin positions be included?

        include_self_position : bool
            Should the position of the agent be included?

        include_bomb_positions : bool
            Should the bomb positions be included?

        include_agent_position : bool
            Should the positions of other agents be included?

        normalize : bool
            If `True`, normalize the different tile values.

        flatten : bool
            If `True`, flatten the resulting feature tensor.

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.include_coins = include_coins
        self.include_self_position = include_self_position
        self.include_bomb_positions = include_bomb_positions
        self.include_agent_positions = include_agent_positions
        self.normalize = normalize
        self.flatten = flatten
        self.world_dims: Tuple[int, int] = World.get_world_size()

    def n_features(self) -> Tuple[int, ...]:
        return (1, int(np.prod(self.world_dims))) if self.flatten else self.world_dims

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract all world tiles as features.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state.

        self_namespace : SimpleNamespace
            The game's namespace information.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        ignore = np.array([], dtype=np.int16)
        tile_map = State.get_field(game_state)
        coin_positions = State.get_coins_numpy(game_state)
        agent_position = np.array(State.get_self_values(game_state).position, dtype=np.int16)
        combined_world_data = utility.combine_world_data(
            tile_map=tile_map,
            coin_positions=coin_positions if self.include_coins else ignore,
            bomb_positions=np.array([], dtype=np.int16) if self.include_bomb_positions else ignore,
            explosion_positions=np.array([], dtype=np.int16) if self.include_bomb_positions else ignore,
            self_agent_position=agent_position if self.include_self_position else ignore,
            other_agent_positions=np.array([], dtype=np.int16) if self.include_agent_positions else ignore,
            normalize=self.normalize,
        )
        if self.flatten:
            return torch.tensor(combined_world_data.flatten(), dtype=torch.float32).unsqueeze(0)
        return torch.tensor(combined_world_data, dtype=torch.float32)
