from typing import Any, Dict, List

from src.events import Event, Reward
from src.info.state import State
from src.utility import utility


class MovedTowardsNearestCoin(Event):
    def __init__(self) -> None:
        super().__init__(0)
        self.position = (0, 0)
        self.last_distance = 0

    def did_take_place(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Dict[str, Any],
        _self_action: str,
    ) -> bool:

        position = State.get_self_values(new_game_state).position

        if utility.closest_coin_direction(
                State.get_field(old_game_state),
                State.get_coins(old_game_state),
                position)[2] < self.last_distance:
            self.position = position
            self.last_distance = utility.closest_coin_direction(State.get_field(
                old_game_state), State.get_coins(old_game_state), position)[2]
            return True

        self.position = position
        self.last_distance = utility.closest_coin_direction(State.get_field(
            old_game_state), State.get_coins(old_game_state), position)[2]
        return False

    def calculate_reward(
        self,
        _old_game_state: Dict[str, Any],
        _new_game_state: Dict[str, Any],
        _self_action: str,
        _events: List[str],
    ) -> Reward:
        return 0  # (1/self.last_distance) * 2


class MovedRelativeToNearestCoin(Event):

    def __init__(self) -> None:
        super().__init__(0)
        self.position = (0, 0)
        self.last_distance = 0
        self.has_neared = False

    def did_take_place(
        self,
        old_game_state: Dict[str, Any],
        new_game_state: Dict[str, Any],
        _self_action: str,
    ) -> bool:

        position = State.get_self_values(new_game_state).position

        if utility.closest_coin_direction(
                State.get_field(old_game_state),
                State.get_coins(old_game_state),
                position)[2] < self.last_distance:
            self.position = position
            self.last_distance = utility.closest_coin_direction(State.get_field(
                old_game_state), State.get_coins(old_game_state), position)[2]
            self.has_neared = True
        else:
            self.position = position
            self.last_distance = utility.closest_coin_direction(State.get_field(
                old_game_state), State.get_coins(old_game_state), position)[2]
            self.has_neared = False

        return True

    def calculate_reward(
        self,
        _old_game_state: Dict[str, Any],
        _new_game_state: Dict[str, Any],
        _self_action: str,
        _events: List[str],
    ) -> Reward:
        if self.has_neared:
            return 0  # (1/self.last_distance) * 2
        return 0  # -(1/self.last_distance) * 2
