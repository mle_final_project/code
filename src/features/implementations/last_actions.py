from types import SimpleNamespace
from typing import Any, Dict, Tuple

import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info import Actions


class LastActionsFeatureExtractor(FeatureExtractor):
    """A feature extractor that extracts the agent's last `n` actions as features."""

    def __init__(self, n: int = 1, normalize: bool = True, cache_features: bool = True) -> None:
        """Initialize extractor.

        Parameters
        ----------
        n : int
            How many of the most recent actions should be extracted as features.

        normalize : bool
            If `True`, normalize action space to [-1, 1].

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.n = n
        self.normalize = normalize

    def n_features(self) -> Tuple[int, ...]:
        return (1, self.n)

    def _extract_features_implementation(
        self,
        self_namespace: SimpleNamespace,
        _game_state: Dict[str, Any],
    ) -> Tensor:
        last_actions = self_namespace.last_actions
        n_queue_elements = len(last_actions)
        first = max(n_queue_elements - self.n, 0)
        last = min(first + self.n, n_queue_elements)
        last_n_elements = [last_actions[i] for i in range(first, last)]
        n_missing = self.n - len(last_n_elements)
        padding = [Actions.INVALID_ID for _ in range(n_missing)]
        features = padding + last_n_elements  # Ensure order new elements enter stays consistent!
        if self.normalize:
            features = [Actions.get_normalized_action_value_from_id(value) for value in features]
        return torch.tensor(features, dtype=torch.float32).unsqueeze(0)
