from types import SimpleNamespace
from typing import Any, Dict, Tuple

import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info.state import State


class AgentBombStateFeatureExtractor(FeatureExtractor):
    """A feature extractor that simply includes if the agent's bomb is ready to be dropped."""

    def n_features(self) -> Tuple[int, ...]:
        return (1, 1)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        is_bomb_available = State.get_self_values(game_state).is_bomb_available
        return torch.tensor([int(is_bomb_available)], dtype=torch.float32).unsqueeze(0)
