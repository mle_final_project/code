from types import SimpleNamespace
from typing import Any, Dict, Tuple

import numpy as np
import numpy.typing as npt
import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor
from src.info.state import State
from src.info.world import Settings, Tiles
from src.utility import utility


class DangerInfoFeatureExtractor(FeatureExtractor):
    """Extract information about the imminent danger around the agent.
    """

    NEW_EXPLOSION_VALUE = -1
    OLD_EXPLOSION_VALUE = -2
    NO_DANGER_VALUE = -3

    def __init__(
        self,
        radius: int = 1,
        include_diagonals: bool = False,
        normalize: bool = True,
        cache_features: bool = True,
    ) -> None:
        """Initialize a new feature extractor instance.

        Parameters
        ----------
        radius : int
            The radius around the agent to include in the resulting feature array.

        include_diagonals : bool
            Should diagonals be included?

        normalize : bool
            Should the values be normalized to [-1, 1]?

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.radius = radius
        self.include_diagonals = include_diagonals
        self.normalize = normalize
        self.previous_round = -1
        self.previous_step = -1
        self.previous_explosions = None
        self.bomb_power = Settings.get_bomb_power()
        self.bomb_timer = Settings.get_bomb_timer()
        self.explosions_timer = Settings.get_explosion_timer()

    def n_features(self) -> Tuple[int, ...]:
        """Get the number of features this extractor produces.

        Returns
        ----------
        n_features : Tuple[int, int]
            The feature dimensions.
        """
        if self.include_diagonals:
            size = 2 * self.radius + 1
            return (1, size * size)
        return (1, 4 * self.radius + 1)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract information about the imminent danger around the agent.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace information.

        game_state : Dict[str, Any]
            The game state.

        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        # Check if round changed and reset data.
        current_round = State.get_round(game_state)
        if current_round != self.previous_round:
            self.previous_round = current_round
            self.previous_step = 0
            self.previous_explosions = None

        # Get required state information.
        agent_position = State.get_self_values(game_state).position
        bomb_info = State.get_bombs_numpy(game_state)
        explosion_map = State.get_explosion_map(game_state)
        tile_map = State.get_field(game_state)

        # Create danger map.
        danger_map = self.NEW_EXPLOSION_VALUE * explosion_map.astype(np.float32)
        danger_map[danger_map == 0] = self.NO_DANGER_VALUE

        # Since the game only tells us the remaining deadly steps, we need to track which tiles
        # where previously marked as dangerous for another turn.
        if self.previous_explosions is not None:
            old_explosion_mask = self.previous_explosions == 1
            danger_map[old_explosion_mask] = self.OLD_EXPLOSION_VALUE

        # Spread bomb countdowns to all tiles which will be affected by the following explosions. We
        # use the minimum between timer and present values (if one was already set) to make sure that
        # (1) the bomb closest to exploding and (2) current explosions are always shown to the agent.
        for x, y, timer in bomb_info:
            left_done, right_done, up_done, down_done = False, False, False, False
            for offset in range(1, self.bomb_power + 1):
                if not left_done:
                    left_done = self._update_bomb_countdown_left(
                        x=x,
                        y=y,
                        offset=offset,
                        timer=timer,
                        tile_map=tile_map,
                        danger_map=danger_map,
                    )
                if not right_done:
                    right_done = self._update_bomb_countdown_right(
                        x=x,
                        y=y,
                        offset=offset,
                        timer=timer,
                        tile_map=tile_map,
                        danger_map=danger_map,
                    )
                if not up_done:
                    up_done = self._update_bomb_countdown_up(
                        x=x,
                        y=y,
                        offset=offset,
                        timer=timer,
                        tile_map=tile_map,
                        danger_map=danger_map,
                    )
                if not down_done:
                    down_done = self._update_bomb_countdown_down(
                        x=x,
                        y=y,
                        offset=offset,
                        timer=timer,
                        tile_map=tile_map,
                        danger_map=danger_map,
                    )
                self._update_bomb_countdown_center(x, y, timer, danger_map)

        # Check if step changed and update previous data.
        current_step = State.get_step(game_state)
        if current_step == self.previous_step + 1:
            self.previous_step = current_step
            self.previous_explosions = explosion_map

        # Limit to desired range.
        danger_map = utility.get_surrounding_tiles(
            danger_map,
            agent_position,
            self.radius,
            self.include_diagonals,
        ).flatten()

        # Normalize if desired.
        if self.normalize:
            danger_map = utility.remap_values_to_range(
                values=danger_map,
                map_to_min_value=-1,
                map_to_max_value=1,
                min_value=self.NO_DANGER_VALUE,
                max_value=self.bomb_timer,
            )

        return torch.tensor(danger_map, dtype=torch.float32).unsqueeze(0)

    def _update_bomb_countdown_left(
        self,
        x: int,
        y: int,
        offset: int,
        timer: int,
        tile_map: npt.NDArray[np.int16],
        danger_map: npt.NDArray[np.float32],
    ) -> bool:
        x_left = x - offset
        if x_left < 0:
            return True
        if tile_map[x_left, y] == Tiles.WALL:
            return True
        value_left = danger_map[x_left, y]
        if value_left == self.NO_DANGER_VALUE:
            danger_map[x_left, y] = timer
        else:
            danger_map[x_left, y] = min(timer, value_left)
        return False

    def _update_bomb_countdown_right(
        self,
        x: int,
        y: int,
        offset: int,
        timer: int,
        tile_map: npt.NDArray[np.int16],
        danger_map: npt.NDArray[np.float32],
    ) -> bool:
        width, _ = tile_map.shape
        x_right = x + offset
        if x_right >= width:
            return True
        if tile_map[x_right, y] == Tiles.WALL:
            return True
        value_right = danger_map[x_right, y]
        if value_right == self.NO_DANGER_VALUE:
            danger_map[x_right, y] = timer
        else:
            danger_map[x_right, y] = min(timer, value_right)
        return False

    def _update_bomb_countdown_down(
        self,
        x: int,
        y: int,
        offset: int,
        timer: int,
        tile_map: npt.NDArray[np.int16],
        danger_map: npt.NDArray[np.float32],
    ) -> bool:
        y_down = y - offset
        if y_down < 0:
            return True
        if tile_map[x, y_down] == Tiles.WALL:
            return True
        value_down = danger_map[x, y_down]
        if value_down == self.NO_DANGER_VALUE:
            danger_map[x, y_down] = timer
        else:
            danger_map[x, y_down] = min(timer, value_down)
        return False

    def _update_bomb_countdown_up(
        self,
        x: int,
        y: int,
        offset: int,
        timer: int,
        tile_map: npt.NDArray[np.int16],
        danger_map: npt.NDArray[np.float32],
    ) -> bool:
        _, height = tile_map.shape
        y_up = y + offset
        if y_up >= height:
            return True
        if tile_map[x, y_up] == Tiles.WALL:
            return True
        value_up = danger_map[x, y_up]
        if value_up == self.NO_DANGER_VALUE:
            danger_map[x, y_up] = timer
        else:
            danger_map[x, y_up] = min(timer, value_up)
        return False

    def _update_bomb_countdown_center(
        self,
        x: int,
        y: int,
        timer: int,
        danger_map: npt.NDArray[np.float32],
    ) -> None:
        value_center = danger_map[x, y]
        if value_center == self.NO_DANGER_VALUE:
            danger_map[x, y] = timer
        else:
            danger_map[x, y] = min(timer, value_center)
