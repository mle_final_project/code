from types import SimpleNamespace
from typing import Any, Dict, List, Tuple

import numpy as np
import torch
from torch import Tensor

from src.features import FeatureExtractor
from src.info.state import State
from src.utility import utility


class FourSplitFeatureExtractor(FeatureExtractor):
    """Feature Extractor that splits the board into 4 identical sections
    - Requires MovementAllowedFeatureExtractor (with using_4split = True) (Walls are ignored)
    - Requires rotation of the selected action later on (Done in agent.py)
    - Might require MinimapFeatureExtractor (with using_4split = True) (Tells where coins are left)
    """

    def __init__(self) -> None:
        super().__init__()

    def n_features(self) -> Tuple[int, ...]:
        return (1, 8 * 9)  # - (3 * 4)

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        tile_map = np.array(State.get_field(game_state))
        tile_map[*zip(*State.get_coins(game_state))] = 1
        tile_map = tile_map.T  # to feed in correct features according to ui (and not the transpose)
        agent_position_ui = (State.get_self_values(game_state).position[1],
                             State.get_self_values(game_state).position[0])
        tile_map[*agent_position_ui] = -2
        current_quadrant = utility.get_current_quadrant(
            position=agent_position_ui,
            transposed=False)
        quadrant_tile_map = tile_map

        for _ in range(current_quadrant):
            # rotate game board 90° counter clockwise until you reach first quadrant (upper left corner)
            quadrant_tile_map = np.rot90(quadrant_tile_map)
        quadrant_tile_map = quadrant_tile_map[:9, :8]

        def remove_outer_walls(tiles: np.ndarray) -> np.ndarray:
            tiles = tiles[1:]  # remove upper wall
            return np.delete(tiles, 0, 1)  # remove left wall

        def remove_inner_walls_flatten(tiles: np.ndarray) -> List[float]:
            return [t for t in tiles.flatten() if t != -1]

        return torch.from_numpy(quadrant_tile_map.flatten()).clone().detach().unsqueeze(0)
