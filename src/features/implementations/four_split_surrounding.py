from types import SimpleNamespace
from typing import Any, Dict

import numpy as np
import torch
from torch import Tensor

from src.features.implementations.surrounding import SurroundingFeatureExtractor
from src.info.state import State
from src.utility import utility


class FourSplitSurroundingFeatureExtractor(SurroundingFeatureExtractor):
    """A feature extractor that extracts the surroundings of the
    agent's position and rotates it according to the current quadrant"""

    def __init__(
        self,
        radius: int = 1,
        normalize: bool = True,
        include_diagonals: bool = False,
        cache_features: bool = True,
        using_4split: bool = True,
    ) -> None:
        """Initialize extractor.

        Parameters
        ----------
        radius : int
            The radius of the surrounding to extract.

        include_diagonals : bool
            If `False`, only include up/down/left/right/center positions.

        cache_features : bool
            Should features for the last two steps in the current round be cached?

        normalize : bool
            If `True`, normalize the different tile values.

        using_4split : bool
            Should the surroundings be rotated according to the current quadrant?
        """
        super().__init__(
            radius=radius,
            include_diagonals=include_diagonals,
            cache_features=cache_features,
            normalize=normalize,
        )
        self.using_4split = using_4split

    def _extract_features_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract the agent's surrounding tiles as features.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state.

        _self_namespace : SimpleNamespace
            The game's namespace information.


        Returns
        ----------
        features : 2D Tensor
            The extracted features.
        """
        tile_map = State.get_field(game_state)
        coin_positions = State.get_coins_numpy(game_state)
        agent_position = State.get_self_values(game_state).position
        combined_world_data = utility.combine_world_data(
            tile_map=tile_map,
            coin_positions=coin_positions,
            bomb_positions=np.array([], dtype=np.int16),
            explosion_positions=np.array([], dtype=np.int16),
            self_agent_position=np.array(agent_position, dtype=np.int16),
            other_agent_positions=np.array([], dtype=np.int16),
            normalize=self.normalize,
        )
        surrounding = utility.get_surrounding_tiles(
            tile_map=combined_world_data,
            position=agent_position,
            radius=self.radius,
            include_diagonals=self.include_diagonals,
        )
        # Make surrounding 2D for convolutional network
        surrounding = surrounding.reshape((2*self.radius + 1, 2*self.radius + 1)).T
        # print("surroundings: \n", surrounding)
        if self.using_4split:
            agent_position = State.get_self_values(game_state).position
            current_quadrant = utility.get_current_quadrant(
                position=agent_position,
                transposed=True)
            for _ in range(current_quadrant):
                # surroundings 90° counterclockwise until you reach first quadrant (upper left corner)
                surrounding = np.rot90(surrounding)
            # print("rotated surroundings: \n", surrounding, "\n")
        return torch.from_numpy(np.float32(surrounding).flatten()).clone().detach().unsqueeze(0)
