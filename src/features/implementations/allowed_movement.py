from types import SimpleNamespace
from typing import Any, Dict

import torch
from torch import Tensor

from src.features import FeatureExtractor
from src.info.state import State
from src.utility import utility


class MovementAllowedFeatureExtractor(FeatureExtractor):
    """ Feature Extractor that tells the agent if movement up/right/down/left is allowed
    - walls can be ignored in rem. features
    - Can consider 4split"""

    def __init__(self, using_4split: bool) -> None:
        self.using_4split = using_4split
        super().__init__()

    def n_features(self) -> int:
        return 4

    def _extract_features_implementation(
            self,
            _self_namespace: SimpleNamespace,
            game_state: Dict[str, Any]) -> Tensor:
        agent_position = State.get_self_values(game_state).position
        allowed_movement = utility.get_allowed_movement(position=agent_position, transposed=True)
        _up: int = 0
        _right: int = 1
        _down: int = 2
        _left: int = 3
        # print("position: ", agent_position[1], agent_position[0])
        # print("allowed (UI):\n up: ",
        #      allowed_movement[up], "  right: ",
        #      allowed_movement[right], "  down: ",
        #      allowed_movement[down], "  left: ",
        #      allowed_movement[left])
        if self.using_4split:
            current_quadrant = utility.get_current_quadrant(
                position=tuple(agent_position),
                transposed=True)
            # print("quadrant: ", current_quadrant)
            for _ in range(current_quadrant):
                # rotate allowed movement to let them point in the correct direction (
                allowed_movement.append(allowed_movement.pop(0))
        # print("allowed (feat):\n up: ",
        #      allowed_movement[up], "  right: ",
        #      allowed_movement[right], "  down: ",
        #      allowed_movement[down], "  left: ",
        #      allowed_movement[left])
        return torch.tensor(allowed_movement, dtype=torch.float32).unsqueeze(0)
