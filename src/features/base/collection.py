from types import SimpleNamespace
from typing import Any, Dict, List, Tuple

import numpy as np
import torch
from torch import Tensor

from src.features.base.feature_extractor import FeatureExtractor


class FeatureExtractorCollection(FeatureExtractor):
    """A 'meta' feature extractor that combines the results of multiple extractors."""

    def __init__(
        self,
        extractors: List[FeatureExtractor],
        try_to_preserve_dims: bool = False,
        cache_features: bool = True,
        enable_debug_logging: bool = False,
    ) -> None:
        """Initialize a new feature extractor instance.

        Parameters
        ----------
        extractors : List[FeatureExtractors]
            The extractor instances to use.

        try_to_preserve_dims : bool
            If `True`, try to preserve the inner feature dimensions by concatenating along the first
            tensor axis. Else, flatten sub-features.

        cache_features : bool
            Should features for the last two steps in the current round be cached?
        """
        super().__init__(cache_features)
        self.extractors = extractors
        self.do_concatenate = try_to_preserve_dims and self._are_dims_compatible()
        if not self.do_concatenate:
            if enable_debug_logging:
                print(
                    "Features can not be concatenated or concatenation was disabled by the user. "
                    "Will use flattened feature tensors instead.",
                )
            self.feature_dims = (1, int(np.sum([np.prod(e.n_features()) for e in self.extractors])))
        else:
            first_dim_size = int(np.sum([e.n_features()[0] for e in self.extractors]))
            sample_feature_dims = self.extractors[0].n_features()
            if len(sample_feature_dims) > 1:
                self.feature_dims = (first_dim_size, *(d for d in sample_feature_dims[1:]))
            else:
                self.feature_dims = (1, first_dim_size)

        # Disable feature caching in children since this would just result in duplicate data.
        if self.do_cache_features:
            for extractor in self.extractors:
                extractor.do_cache_features = False

    def n_features(self) -> Tuple[int, ...]:
        """Get the total feature count for all registered extractors.

        Returns
        ----------
        n_features : Tuple[int, ...]
            The total feature count.
        """
        return self.feature_dims

    def _are_dims_compatible(self) -> bool:
        """Check if the dimensions of the sub-extractors are compatible, i.e., can be combined
        without flattening.

        Returns
        ----------
        are_compatible : bool
            `True` if the extracted sub-features can be concatenated, else `False`.
        """
        for index in range(1, len(self.extractors)):
            prev_dims = np.array(self.extractors[index - 1].n_features())
            curr_dims = np.array(self.extractors[index].n_features())
            if len(prev_dims) != len(curr_dims):
                return False
            if len(prev_dims) > 1 and prev_dims[1:] != curr_dims[1:]:
                return False
        return True

    def _extract_features_implementation(
        self,
        self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Tensor:
        """Extract and concatenate features using all registered extractors.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The game's namespace information.

        game_state : Dict[str, Any]
            The game state.

        Returns
        ----------
        features : Tensor
            The extracted features.
        """
        features = [
            e._extract_features_implementation(self_namespace, game_state)
            for e in self.extractors
        ]
        if self.do_concatenate:
            return torch.cat(features, dim=0)
        return torch.cat([f.flatten() for f in features]).unsqueeze(0)
