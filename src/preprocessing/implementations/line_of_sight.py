from types import SimpleNamespace
from typing import Any, Dict, List, Tuple

import numpy as np
import numpy.typing as npt

from src.info.state import AgentValues, State
from src.info.world import Tiles
from src.preprocessing.base.preprocessor import StatePreprocessor


class LineOfSightPreprocessor(StatePreprocessor):

    def __init__(
        self,
        view_distance: int,
        filter_tiles: bool = True,
        filter_coins: bool = True,
        filter_bombs: bool = True,
        filter_explosions: bool = True,
        filter_agent_values: bool = True,
        walls_block_view: bool = True,
        cache_states: bool = True,
    ) -> None:
        """Initialize preprocessor settings.

        Parameters
        ----------
        view_distance : int
            The number of tiles the agent can see in each direction.

        filter_tiles : bool
            If `True`, filter out invisible tile data.

        filter_tiles : bool
            If `True`, filter out invisible coin data.

        filter_bombs : bool
            If `True`, filter out invisible bomb data.

        filter_explosions : bool
            If `True`, filter out invisible explosion data.

        filter_agent_values : bool
            If `True`, filter out invisible agent data.

        walls_block_view : bool
            If `True`, walls will obstruct the agent's view, replacing tile information with walls.
            Only affects result if mode is set to STRAIGHT.

        cache_states : bool
            If `True`, cache the last two processed states to speed up multiple processing calls
            during the same step.
        """
        super().__init__(cache_states)
        self.view_distance = view_distance
        self.filter_tiles = filter_tiles
        self.filter_coins = filter_coins
        self.filter_bombs = filter_bombs
        self.filter_explosions = filter_explosions
        self.filter_agent_values = filter_agent_values
        self.walls_block_view = walls_block_view

    def _process_implementation(
        self,
        _self_namespace: SimpleNamespace,
        game_state: Dict[str, Any],
    ) -> Dict[str, Any]:
        """Remove all information that is not visible from the agent position.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        game_state : Dict[str, Any]
            The game state to process.

        Returns
        ----------
        processed_game_state : Dict[str, Any]
            The processed game state.
        """
        agent_position = np.array(State.get_self_values(game_state).position, dtype=np.int16)
        tile_map = State.get_field(game_state)
        obstruction_positions = self._get_obstruction_positions(tile_map, agent_position)
        if self.filter_tiles:
            self._filter_tile_map(tile_map, agent_position, obstruction_positions)
            game_state[State.FIELD] = tile_map
        if self.filter_coins:
            coins = State.get_coins(game_state)
            self._filter_coins(coins, agent_position, obstruction_positions)
            game_state[State.COINS] = coins
        if self.filter_bombs:
            bombs = State.get_bombs(game_state)
            self._filter_bombs(bombs, agent_position, obstruction_positions)
            game_state[State.BOMBS] = bombs
        if self.filter_explosions:
            explosion_map = State.get_explosion_map(game_state)
            self._filter_explosion_map(explosion_map, agent_position, obstruction_positions)
            game_state[State.EXPLOSION_MAP] = explosion_map
        if self.filter_agent_values:
            other_values = State.get_other_values(game_state)
            self._filter_other_values(other_values, agent_position, obstruction_positions)
            game_state[State.OTHERS] = other_values
        return game_state

    def _get_obstruction_positions(
        self,
        tile_map: npt.NDArray[np.int16],
        agent_position: npt.NDArray[np.int16],
    ) -> Tuple[int, int, int, int]:
        # Get positions and map dimensions.
        agent_x, agent_y = agent_position
        width, height = tile_map.shape

        if not self.walls_block_view:
            return 0, width, 0, height

        # Find obstructions on x axis.
        min_view_x = 0
        max_view_x = width - 1
        for x in range(width):
            if tile_map[x, agent_y] != Tiles.WALL:
                continue
            if x < agent_x:
                min_view_x = x if x > min_view_x else x
            if x > agent_x:
                max_view_x = x if x < max_view_x else x

        # Find obstructions on y axis.
        min_view_y = 0
        max_view_y = height - 1
        for y in range(height):
            if tile_map[agent_x, y] != Tiles.WALL:
                continue
            if y < agent_y:
                min_view_y = y if y > min_view_y else y
            if y > agent_y:
                max_view_y = y if y < max_view_y else y

        return min_view_x, max_view_x, min_view_y, max_view_y

    def _filter_tile_map(
        self,
        tile_map: npt.NDArray[np.int16],
        agent_position: npt.NDArray[np.int16],
        obstruction_positions: Tuple[int, int, int, int],
    ) -> None:
        agent_x, agent_y = agent_position
        width, height = tile_map.shape
        min_view_x, max_view_x, min_view_y, max_view_y = obstruction_positions
        for x in range(width):
            for y in range(height):
                is_invisible = x < min_view_x or x > max_view_x or y < min_view_y or y > max_view_y
                if (x == agent_x or y == agent_y) and not is_invisible:
                    continue
                tile_map[x, y] = Tiles.FREE

    def _filter_coins(
        self,
        coins: List[Tuple[int, int]],
        agent_position: npt.NDArray[np.int16],
        obstruction_positions: Tuple[int, int, int, int],
    ) -> None:
        agent_x, agent_y = agent_position
        min_view_x, max_view_x, min_view_y, max_view_y = obstruction_positions
        old_coins = coins.copy()
        coins.clear()
        for (x, y) in old_coins:
            is_invisible = x < min_view_x or x > max_view_x or y < min_view_y or y > max_view_y
            if x != agent_x and y != agent_y or is_invisible:
                continue
            coins.append((x, y))

    def _filter_bombs(
        self,
        bombs: List[Tuple[Tuple[int, int], int]],
        agent_position: npt.NDArray[np.int16],
        obstruction_positions: Tuple[int, int, int, int],
    ) -> None:
        agent_x, agent_y = agent_position
        min_view_x, max_view_x, min_view_y, max_view_y = obstruction_positions
        old_bombs = bombs.copy()
        bombs.clear()
        for (x, y), countdown in old_bombs:
            is_invisible = x < min_view_x or x > max_view_x or y < min_view_y or y > max_view_y
            if x != agent_x and y != agent_y or is_invisible:
                continue
            bombs.append(((x, y), countdown))

    def _filter_explosion_map(
        self,
        explosion_map: npt.NDArray[np.int16],
        agent_position: npt.NDArray[np.int16],
        obstruction_positions: Tuple[int, int, int, int],
    ) -> None:
        agent_x, agent_y = agent_position
        width, height = explosion_map.shape
        min_view_x, max_view_x, min_view_y, max_view_y = obstruction_positions
        for x in range(width):
            for y in range(height):
                is_invisible = x < min_view_x or x > max_view_x or y < min_view_y or y > max_view_y
                if (x == agent_x or y == agent_y) and not is_invisible:
                    continue
                explosion_map[x, y] = Tiles.FREE

    def _filter_other_values(
        self,
        other_values: List[AgentValues],
        agent_position: npt.NDArray[np.int16],
        obstruction_positions: Tuple[int, int, int, int],
    ) -> None:
        agent_x, agent_y = agent_position
        min_view_x, max_view_x, min_view_y, max_view_y = obstruction_positions
        old_values = other_values.copy()
        other_values.clear()
        for values in old_values:
            x, y = values.position
            is_invisible = x < min_view_x or x > max_view_x or y < min_view_y or y > max_view_y
            if x != agent_x and y != agent_y or is_invisible:
                continue
            other_values.append(values)
