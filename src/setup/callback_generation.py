"""This module defines the callbacks used by the game for letting the agent act."""

import os
import random
from collections import deque
from types import SimpleNamespace
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import torch

from src.agents import Agent
from src.events import Event, EventProcessor
from src.info import DataLogger, State
from src.preprocessing.base.preprocessor import StatePreprocessor

SetupFuncType = Callable[[SimpleNamespace], None]
"""The type of the `setup` callback."""

ActFuncType = Callable[[SimpleNamespace, Dict[str, Any]], str]
"""The type of the `act` callback."""


def _setup_namespace_and_agent(
    self: SimpleNamespace,
    agent: Agent,
    model_path: Optional[Union[str, os.PathLike]] = None,
    data_path: Optional[Union[str, os.PathLike]] = None,
    base_events: Optional[List[Tuple[str, Optional[float]]]] = None,
    custom_events: Optional[List[Event]] = None,
    preprocessors: Optional[List[StatePreprocessor]] = None,
    store_prefix: Optional[str] = None,
    device: Optional[torch.device] = None,
    log_interval: Optional[int] = None,
    log_capacity: Optional[int] = None,
) -> None:
    """Setup the namespace object provided by the game and initialize the agent.

    Optional parameters are queried from the system environment or, if that fails, set to a fitting
    default value.

    The used system environment variables are:

    - `MODEL_PATH`
    - `DATA_PATH`
    - `PREFIX`
    - `USE_GPU`
    - `LAST_ROUND`
    - `SEED`
    - `LOG_INTERVAL`
    - `LOG_CAPACITY`

    Parameters
    ----------
    agent : Agent
        The agent to generate the callbacks for.

    model_path : PathLike
        The path to use as the agent's model path. If not provided, try to load from environment
        variable `MODEL_PATH`. If unsuccessful, use `f"{os.path.abspath(os.curdir)}/models"`.

    data_path : PathLike
        The path to use as the event manager's save path. If not provided, try to load from
        environment variable `DATA_PATH`. If unsuccessful, use
        `f"{os.path.abspath(os.curdir)}/data"`.

    base_events : Optional[List[Tuple[str, Optional[float]]]]
        The base events to enable. If `None`, use all available ones. Provide a `float` parameter to
        override pre-defined rewards.

    custom_events : Optional[List[Event]]
        The custom events to enable. If `None`, use all available ones.

    preprocessors : Optional[List[StatePreprocessor]]
        The state preprocessors to use.

    store_prefix : Optional[str]
        A prefix to use for save files. If not provided, try to load from environment variable
        `PREFIX`. If unsuccessful, use `f"{agent.name}"`.

    device : Optional[torch.device]
        The device to use. If not provided, try to use `CUDA` device if environment variable
        `USE_GPU` is set, else `CPU`.

    log_interval : int
        The number of rounds to wait before saving newly logged data to disk. Values are averaged
        over the set interval.

    log_capacity : int
        The number of rounds to store in the data logger. Affects the amount of data that gets
        stored to disk during training.
    """
    # Load variables from system environment if not provided manually.
    model_path = os.getenv("MODEL_PATH") if model_path is None else model_path
    data_path = os.getenv("DATA_PATH") if data_path is None else data_path
    store_prefix = os.getenv("PREFIX") if store_prefix is None else store_prefix
    if device is None:
        use_cuda = os.getenv("USE_GPU") == "True" and torch.cuda.is_available()
        device = torch.device("cuda" if use_cuda else "cpu")

    # Get last round number from environment.
    last_round = os.getenv("LAST_ROUND")
    if last_round is not None:
        last_round = int(last_round)

    # Get random seed from environment.
    seed = os.getenv("SEED")
    if seed is not None:
        seed = int(seed)
        random.seed = seed

    # Check if log interval was provided.
    log_interval_var = os.getenv("LOG_INTERVAL") if log_interval is None else log_interval
    log_interval_var = 1 if log_interval_var is None else int(log_interval_var)

    # Check if log capacity was provided.
    log_capacity_var = os.getenv("LOG_CAPACITY") if log_capacity is None else log_capacity
    log_capacity_var = 100_000 if log_capacity_var is None else int(log_capacity_var)

    # Set defaults if values are missing.
    model_path = f"{os.path.abspath(os.curdir)}/models" if model_path is None else model_path
    data_path = f"{os.path.abspath(os.curdir)}/data" if data_path is None else data_path
    store_prefix = f"{agent.name}" if store_prefix is None else store_prefix

    # Add namespace data.
    self.event_processor = EventProcessor(
        base_events=base_events,
        custom_events=custom_events,
    )
    self.is_in_training = False
    self.last_actions = deque([], maxlen=400)
    self.data_logger = DataLogger(capacity=log_capacity_var)
    self.model_path = model_path
    self.data_path = data_path
    self.preprocessors = preprocessors if preprocessors is not None else []
    self.store_prefix = store_prefix
    self.agent = agent
    self.last_round = last_round
    self.total_round_reward = 0
    self.log_interval = log_interval_var

    # Set agent values.
    self.agent.set_device(device)
    self.agent.save_path = model_path
    self.agent.logger = self.logger
    self.agent.data_logger = self.data_logger

    # Load stored model.
    self.agent.load_model()
    self.data_logger.current_round = self.agent.rounds_since_creation + 1


def generate_callbacks(
    agent: Agent,
    model_path: Optional[Union[str, os.PathLike]] = None,
    data_path: Optional[Union[str, os.PathLike]] = None,
    base_events: Optional[List[Tuple[str, Optional[float]]]] = None,
    custom_events: Optional[List[Event]] = None,
    preprocessors: Optional[List[StatePreprocessor]] = None,
    store_prefix: Optional[str] = None,
    device: Optional[torch.device] = None,
    log_interval: Optional[int] = None,
) -> Tuple[SetupFuncType, ActFuncType]:
    """Create the callbacks the game expects in the `callbacks.py` file for the given agent.

    Fields added to the SelfNamespace during setup:

     1) event_processor : EventProcessor
        The event processor used during training.

     2) is_in_training : bool
        Marks if the current games are played in training or play mode. Is updated in
        `setup_training`.

     3) last_actions : Deque[int]
        The last actions the agent performed. Stores up to a whole round (400 actions)

     4) data_logger : DataLogger
        The data logger used during the game.

     5) model_path : str
        The path to use to store models.

     6) data_path : str
        The path to use to store data.

     7) store_prefix : str
        The prefix to store files with.

     8) agent : Agent
        The agent instance.

     9) preprocessors : List[StatePreprocessor]
        The active state preprocessor instances.

    10) last_round : Optional[int]
        The last round to be played.

    11) total_round_reward : float
        The total reward accumulated during the current round.

    12) log_interval : int
        The number of rounds to wait before saving newly logged data to disk. Values are averaged
        over the set interval.

    Parameters
    ----------
    agent : Agent
        The agent to generate the callbacks for.

    model_path : PathLike
        The path to use as the agent's model path. If not provided, try to load from environment
        variable `MODEL_PATH`. If unsuccessful, use `f"{os.path.abspath(os.curdir)}/models"`.

    data_path : PathLike
        The path to use as the event manager's save path. If not provided, try to load from
        environment variable `DATA_PATH`. If unsuccessful, use
        `f"{os.path.abspath(os.curdir)}/data"`.

    base_events : Optional[List[Tuple[str, Optional[float]]]]
        The base events to enable. If `None`, use all available ones. Provide a `float` parameter to
        override pre-defined rewards.

    custom_events : Optional[List[Event]]
        The custom events to enable. If `None`, use all available ones.

    preprocessors : Optional[List[StatePreprocessor]]
        The state preprocessors to use.

    store_prefix : Optional[str]
        A prefix to use for save files. If not provided, try to load from environment variable
        `PREFIX`. If unsuccessful, use `f"{agent.name}"`.

    device : Optional[torch.device]
        The device to use. If not provided, try to use `CUDA` device if environment variable
        `USE_GPU` is set, else `CPU`.

    log_interval : int
        The number of rounds to wait before saving newly logged data to disk. Values are averaged
        over the set interval.

    Returns
    ----------
    setup : SetupFuncType
        The `setup(self: SimpleNamespace)` callback function.

    act : ActFuncType
        The `act(self: SimpleNamespace, game_state: Dict[str, Any]) -> str` callback function.
    """

    def setup(self: SimpleNamespace) -> None:
        """Create agent, setup values, and try to load old state."""
        _setup_namespace_and_agent(
            self=self,
            agent=agent,
            model_path=model_path,
            data_path=data_path,
            base_events=base_events,
            custom_events=custom_events,
            preprocessors=preprocessors,
            store_prefix=store_prefix,
            device=device,
            log_interval=log_interval,
        )

    def act(self: SimpleNamespace, game_state: Dict[str, Any]) -> str:
        """Let the agent select an action based on the game state.

        Parameters
        ----------
        game_state : Dict[str, Any]
            The game state dictionary. For a list of possible keys, please refer to constants.State.

        Returns
        ----------
        action_name : str
            The name of the selected action.
        """
        current_round = State.get_round(game_state)
        current_step = State.get_step(game_state)

        # Preprocess game state.
        preprocessors: List[StatePreprocessor] = self.preprocessors
        for preprocessor in preprocessors:
            game_state = preprocessor.process(self, game_state)

        # Select next action
        agent: Agent = self.agent
        features = agent.feature_extractor.extract_features(self, game_state).to(agent.device)
        features = agent.preprocess_features(features, game_state)
        action = agent.select_action(features, current_round, current_step)
        self.last_actions.append(action)
        action_name = agent.action_set.get_action_name(action)

        # Logging
        self.data_logger.log_datum(action_name, 1)
        return action_name

    return setup, act
