import os
from logging import Logger
from typing import List, Optional, Union

import torch

from src import features
from src.agents import Agent
from src.info.actions import Actions, ActionSet


class RuleBasedITiger(Agent):
    """An agent that always takes the highest coin gradient value as next step."""

    def __init__(
        self,
        feature_extractor: features.FeatureExtractorCollection,
        action_set: ActionSet,
        device: torch.device,
        memory_size: int = 10000,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
        name: Optional[str] = None,
    ) -> None:

        super().__init__(
            feature_extractor=feature_extractor,
            action_set=action_set,
            device=device,
            memory_size=memory_size,
            do_auto_save=do_auto_save,
            save_interval=save_interval,
            save_path=save_path,
            logger=logger,
            name=name,
        )

    def _select_action_implementation(
        self,
        features: torch.Tensor,
        _current_round: int,
        _current_step: int,
    ) -> int:
        '''
        if features[0][4].item() != -1:
            return int(features[0][4].item())

        if features[0][5].item() == 1:
            return self.action_set.get_action_id(Actions.BOMB)


        move_features = features[0][0:4]

        """Move towards largest direction value."""
        index = np.argmax(move_features).item()
        action_id = Actions.INVALID_ID
        if index == 0:
            action_id = self.action_set.get_action_id(Actions.LEFT)
        if index == 1:
            action_id = self.action_set.get_action_id(Actions.RIGHT)
        if index == 2:
            action_id = self.action_set.get_action_id(Actions.UP)
        if index == 3:
            action_id = self.action_set.get_action_id(Actions.DOWN)

        return action_id
        '''
        # print("Left: ", self.action_set.get_action_id(Actions.LEFT))
        # print("Right: ", self.action_set.get_action_id(Actions.RIGHT))
        # print("Up: ", self.action_set.get_action_id(Actions.UP))
        # print("Down: ", self.action_set.get_action_id(Actions.DOWN))
        # print(("Instruction: ") + Actions.get_action_name(int(float(features[0][0].item()))))
        features = features[0].numpy()
        # print("features:", features)
        # print("fratuire 1", int(features[1]))
        if features[1] != -1:
            # print("r: ", Actions.get_action_name(int(features[1])))
            return int(features[1])

        if features[2] == 1:
            # print("r: Bomb")
            return Actions.ACTION_TO_ID[Actions.BOMB]

        if features[0] != -1:
            # print("features[0]: ", features[0], " ",
            #      self.action_set.get_action_name(int(features[0])))
            # print("r: ", Actions.get_action_name(int(features[0])))
            return int(features[0])

        # print("r: Wait")
        return Actions.ACTION_TO_ID[Actions.WAIT]
