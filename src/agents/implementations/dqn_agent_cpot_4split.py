import math
import os
import random
from logging import Logger
from typing import Any, Dict, Optional, Union

import numpy as np
import torch
from torch import Tensor, nn, optim
from src.agents.base.training_batch import sample_batch_from_replay_buffer

from src.agents.implementations.cdqn_agent import CDQNAgent
from src.agents import Transition
from src.features import FeatureExtractor
from src.info import State
from src.info.actions import Actions, ActionSet
from src.info.logging import DataLogger
from src.utility import utility
from src.info.world import World


class DQNAgentCpot4split(CDQNAgent):
    def __init__(
        self,
        feature_extractor: FeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        batch_size: int,
        gamma: float,
        eps_start: float,
        eps_end: float,
        eps_decay: float,
        tau: float,
        lr: float,
        n_inner: int,
        conv_input_shape: tuple[int, int],
        n_hidden: int = 3,
        use_4split: bool = True,
        update_interval: int = 1,
        target_update_interval: int = 1,
        n_out: int = CDQNAgent.USE_ACTION_SET_OUTPUT,
        loss_function: CDQNAgent.LossFunction = CDQNAgent.LossFunction.SMOOTH_L1,
        n_conv_channels: int = 16,
        conv_kernel_shape: tuple[int, int] = (3, 3),
        n_conv_layers: int = 1,
        batch_normalization: bool = True,
        relu: bool = True,
        memory_size: int = 10000,
        do_random_choices: bool = True,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
        data_logger: Optional[DataLogger] = None,
        name: Optional[str] = None,
    ) -> None:
        self.current_position = (1, 1)
        self.experience_map = np.zeros((World.get_world_height(), World.get_world_width()))
        self.current_round = 1
        self.use_4split = use_4split
        super().__init__(
            feature_extractor=feature_extractor,
            action_set=action_set,
            device=device,
            batch_size=batch_size,
            gamma=gamma,
            eps_start=eps_start,
            eps_end=eps_end,
            eps_decay=eps_decay,
            tau=tau,
            lr=lr,
            n_inner=n_inner,
            conv_input_shape=conv_input_shape,
            n_conv_channels=n_conv_channels,
            conv_kernel_shape=conv_kernel_shape,
            n_conv_layers=n_conv_layers,
            batch_normalization=batch_normalization,
            relu=relu,
            n_hidden=n_hidden,
            update_interval=update_interval,
            target_update_interval=target_update_interval,
            n_out=n_out,
            loss_function=loss_function,
            memory_size=memory_size,
            do_random_choices=do_random_choices,
            do_auto_save=do_auto_save,
            save_interval=save_interval,
            save_path=save_path,
            logger=logger,
            data_logger=data_logger,
            name=name,
        )

    def _setup_training_implementation(self) -> None:
        self.do_random_choices = True
        self.do_auto_save = True

    def _select_action_implementation(
            self,
            features: torch.Tensor,
            _current_round: int,
            _current_step: int,
    ) -> int:
        """Select an action based on the set features.
        """
        def do_random_action() -> bool:
            if not self.do_random_choices:
                return False
            eps_diff = self.eps_start - self.eps_end
            eps_threshold = self.eps_end + eps_diff * \
                            math.exp(-1 * self.rounds_since_creation / self.eps_decay)
            return random.random() < eps_threshold
        pre_action_id = Actions.INVALID_ID
        if do_random_action():
            self.random_choices += 1
            if self.data_logger is not None:
                self.data_logger.log_datum("RANDOM_ACTION_RATIO", 0.25)
            action_ids = [
                self.action_set.get_action_id(Actions.UP),
                self.action_set.get_action_id(Actions.RIGHT),
                self.action_set.get_action_id(Actions.DOWN),
                self.action_set.get_action_id(Actions.LEFT)]
            action_choice = utility.pick_least_experienced_tile(
                action_ids=action_ids,
                experience_map=self.experience_map,
                position=self.current_position,
                transposed=True,
                use_4split=self.use_4split,
            )
            pre_action_id = int(torch.tensor([[action_choice]], device=self.device, dtype=torch.long).item())
        else:
            pre_action_id = self.policy_net(features).max(1)[1].view(1, 1).item()
            # Call correct_action() if you want action to be adapted according to 4split
        if self.use_4split:
            self.uncorrected_action = pre_action_id
            pre_action_id = self.action_set.get_action_id(utility.correct_quadrant_movement(
                self.action_set.get_action_name(pre_action_id),
                utility.get_current_quadrant(position=self.current_position, transposed=True),
                [Actions.UP, Actions.RIGHT, Actions.DOWN, Actions.LEFT]))
        return pre_action_id

    def preprocess_features(self, features: Tensor, _game_state: Dict[str, Any]) -> Tensor:
        """Only used to memorize the agent position to correct the movement according to 4split.
        """
        self.current_position = State.get_self_values(_game_state).position
        self.experience_map[self.current_position[1], self.current_position[0]] += 1
        if State.get_round(_game_state) != self.current_round:
            self.current_round = State.get_round(_game_state)
            self.experience_map = np.zeros((World.get_world_height(), World.get_world_width()))
            self.experience_map = np.zeros((World.get_world_height(), World.get_world_width()))
        return features

    def _optimize_model_implementation(self) -> None:
        """Optimize model by evaluating a batch of state transitions."""
        if self.use_4split:
            # Since we alter the output depending on the agent position, we cant use the altered
            # output to compare it to the unaltered output of the network. Hence, save unaltered action.
            last_transition = self.memory.memory.pop()
            last_transition = Transition(
                last_transition.old_features,
                torch.tensor([[self.uncorrected_action]], device=self.device),
                last_transition.new_features,
                last_transition.reward,
            )
            self.memory.push(last_transition)
        if len(self.memory) < self.batch_size:
            return

        batch = sample_batch_from_replay_buffer(self.memory, self.batch_size)

        non_final_mask = torch.tensor(
            [s is not None for s in batch.new_state_features],
            device=self.device,
            dtype=torch.bool,
        )
        non_final_next_states = torch.cat([s for s in batch.new_state_features if s is not None])

        # Concatenate batch tensors into one larger one each
        old_features_batch = torch.cat(batch.old_state_features)
        action_batch = torch.cat(batch.actions)
        reward_batch = torch.cat(batch.rewards)

        # Infer old policy network results from old features and combine with actions.
        state_feature_action_values = self.policy_net(old_features_batch).gather(1, action_batch)

        # Infer "future" target network results for non-final states.
        next_state_values = torch.zeros(self.batch_size, device=self.device)

        with torch.no_grad():
            next_state_values[non_final_mask] = self.target_net(
                non_final_next_states
            ).max(1)[0]

        # Compute expected "future" reward by multiplying rewards predicted by target network with
        # the chosen gamma value and adding the observed rewards.
        expected_state_action_values = (
                (next_state_values * self.gamma) + reward_batch
        ).float()

        # Perform an optimizer step using the specified loss function.
        if self.loss_function == self.LossFunction.SMOOTH_L1:
            criterion = nn.SmoothL1Loss()
        elif self.loss_function == self.LossFunction.HUBER:
            criterion = nn.HuberLoss()
        else:
            raise RuntimeError(f"Unknown loss function with value {self.loss_function}.")

        criterion = nn.HuberLoss()
        loss = criterion(state_feature_action_values, expected_state_action_values.unsqueeze(1))
        self.data_logger.log_datum("LOSS", float(loss))
        self.optimizer.zero_grad()
        loss.backward()
        # torch.nn.utils.clip_grad_norm_(
        #     self.policy_net.parameters(),
        #     2,
        #     error_if_nonfinite=True,
        # )
        lr_start = 1e-5
        lr_end = 1e-7
        lr_decay = 1500
        lr_diff = lr_start - lr_end
        lr_current = lr_end + lr_diff * \
                     math.exp(-1 * self.rounds_since_creation / lr_decay)
        self.data_logger.log_datum("LR", lr_current / 400.0)
        self.optimizer.param_groups[0]['lr'] = lr_current
        self.optimizer.step()

        # Update target net by copying from policy net every target_update_interval steps.
        if self.optimization_steps_since_creation % self.target_update_interval != 0:
            return
        target_net_state_dict = self.target_net.state_dict()
        policy_net_state_dict = self.policy_net.state_dict()
        for key in policy_net_state_dict:
            policy_weight = policy_net_state_dict[key]
            target_weight = target_net_state_dict[key]
            target_net_state_dict[key] = policy_weight * \
                self.tau + target_weight * (1 - self.tau)
        self.target_net.load_state_dict(target_net_state_dict)
