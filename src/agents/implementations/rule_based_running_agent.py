import os
from logging import Logger
from typing import Any, Dict, Optional, Union

import numpy as np
import torch
from torch import Tensor

from src.agents import Agent
from src.features import FeatureExtractor
from src.info import State
from src.info.actions import Actions, ActionSet
from src.utility import utility


class RuleBasedRunningAgent(Agent):
    """An agent that always takes any allowed direction."""

    def __init__(
        self,
        feature_extractor: FeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        memory_size: int = 10000,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
    ) -> None:
        # has_correct_extractor = type(feature_extractor) == MovementAllowedFeatureExtractor
        # wrong_feature_extractor_msg = "This agent only works with MovementAllowedFeatureExtractor!"
        # assert has_correct_extractor, wrong_feature_extractor_msg
        self.current_position = ()
        super().__init__(
            feature_extractor,
            action_set,
            device,
            memory_size,
            do_auto_save,
            save_interval,
            save_path,
            logger,
        )

    def _select_action_implementation(
        self,
        features: torch.Tensor,
        _current_round: int,
        _current_step: int,
    ) -> int:
        """Take the next allowed movement."""
        index = np.argmax(torch.Tensor.cpu(features[0])).item()
        pre_action_id = Actions.INVALID_ID
        if index == 0:
            pre_action_id = self.action_set.get_action_id(Actions.UP)
        if index == 1:
            pre_action_id = self.action_set.get_action_id(Actions.RIGHT)
        if index == 2:
            pre_action_id = self.action_set.get_action_id(Actions.DOWN)
        if index == 3:
            pre_action_id = self.action_set.get_action_id(Actions.LEFT)
        return self.action_set.get_action_id(
            utility.correct_quadrant_movement(
                self.action_set.get_action_name(pre_action_id),
                utility.get_current_quadrant(position=self.current_position, transposed=True),
                [Actions.UP, Actions.RIGHT, Actions.DOWN, Actions.LEFT],
            ),
        )

    def preprocess_features(self, features: Tensor, _game_state: Dict[str, Any]) -> Tensor:
        """Only used to memorize the agent position to correct the movement according to 4split.
        """
        self.current_position = State.get_self_values(_game_state).position
        return features
