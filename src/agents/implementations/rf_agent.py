import os
import pickle
import random
from logging import Logger
from types import SimpleNamespace
from typing import Any, Dict, List, Optional, Union, cast

import numpy as np
import numpy.typing as npt
import torch
from sklearn.ensemble import RandomForestRegressor

from src.agents.base.agent import Agent
from src.agents.base.replaymemory import ReplayMemory, Transition
from src.features.base.feature_extractor import FeatureExtractor
from src.info.actions import ActionSet
from src.info.logging import DataLogger


class RFAgent(Agent):

    def __init__(
        self,
        feature_extractor: FeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        random_prob: float = 0.1,
        fit_interval: int = 400,
        n_estimators: int = 10,
        memory_size: int = 10000,
        prioritization_alpha: float = 0.6,
        prioritization_beta: float = 0.4,
        prioritization_eps: float = 1e-2,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
        data_logger: Optional[DataLogger] = None,
        name: Optional[str] = None,
    ) -> None:
        """Initialize an agent instance.

        Parameters
        ----------
        feature_extractor : FeatureExtractor
            The feature extractor to use.

        action_set : ActionSet
            The set of actions the agent can use.

        device : torch.device
            The device to use for this agent.

        memory_size : int
            The size of the internal replay memory.

        use_prioritized_sampling : bool
            Determines if a prioritized replay buffer is used.

        prioritization_alpha : float
            The alpha value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        prioritization_beta : float
            The beta value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        prioritization_eps : float
            The epsilon value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        do_auto_save : bool
            If `True`, save the model automatically.

        save_interval : int
            If `do_auto_save` is `True`, save the model after this number of optimization steps have
            passed.

        save_path : Union[str, os.PathLike]
            The path to save the model to.

        logger : Optional[Logger]
            The logger to use for logging messages.

        data_logger : Optional[DataLogger]
            The data logger to use for logging data.

        name: Optional[str]
            The name of this agent.
        """
        super().__init__(
            feature_extractor=feature_extractor,
            action_set=action_set,
            device=device,
            memory_size=memory_size,
            use_prioritized_sampling=False,
            prioritization_alpha=prioritization_alpha,
            prioritization_beta=prioritization_beta,
            prioritization_eps=prioritization_eps,
            do_auto_save=do_auto_save,
            save_interval=save_interval,
            save_path=save_path,
            logger=logger,
            data_logger=data_logger,
            name=name,
        )
        self.memory = cast(ReplayMemory, self.memory)
        self.random_prob = random_prob
        self.is_training = False
        self.fit_interval = fit_interval
        self.forests = [RandomForestRegressor(n_estimators=n_estimators)
                        for _ in action_set.get_all_actions()]
        for forest in self.forests:
            forest.fit(np.zeros(shape=self.feature_extractor.n_features()), [0])

    def _select_action_implementation(
        self,
        features: torch.Tensor,
        _current_round: int,
        _current_step: int,
    ) -> int:
        """Select and action based on the given feature tensor and the current time step.

        Parameters
        ----------
        features : torch.Tensor
            The features to use.

        current_round : int
            The current game round.

        current_step : int
            The current round step.

        Returns
        ----------
        action_id : int
            The ID of the selected action.
        """
        if self.is_training and random.random() < self.random_prob:
            return self.action_set.get_action_id(
                np.random.choice(
                    self.action_set.get_all_actions(),
                    p=[0.2, 0.2, 0.2, 0.2, 0.1, 0.1],
                ),
            )

        f = np.array([feature.item() for feature in features])
        selected_action = int(np.argmax([forest.predict(f.reshape(-1, 1))
                              for forest in self.forests]))
        # print(selected_action)
        return selected_action

    def _save_model_implementation(
        self,
        directory: Union[str, os.PathLike],
        file_name_base: str,
    ) -> None:
        """Save the current model state.

        Parameters
        ----------
        directory : PathLike
            The directory to save to.

        file_name_base : str
            The base of the file name to use. Can be extended but should not be prefixed.
        """
        with open(f"{directory}/{file_name_base}", mode="wb") as f:
            pickle.dump(self.forests, f)

    def _load_model_implementation(
        self,
        directory: Union[str, os.PathLike],
        file_name_base: str,
    ) -> None:
        """Load a previous model state.

        Parameters
        ----------
        directory : PathLike
            The directory to load from.

        file_name_base : str
            The base of the file name to use. Can be extended but should not be prefixed.
        """
        try:
            with open(f"{directory}/{file_name_base}", mode="rb") as f:
                self.forests = pickle.load(f)
        except Exception as e:  # noqa: BLE001
            self.log_message(f"Failed to load model. Error message: {e}", log_as_error=True)
        else:
            self.log_message("Loaded forests.")

    def _setup_training_implementation(self) -> None:
        """Perform additional training initialization steps."""
        self.is_training = True

    def pre_optimize(
        self,
        self_namespace: SimpleNamespace,
        old_game_state: Dict[str, Any],
        self_action: int,
        new_game_state: Optional[Dict[str, Any]],
        total_reward: float,
        _reward_per_source: Dict[str, float],
    ) -> bool:
        """Extract old and new features and push state transition to replay memory.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : int
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in final state transition.

        total_reward : float
            The total reward computed for this transition.

        reward_per_source : Dict[str, float]
            The individual rewards per processed event.

        Returns
        ----------
        should_optimize : bool
            If `False`, do not optimize the model in this step.
        """
        # Extract features.
        feature_extractor: FeatureExtractor = self.feature_extractor
        old_features = self.preprocess_features(
            feature_extractor.extract_features(self_namespace, old_game_state),
            old_game_state,
        ).to(self.device)
        if new_game_state is not None:
            new_features = self.preprocess_features(
                feature_extractor.extract_features(self_namespace, new_game_state),
                new_game_state,
            ).to(self.device)
        else:
            new_features = None

        # Push them into memory.
        transition = Transition(
            old_features,
            torch.tensor([[self_action]], device=self.device),
            new_features,
            torch.tensor([total_reward], device=self.device),
        )
        self.memory.push(transition)

        return self.steps_since_creation % self.fit_interval == 0

    def _optimize_model_implementation(self) -> None:
        """Perform one model optimization step."""

        gamma = 0.9

        # Create temporary lists for each action/forest
        _action_rewards: List[List[float]] = [[]]*self.action_set.n_actions()
        _action_old_features: List[List[float]] = [[]]*self.action_set.n_actions()
        _action_new_features: List[List[float | None]] = [[]]*self.action_set.n_actions()

        # Filter the Transitions into their respective inner List depending on the Transitions Action
        transition: Transition
        for transition in self.memory.memory:
            _action_rewards[transition.action].append(transition.reward.item())
            _action_old_features[transition.action].append(transition.old_features.item())
            _action_new_features[transition.action].append(
                transition.new_features.item() if transition.new_features else None)

        action_rewards = np.array(_action_rewards)
        action_old_features = np.array(_action_old_features)
        action_new_features = np.array(_action_new_features)

        action_id: int
        for action_id, (rewards, old_features, new_features) in enumerate(
                zip(action_rewards, action_old_features, action_new_features)):
            # filter out new and old features where new_features have None values
            mask = new_features != None

            q_vals = rewards[mask] + gamma * np.max([forest.predict(new_features[mask].reshape((-1, 1)))
                                                     for forest in self.forests], axis=0)
            self.forests[action_id].fit(old_features[mask].reshape((-1, 1)), q_vals)
