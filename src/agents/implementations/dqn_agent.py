import math
import os
import random
from enum import Enum
from logging import Logger
from types import SimpleNamespace
from typing import Any, Dict, List, NamedTuple, Optional, Tuple, Union

import torch
from torch import Tensor, nn, optim

from src.agents import Agent, FullyConnectedDQNetwork, Transition
from src.agents.base.prioritized_replaymemory import PrioritizedReplayMemory
from src.agents.base.training_batch import sample_batch_from_replay_buffer
from src.features import FeatureExtractor
from src.info.actions import ActionSet
from src.info.logging import DataLogger
from src.info.world import Settings


class DQNAgent(Agent):
    """DQN agent that uses a policy/target net approach."""

    USE_ACTION_SET_OUTPUT = -1
    """If provided as `n_out` argument, use the size of the provided action set as output size."""

    class Batch(NamedTuple):
        old_features: Tuple[Tensor, ...]
        actions: Tuple[Tensor, ...]
        new_features: Tuple[Optional[Tensor], ...]
        rewards: Tuple[Tensor, ...]

    class LossFunction(Enum):
        SMOOTH_L1 = 0
        HUBER = 1

    def __init__(
        self,
        feature_extractor: FeatureExtractor,
        action_set: ActionSet,
        device: torch.device,
        batch_size: int,
        gamma: float,
        eps_start: float,
        eps_end: float,
        eps_decay: float,
        tau: float,
        lr: float,
        n_inner: int,
        n_hidden: int = 3,
        update_interval: int = 1,
        target_update_interval: int = 1,
        use_double_dqn: bool = False,
        n_out: int = USE_ACTION_SET_OUTPUT,
        loss_function: LossFunction = LossFunction.SMOOTH_L1,
        memory_size: int = 10000,
        use_prioritized_sampling: bool = False,
        prioritization_alpha: float = 0.6,
        prioritization_beta: float = 0.4,
        prioritization_eps: float = 1e-2,
        auto_load_memory: bool = True,
        do_random_choices: bool = True,
        do_auto_save: bool = True,
        save_interval: int = 1000,
        save_path: Union[str, os.PathLike] = "./models",
        logger: Optional[Logger] = None,
        data_logger: Optional[DataLogger] = None,
        name: Optional[str] = None,
    ) -> None:
        """Initialize an agent instance.

        Parameters
        ----------
        feature_extractor : FeatureExtractor
            The feature extractor to use.

        action_set : ActionSet
            The set of actions the agent can use.

        device : torch.device
            The device to use for this agent.

        batch_size : int
            The number of replays to use for one optimization step.

        gamma : float
            Value in [0, 1] that determines how much the agent should focus on future rewards
            instead of short-term ones.

        eps_start : float
            The initial chance to pick a random action.

        eps_end : float
            The final chance to pick a random action.

        eps_decay : float
            The rate at which the epsilon value reduces each played round. Higher values mean a
            slower decrease.

        tau : float
            Determines how much the target net is updated in each optimization step.

        lr : float
            The learning rate to use for network optimization.

        n_inner : int
            The number of neurons per fully connected layer in the Q-Network.

        n_hidden : int
            The number of fully connected hidden layers to use in the Q-Network.

        update_interval : int
            The number of steps between weight updates.

        target_update_interval : int
            The number of steps between target network weight updates.

        use_double_dqn : bool
            If `True`, use the double DQN algorithm for updating network weights.

        n_out : int
            The umber of outputs. Set to `USE_ACTION_SET_COUNT_OUTPUT` to use number of actions in
            the provided action set as output number.

        loss_function : LossFunction
            The loss function to use.

        memory_size : int
            The size of the internal replay memory.

        use_prioritized_sampling : bool
            Determines if a prioritized replay buffer is used.

        prioritization_alpha : float
            The alpha value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        prioritization_beta : float
            The beta value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        prioritization_eps : float
            The epsilon value to use for the prioritized replay buffer if `use_prioritized_sampling`
            is `True`.

        auto_load_memory : bool
            If `True`, try to load stored replays when setting up training.

        do_random_choices : bool
            If `False`, never pick a random action.

        do_auto_save : bool
            If `True`, save the model automatically.

        save_interval : int
            If `do_auto_save` is `True`, save the model after this number of optimization steps have
            passed.

        save_path : Union[str, os.PathLike]
            The path to save the model to.

        logger : Optional[Logger]
            The logger to use for logging messages.

        data_logger : Optional[DataLogger]
            The data logger to use for logging data.

        name: Optional[str]
            The name of this agent.
        """
        super().__init__(
            feature_extractor=feature_extractor,
            action_set=action_set,
            device=device,
            memory_size=memory_size,
            use_prioritized_sampling=use_prioritized_sampling,
            prioritization_alpha=prioritization_alpha,
            prioritization_beta=prioritization_beta,
            prioritization_eps=prioritization_eps,
            do_auto_save=do_auto_save,
            save_interval=save_interval,
            save_path=save_path,
            logger=logger,
            data_logger=data_logger,
            name=name,
        )

        # Assert correct feature dimensions.
        feature_dims = feature_extractor.n_features()
        wrong_dims_msg = "Agent only supports two dimensional feature tensors with dim[0] = 1!"
        assert len(feature_dims) == 2, wrong_dims_msg
        assert feature_dims[0] == 1, wrong_dims_msg

        # Initialize variables
        self.batch_size = batch_size
        self.gamma = gamma
        self.eps_start = eps_start
        self.eps_end = eps_end
        self.eps_decay = eps_decay
        self.tau = tau
        self.lr = lr
        self.update_interval = update_interval
        self.target_update_interval = target_update_interval
        self.use_double_dqn = use_double_dqn
        self.loss_function = loss_function
        self.n_out = self.action_set.n_actions() if n_out == self.USE_ACTION_SET_OUTPUT else n_out
        self.n_observations = feature_extractor.n_features()[-1]
        self.n_inner = n_inner
        self.n_layers = n_hidden
        self.do_random_choices = do_random_choices
        self.auto_load_memory = auto_load_memory

        self.random_choices_this_round = 0
        self.total_choices = 0
        self.actions_this_round = 0

        # Create networks
        self._create_networks()

    def pre_optimize(
        self,
        self_namespace: SimpleNamespace,
        old_game_state: Dict[str, Any],
        self_action: int,
        new_game_state: Optional[Dict[str, Any]],
        total_reward: float,
        _reward_per_source: Dict[str, float],
    ) -> bool:
        """Extract old and new features and push state transition to replay memory.

        Parameters
        ----------
        self_namespace : SimpleNamespace
            The namespace object provided by the game.

        old_game_state : Dict[str, Any]
            The game state before the state transition.

        self_action : int
            The action that was performed by the agent in this transition.

        new_game_state : Optional[Dict[str, Any]]
            The new game state after the state transition. Is `None` in final state transition.

        total_reward : float
            The total reward computed for this transition.

        _reward_per_source : Dict[str, float]
            The individual rewards per processed event.

        Returns
        ----------
        should_optimize : bool
            If `False`, do not optimize the model in this step.
        """
        # Extract features.
        feature_extractor: FeatureExtractor = self.feature_extractor
        old_features = self.preprocess_features(
            feature_extractor.extract_features(self_namespace, old_game_state),
            old_game_state,
        ).to(self.device)
        if new_game_state is not None:
            new_features = self.preprocess_features(
                feature_extractor.extract_features(self_namespace, new_game_state),
                new_game_state,
            ).to(self.device)
        else:
            new_features = None
        
        # Push them into memory.
        transition = Transition(
            old_features,
            torch.tensor([[self_action]], device=self.device),
            new_features,
            torch.tensor([total_reward], device=self.device),
        )
        self.memory.push(transition)

        # Only optimize if there are enough replays saved.
        if len(self.memory) < self.batch_size:
            return False

        # Optimize in the desired frequency.
        return self.total_choices % self.update_interval == 0

    def _create_networks(self) -> None:
        self.policy_net = FullyConnectedDQNetwork(
            self.n_observations,
            self.n_out,
            self.n_inner,
            self.n_layers,
        ).to(self.device)
        self.target_net = FullyConnectedDQNetwork(
            self.n_observations,
            self.n_out,
            self.n_inner,
            self.n_layers,
        ).to(self.device)
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.optimizer = optim.AdamW(self.policy_net.parameters(), lr=self.lr, amsgrad=True)

    def _select_action_implementation(
        self,
        features: torch.Tensor,
        _current_round: int,
        _current_step: int,
    ) -> int:
        """Select an action based on the set eps values. Higher values mean the agent is more
        likely to choose random actions.

        Parameters
        ----------
        features : Tensor
            The feature tensor to use.

        _current_round : int
            The active round in the current game session.

        _current_step : int
            The current step in the active game.

        Returns
        ----------
        action : int
            The action index encoding one of the possible outputs.
        """
        self.actions_this_round += 1

        def do_random_action() -> bool:
            if not self.do_random_choices:
                return False
            full_games_since_creation = self.steps_since_creation / Settings.get_max_steps()
            exponential = math.exp(-1 * full_games_since_creation / self.eps_decay)
            eps_threshold = self.eps_end + (self.eps_start - self.eps_end) * exponential
            return random.random() < eps_threshold

        if do_random_action():
            if self.data_logger is not None:
                self.random_choices_this_round += 1
                self.data_logger.log_datum("RANDOM_ACTION", 1)
            choice = random.randint(0, self.n_out - 1)
            return int(torch.tensor([[choice]], device=self.device, dtype=torch.long).item())
        return self.policy_net(features).max(1)[1].view(1, 1).item()

    def _optimize_model_implementation(self) -> None:
        """Optimize model by evaluating a batch of state transitions.

        To do so, we sample a batch of replays for which we then predict the expected future rewards
        using the target network and optimize using the resulting gradients.
        """
        # Only optimizer if we saved enough replays to form a training batch.
        if len(self.memory) < self.batch_size:
            return

        # Sample batch and determine non-final follow state features.
        batch = sample_batch_from_replay_buffer(self.memory, self.batch_size)
        non_final_mask = torch.tensor(
            [s is not None for s in batch.new_state_features],
            device=self.device,
            dtype=torch.bool,
        )
        non_final_new_features = torch.cat([s for s in batch.new_state_features if s is not None])

        # Concatenate batch tensors into one larger one each.
        old_features_batch = torch.cat(batch.old_state_features)
        action_batch = torch.cat(batch.actions)
        reward_batch = torch.cat(batch.rewards)
        weight_batch = torch.cat(batch.weights)

        # Infer old policy network results from old features and combine with actions.
        state_feature_action_values = self.policy_net(old_features_batch).gather(1, action_batch)
        state_feature_action_values = weight_batch.unsqueeze(1) * state_feature_action_values

        # Infer "future" target network results for non-final states.
        next_state_feature_values = torch.zeros(self.batch_size, device=self.device)
        if self.use_double_dqn:
            with torch.no_grad():
                next_state_actions = self.policy_net(non_final_new_features).max(1)[1].unsqueeze(1)
                next_state_feature_values[non_final_mask] = self.target_net(
                    non_final_new_features,
                ).gather(1, next_state_actions).squeeze()
        else:
            with torch.no_grad():
                next_state_feature_values[non_final_mask] = self.target_net(
                    non_final_new_features,
                ).max(1)[0]

        # Compute expected "future" reward by multiplying rewards predicted by target network with
        # the chosen gamma value and adding the observed rewards.
        expected_state_action_values = (
            (next_state_feature_values * self.gamma) + reward_batch
        ).float()
        expected_state_action_values = weight_batch * expected_state_action_values

        # Perform an optimizer step using the specified loss function.
        if self.loss_function == self.LossFunction.SMOOTH_L1:
            criterion = nn.SmoothL1Loss()
        elif self.loss_function == self.LossFunction.HUBER:
            criterion = nn.HuberLoss()
        else:
            raise RuntimeError(f"Unknown loss function with value {self.loss_function}.")

        # Compute loss per batch element and total loss.
        criterion = nn.HuberLoss(reduction="none")
        loss_all = criterion(state_feature_action_values, expected_state_action_values.unsqueeze(1))
        loss = torch.mean(loss_all)

        # Log loss if data logger is present.
        if self.data_logger is not None:
            self.data_logger.log_datum("LOSS", loss.item())

        # If necessary, update priorities.
        if isinstance(self.memory, PrioritizedReplayMemory):
            self.memory.update_priorities(batch.indices, loss_all)

        # Perform optimizer step.
        self.optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad.clip_grad_value_(self.policy_net.parameters(), 100)
        self.optimizer.step()

        # Update target net by copying from policy net every target_update_interval steps.
        if self.optimization_steps_since_creation % self.target_update_interval != 0:
            return
        target_net_state_dict = self.target_net.state_dict()
        policy_net_state_dict = self.policy_net.state_dict()
        for key in policy_net_state_dict:
            policy_weight = policy_net_state_dict[key]
            target_weight = target_net_state_dict[key]
            target_net_state_dict[key] = policy_weight * self.tau + target_weight * (1 - self.tau)
        self.target_net.load_state_dict(target_net_state_dict)

    def _save_model_implementation(
        self,
        directory: Union[str, os.PathLike],
        file_name_base: str,
    ) -> None:
        # Export model networks.
        self.policy_net.save_model(f"{directory}/{file_name_base}_policy")
        self.target_net.save_model(f"{directory}/{file_name_base}_target")

        # Export additional data used for optimization.
        additional_variables = {
            "Rounds": self.rounds_since_creation,
            "Steps": self.steps_since_creation,
            "Optimizations": self.optimization_steps_since_creation,
        }
        torch.save(additional_variables, f"{directory}/{file_name_base}_additional")

    def _load_model_implementation(
        self,
        directory: Union[str, os.PathLike],
        file_name_base: str,
    ) -> None:
        try:
            # Load model networks.
            self.policy_net.load_model(f"{directory}/{file_name_base}_policy")
            self.policy_net.eval()
            self.target_net.load_model(f"{directory}/{file_name_base}_target")
            self.target_net.eval()
        except FileNotFoundError as e:
            self.log_message(f"Failed to load model. Error message: {e}", log_as_error=True)
            self.clear_model()
        else:
            self.log_message("Loaded target and policy models.")

        # Load additional data used for optimization.
        try:
            additional_variables = torch.load(f"{directory}/{file_name_base}_additional")
            self.rounds_since_creation = additional_variables["Rounds"]
            self.steps_since_creation = additional_variables["Steps"]
            self.optimization_steps_since_creation = additional_variables["Optimizations"]
        except FileNotFoundError as e:
            self.log_message(
                f"Failed to load additional training information. Exception: {e}",
                log_as_error=True,
            )
        else:
            self.log_message(
                f"Loaded target and policy models were trained for {self.rounds_since_creation} "
                f"rounds or {self.steps_since_creation} steps in total and "
                f"{self.optimization_steps_since_creation} optimization steps.",
            )

    def _clear_model_implementation(self) -> None:
        self.rounds_since_creation = 0
        self.steps_since_creation = 0
        self.optimization_steps_since_creation = 0
        self._create_networks()

    def _setup_training_implementation(self) -> None:
        self.do_random_choices = True
        self.do_auto_save = True
        if self.auto_load_memory:
            self.try_load_replays()

    def on_device_changed(self) -> None:
        self.policy_net.to(self.device)
        self.target_net.to(self.device)

    def on_end_of_session(self) -> None:
        self.try_save_replays()

    def on_end_of_round(
        self,
        _last_game_state: Dict[str, Any],
        _last_action: str,
        _events: List[str],
    ) -> None:
        if self.data_logger is not None:
            random_ratio = float(self.random_choices_this_round) / self.actions_this_round * 100
            self.data_logger.log_datum("RANDOM_RATIO", random_ratio)
        self.random_choices_this_round = 0
        self.actions_this_round = 0
