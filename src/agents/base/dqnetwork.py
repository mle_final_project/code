from abc import ABC, abstractmethod

import torch
from torch import nn


class DQNetwork(ABC, nn.Module):

    def __init__(self) -> None:
        nn.Module.__init__(self)

    @abstractmethod
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        pass

    @abstractmethod
    def save_model(self, path: str) -> None:
        pass

    @abstractmethod
    def load_model(self, path: str) -> None:
        pass

    @abstractmethod
    def n_out(self) -> int:
        pass
