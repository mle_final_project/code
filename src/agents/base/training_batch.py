"""This module contains the definition of a training batch."""

from typing import NamedTuple, Optional, Tuple, Union

import torch
from torch import Tensor

from src.agents.base.prioritized_replaymemory import PrioritizedReplayMemory
from src.agents.base.replaymemory import ReplayMemory


class Batch(NamedTuple):
    """A batch of state transitions used for training."""
    old_state_features: Tuple[Tensor, ...]
    actions: Tuple[Tensor, ...]
    new_state_features: Tuple[Optional[Tensor], ...]
    rewards: Tuple[Tensor, ...]
    weights: Tuple[Tensor, ...]
    indices: Tuple[int, ...]


def sample_batch_from_replay_buffer(
    replay_memory: Union[ReplayMemory, PrioritizedReplayMemory],
    batch_size: int,
) -> Batch:
    """Sample a training batch from a replay memory instance.

    Parameters
    ----------
    replay_memory : Union[ReplayMemory, PrioritizedReplayMemory]
        Replay memory to sample from.

    batch_size : int
        The number of samples to take.

    Returns
    ----------
    batch : Batch
        The sampled batch.
    """
    # Prepare transitions and weights.
    if isinstance(replay_memory, ReplayMemory):
        transitions = replay_memory.sample(batch_size)
        transitions_weights_indices = [(transition, 1, -1) for transition in transitions]
    elif isinstance(replay_memory, PrioritizedReplayMemory):
        transitions_weights_indices = replay_memory.sample(batch_size)
    else:
        raise RuntimeError(
            f"Expected memory of type {type(PrioritizedReplayMemory)} or {type(ReplayMemory)} "
            f"but found {type(replay_memory)}.",
        )

    # Create feature, reward, and weight lists.
    old_features_list = []
    action_list = []
    new_features_list = []
    reward_list = []
    weight_list = []
    index_list = []
    for transition, weight, index in transitions_weights_indices:
        old_features, action, new_features, reward = transition
        old_features_list.append(old_features)
        action_list.append(action)
        new_features_list.append(new_features)
        reward_list.append(reward)
        weight_list.append(weight)
        index_list.append(index)

    # Normalize weights.
    weights = torch.tensor(weight_list).unsqueeze(0)
    if isinstance(replay_memory, PrioritizedReplayMemory):
        max_weight = weights.max()
    else:
        max_weight = torch.tensor(1)
    weights = weights / max_weight

    return Batch(
        tuple(old_features_list),
        tuple(action_list),
        tuple(new_features_list),
        tuple(reward_list),
        tuple(weights),
        tuple(index_list),
    )
