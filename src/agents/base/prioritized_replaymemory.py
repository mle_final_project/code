"""This module contains the a circular memory buffer implementation adapted from
https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html updated to use replay
prioritization as described in the paper "Prioritized Experience Replay" found under
https://arxiv.org/abs/1707.06347.
"""

import random
from typing import List, NamedTuple, Optional, Tuple

import numpy as np
from torch import Tensor

from src.agents.base.replaymemory import Transition
from src.agents.base.sum_tree import SumTree


class PrioritizedTransition(NamedTuple):
    transition: Transition
    priority: float


class PrioritizedReplayMemory:
    """Cyclic memory buffer with fixed capacity that makes use of replay prioritization as described
    in "Prioritized Experience Replay" (Schulman et al., https://arxiv.org/abs/1707.06347).
    """

    def __init__(self, capacity: int, alpha: float, beta: float, eps: float) -> None:
        """Initialize this buffer instance.

        Parameters
        ----------
        capacity : int
            The capacity of this memory buffer.

        alpha : float
            The alpha value in [0, 1] to use. Determines the prioritization ratio. Higher values
            lead to higher prioritization of larger priorities. 0 is equivalent to the uniform case.

        beta : float
            The (negative) beta value in [0, 1] to use. Determines the bias annealing. Higher remove
            bias more strongly.

        eps : float
            The minimum priority to use.
        """
        self.capacity = capacity
        self.alpha = alpha
        self.beta = beta
        self.eps = eps
        self.memory: List[Optional[Transition]] = [None] * self.capacity
        self.sum_tree: SumTree = SumTree.create_tree([0 for _ in range(capacity)])
        self.max_priority = self.eps
        self.next_data_index = 0
        self.active_transitions = 0

    def n_transitions(self) -> int:
        """Get the number of stored elements.

        Returns
        ----------
        n_transitions : int
            The number of stored transitions.
        """
        return self.active_transitions

    def push(self, transition: Transition) -> None:
        """Push a list of transitions into memory.

        Parameters
        ----------
        args : Transition
            The transitions to push.
        """
        # Update priority sum tree.
        priority = (self.max_priority + self.eps)**self.alpha
        self.sum_tree.update(priority, self.sum_tree.leafs[self.next_data_index])

        # Update maximum priority.
        self.max_priority = max(self.max_priority, priority)

        # Update transition memory.
        self.memory[self.next_data_index] = transition

        # Increment data index for next iteration.
        self.next_data_index = (self.next_data_index + 1) % self.capacity
        self.active_transitions = min(self.active_transitions + 1, self.capacity)

    def sample(self, batch_size: int) -> List[Tuple[Transition, float, int]]:
        """Sample a number of Transitions from memory.

        Parameters
        ----------
        batch_size : int
            The number of Transitions to sample.

        Returns
        ----------
        samples : List[Tuple[Transition, float, int]]
            The sampled Transitions, priorities, and sample indices.
        """
        n_stored_transitions = self.n_transitions()

        # Determine ranges to sample from.
        total_priority = self.sum_tree.root.value
        range_width = total_priority / batch_size

        # Sample transitions from memory.
        batch = []
        for k in range(batch_size):
            # Draw from each range once.
            uniform_offset = random.uniform(0, range_width)

            # Get priority value to sample.
            sampled_priority = k * range_width + uniform_offset

            # Query index from sum tree.
            sampled_node = self.sum_tree.query(sampled_priority)

            # Compute weight based on present data size.
            weight = np.power(
                sampled_node.value / total_priority * n_stored_transitions,
                -self.beta,
                dtype="float32",
            )

            # Add transition/weight pair to sample batch.
            batch.append((self.memory[sampled_node.index], weight, sampled_node.index))

        return batch

    def update_priorities(self, indices: Tuple[int, ...], priorities: List[Tensor]) -> None:
        """Update a number of priorities.

        Parameters
        ----------
        indices : Tuple[int, ...]
            The indices of the elements to update.

        priorities : List[Tensor]
            The new priorities.
        """
        for index, priority in zip(indices, priorities):
            scaled_priority = (abs(float(priority)) + self.eps)**self.alpha
            self.sum_tree.update(scaled_priority, self.sum_tree.leafs[index])
            self.max_priority = max(self.max_priority, scaled_priority)

    def __len__(self) -> int:
        return self.n_transitions()
