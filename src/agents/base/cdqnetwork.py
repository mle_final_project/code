"""This module contains an adapted Q-network which utilizes convolution on some specified inputs."""

import torch
import torch.nn.functional as f

from torch import nn


class CDQNetwork(nn.Module):
    def __init__(
            self,
            n_observations: int,
            n_actions: int,
            n_inner: int,
            conv_input_shape: tuple[int, int],
            n_hidden: int = 3,
            conv_kernel_shape: tuple[int, int] = (3, 3),
            n_conv_channels: int = 16,
            n_conv_layers: int = 1,
            batch_normalization: bool = True,
            relu: bool = True,) -> None:
        super().__init__()
        self.conv_input_shape = conv_input_shape
        self.n_fully_inputs = n_observations - (conv_input_shape[0] * conv_input_shape[1])
        self.n_conv_inputs = n_observations - self.n_fully_inputs
        conv_output_shape = self.get_resulting_conv_output_shape(conv_input_shape, conv_kernel_shape, n_conv_layers)
        if sum(self.conv_input_shape) != 0:
            self.convolution_network = nn.Sequential(
                nn.Conv2d(in_channels=1, out_channels=n_conv_channels, kernel_size=conv_kernel_shape)
                )
            if batch_normalization:
                self.convolution_network.append(
                    nn.BatchNorm2d(num_features=n_conv_channels),
                )
            for _ in range(n_conv_layers-1):
                self.convolution_network.append(
                    nn.Conv2d(in_channels=n_conv_channels,
                              out_channels=n_conv_channels,
                              kernel_size=conv_kernel_shape)
                )
                if batch_normalization:
                    self.convolution_network.append(
                        nn.BatchNorm2d(num_features=n_conv_channels),
                    )
            if relu:
                self.convolution_network.append(
                    nn.ReLU(),
                )
        self.fully_network = nn.Sequential(
            nn.Linear(
                n_conv_channels * conv_output_shape[0] * conv_output_shape[1] +
                self.n_fully_inputs, n_inner),
            nn.ReLU()
        )
        for _ in range(n_hidden - 2):
            self.fully_network.append(nn.Linear(n_inner, n_inner))
            self.fully_network.append(nn.ReLU())
        self.fully_network.append(nn.Linear(n_inner, n_actions))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        if sum(self.conv_input_shape) != 0:
            x = torch.split(x, [self.n_conv_inputs, self.n_fully_inputs], 1)
            x = torch.cat((
                self.convolution_network(
                    torch.unflatten(x[0], 1, self.conv_input_shape).unsqueeze(1)).flatten(1, 3), x[1]), 1)
        return self.fully_network(x)

    def save_model(self, path: str) -> None:
        torch.save(self.state_dict(), path)

    def load_model(self, path: str) -> None:
        self.load_state_dict(torch.load(path))

    def get_resulting_conv_output_shape(
            self,
            initial_shape: tuple[int, int],
            kernel_shape: tuple[int, int],
            n_conv_layers: int
    ) -> list[int]:
        if sum(self.conv_input_shape) != 0:
            return [initial_shape[0] - n_conv_layers * 2 * (kernel_shape[0]//2),
                    initial_shape[1] - n_conv_layers * 2 * (kernel_shape[1]//2)]
        else:
            return [0, 0]
