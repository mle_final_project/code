"""This module contains the definition of the agent's convolutional Q-network."""

from typing import List, Tuple

import torch
from torch import nn

from src.agents.base.dqnetwork import DQNetwork


def get_conv2d_output_height_and_width(
    h_in: int,
    w_in: int,
    kernel_size: Tuple[int, int],
    padding: Tuple[int, int] = (0, 0),
    dilation: Tuple[int, int] = (1, 1),
    stride: Tuple[int, int] = (1, 1),
) -> Tuple[int, int]:
    h_out = int((h_in + 2 * padding[0] - dilation[0] * (kernel_size[0] - 1) - 1) / stride[0] + 1)
    w_out = int((w_in + 2 * padding[1] - dilation[1] * (kernel_size[1] - 1) - 1) / stride[1] + 1)
    return h_out, w_out


class ConvolutionalDQNetwork(DQNetwork):
    """A shallow convolutional network to use as Q approximator."""

    WRONG_CONFIG_MESSAGE = "Number of output channel and kernel configs must be the same!"
    WRONG_STRIDES_MESSAGE = "If provided, strides must have same length as other settings!"

    def __init__(
        self,
        h_in: int,
        w_in: int,
        out_channels: List[int],
        kernel_sizes: List[Tuple[int, int]],
        n_out: int,
        n_hidden: List[int] | None = None,
        strides: List[Tuple[int, int]] | None = None,
    ) -> None:
        """Construct a new network instance.

        Parameters
        ----------
        h_in : int
            The input image height.

        w_in : int
            The input image width.

        out_channels : List[int]
            The number of output channels for the convolutional layers.

        kernel_sizes: List[Tuple[int, int]]
            The kernel sizes to use for the convolutional layers.

        n_hidden : int
            The number of hidden linear layers to use.

        n_out : int
            The number of outputs of the final linear layer.

        n_hidden : List[int] | None
            The number of neurons to use for the hidden linear layers. If `None`, only use
            linear output.

        strides : List[Tuple[int, int]] | None
            The strides to use.
        """
        # Assert that at least one (valid) layer configuration was provided.
        assert len(out_channels) >= 1, "At least one convolutional layer is required!"
        assert len(out_channels) == len(kernel_sizes), self.WRONG_CONFIG_MESSAGE
        if strides is not None:
            assert len(strides) == len(out_channels), self.WRONG_STRIDES_MESSAGE
        else:
            strides = [(1, 1) for _ in range(len(out_channels))]
        if n_hidden is None:
            n_hidden = []

        # Initialize torch module.
        super().__init__()

        # Store input dimensions for later use.
        self.h_in = h_in
        self.w_in = w_in

        # Create convolutional layers.
        self.conv2d_blocks = nn.Sequential()
        h_out, w_out = h_in, w_in
        for index in range(len(out_channels)):
            # Get layer config.
            previous_out = out_channels[index - 1] if index > 0 else 1
            current_out = out_channels[index]
            kernel_size = kernel_sizes[index]
            stride = strides[index]

            # Create convolutional block and add to model.
            block = nn.Sequential(
                nn.Conv2d(previous_out, current_out, kernel_size, stride),
                nn.MaxPool2d(2),
                nn.ReLU(),
            )
            self.conv2d_blocks.append(block)

            # Compute following dimensions.
            h_out, w_out = get_conv2d_output_height_and_width(
                h_out,
                w_out,
                kernel_size,
                stride=stride,
            )
            h_out, w_out = get_conv2d_output_height_and_width(h_out, w_out, (2, 2), stride=(2, 2))

        # Add fully connected output.
        linear_in = h_out * w_out * out_channels[-1]
        self.linear_blocks = nn.Sequential()
        for inner in n_hidden:
            block = nn.Sequential(
                nn.Linear(linear_in, inner),
                nn.ReLU(),
            )
            self.linear_blocks.append(block)
            linear_in = inner
        self.linear_blocks.append(nn.Linear(linear_in, n_out))
        self.output_count = n_out

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        # We assume that we got a batch of inputs if the first dimension does not match the expected
        # input shape.
        if x.shape[0] != self.h_in:
            x = x.reshape(-1, 1, self.h_in, self.w_in)
        is_batched = len(x.shape) == 4

        # If we do not have batched input, we need to add an implicit channel count of 1.
        if not is_batched:
            x = x.unsqueeze(0)

        # Perform convolution inference.
        for block in self.conv2d_blocks:
            x = block.forward(x)

        # Flatten convolution output.
        x = x.flatten().unsqueeze(0) if not is_batched else x.flatten(1)

        # Perform linear inference.
        for block in self.linear_blocks:
            x = block.forward(x)

        return x

    def save_model(self, path: str) -> None:
        torch.save(self.state_dict(), path)

    def load_model(self, path: str) -> None:
        self.load_state_dict(torch.load(path))

    def n_out(self) -> int:
        return self.output_count
