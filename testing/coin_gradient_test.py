import sys
from pathlib import Path

sys.path.append(str(Path("./").resolve()))  # noqa: RUF100

from argparse import ArgumentParser

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from src.features import create_coin_gradient

if __name__ == "__main__":

    matplotlib.use("tkagg")

    parser = ArgumentParser()
    parser.add_argument(
        "--map_dim",
        type=int,
        default=17,
        dest="map_dim",
        help="Map dimensions to use.",
    )
    parser.add_argument(
        "--n_coins",
        type=int,
        default=50,
        dest="n_coins",
        help="The number of coins to spawn.",
    )
    args = parser.parse_args()

    # Settings to use
    map_dim = args.map_dim
    n_coins = args.n_coins

    # Create data
    tile_map = np.zeros(shape=(map_dim, map_dim)).astype(np.int16)
    coin_positions = np.random.randint(0, map_dim - 1, (n_coins, 2))

    # Plot gradient
    gradient = create_coin_gradient(tile_map, coin_positions)
    plt.imshow(gradient)
    plt.show()
