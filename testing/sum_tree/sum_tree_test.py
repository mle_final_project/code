from typing import List

import numpy as np
from sum_tree import SumTree

if __name__ == "__main__":
    # Create tree.
    values: List[float] = [1, 4, 2, 3]
    tree = SumTree.create_tree(values)

    # Sample values.
    tree_total = tree.root.value
    iterations = 1000000
    selected_values = []
    for _ in range(iterations):
        random_value = np.random.uniform(0, tree_total)
        sampled_value = tree.query(random_value).value
        selected_values.append(sampled_value)

    # Print test results.
    expected = 4
    left = sum([1 for x in selected_values if x == 4])
    right = sum([1 for y in selected_values if y == 1])
    print(f"Expected: ~{expected}, Got: {left / right}")
