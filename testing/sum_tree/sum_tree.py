from typing import List, Optional


class Node:

    next_index = 0

    def __init__(
        self,
        left: Optional["Node"] = None,
        right: Optional["Node"] = None,
        value: float = 0,
    ) -> None:
        self.parent: Optional[Node] = None
        self.left: Optional[Node] = left
        if self.left is not None:
            self.left.parent = self
        self.right: Optional[Node] = right
        if self.right is not None:
            self.right.parent = self
        if Node.is_leaf(self):
            self.value: float = value
            self.index: int = Node.next_index
            Node.next_index += 1
        else:
            self.value: float = Node.get_value(self.left) + Node.get_value(self.right)

    @staticmethod
    def is_leaf(node: Optional["Node"]) -> bool:
        if node is None:
            return False
        return node.left is None and node.right is None

    @staticmethod
    def get_value(node: Optional["Node"]) -> float:
        return 0 if node is None else node.value


class SumTree:

    def __init__(
        self,
        root: Node,
        leafs: List[Node],
    ) -> None:
        self.root = root
        self.leafs = leafs

    def query(self, value: float) -> Node:

        def query_internal(value: float, node: Node) -> Node:
            while not Node.is_leaf(node):
                left_priority = Node.get_value(node.left)
                if node.left is not None and left_priority >= value:
                    node = node.left
                elif node.right is not None:
                    value -= left_priority
                    node = node.right
                else:
                    raise RuntimeError("Both children of a non-leaf node are None!")
            return node

        return query_internal(value, self.root)

    def update_by(self, difference: float, node: Optional[Node]) -> None:
        while node is not None:
            node.value += difference
            node = node.parent

    def update(self, new_value: float, node: Optional[Node]) -> None:
        if node is None:
            return
        difference = new_value - node.value
        node.value = new_value
        self.update_by(difference, node.parent)

    @staticmethod
    def create_tree(values: List[float]) -> "SumTree":
        nodes = [Node(None, None, value) for value in values]
        leafs = nodes
        while len(nodes) > 1:
            nodes_iter = iter(nodes)
            nodes = [Node(*pair) for pair in zip(nodes_iter, nodes_iter)]
        return SumTree(nodes[0], leafs)
