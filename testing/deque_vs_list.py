import random
import time
from collections import deque

import numpy as np

N = 1_000_000
n = 1024

if __name__ == "__main__":
    random_list = np.random.random(N)

    # Measure queue times.
    random.seed = 0
    queue = deque(random_list)
    start_time = time.perf_counter_ns()
    for _ in range(n):
        random.sample(queue, 1024)
    time_queue = (time.perf_counter_ns() - start_time) / N

    # Measure list times.
    random.seed = 0
    lst = random_list.tolist()
    start_time = time.perf_counter_ns()
    for _ in range(n):
        random.sample(lst, 1024)
    time_list = (time.perf_counter_ns() - start_time) / N

    # Print results.
    print(
        f"Average time to sample {n} from {N} queue elements: {time_queue} ns\n"
        f"Average time to sample {n} from {N} list elements:  {time_list} ns",
    )
